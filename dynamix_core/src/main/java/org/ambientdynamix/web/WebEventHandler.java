/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.util.Base64;

import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.security.CryptoUtils;
import org.ambientdynamix.util.Log;

import java.util.Date;
import java.util.Map;

/**
 * Handles event sending for Web-apps.
 * 
 * @author Darren Carlson
 *
 */
public class WebEventHandler {
	public static String TAG = WebEventHandler.class.getSimpleName();

	/**
	 * Singleton constructor
	 */
	private WebEventHandler() {
	}

	/**
	 * Sends the event using the incoming method name, callback and params.
	 */
	public static void sendEvent(WebAppManager<String> wlMgr, String callbackId, String methodName,
			Map<Object, Object> params) {
		Log.i(TAG, "Started sendEvent: " + new Date());
		try {
			RemotePairing pairing = wlMgr.getDynamixApp().getRemotePairing();
			if (pairing != null) {
				String aesKeyString = pairing.getEncryptionKey();
				wlMgr.add(CryptoUtils.encryptStringAES(Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP),
						Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP),
						WebUtils.prepWebAgentEvent(methodName, callbackId, params)));
			} else {
				wlMgr.add(WebUtils.prepWebAgentEvent(methodName, callbackId, params));
			}
		} catch (Exception e) {
			Log.w(TAG, "Encryption error: " + e);
		}
		Log.i(TAG, "Finished sendEvent: " + new Date());
	}
}
