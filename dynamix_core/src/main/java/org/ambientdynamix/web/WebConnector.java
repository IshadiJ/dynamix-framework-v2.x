/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.RemoteException;
import android.util.Base64;
import android.util.Log;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.core.ContextManager;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.Utils;
import org.ambientdynamix.core.WebFacadeBinder;
import org.ambientdynamix.remote.RemotePairing;
import org.ambientdynamix.security.CryptoUtils;
import org.ambientdynamix.security.TrustedCert;
import org.ambientdynamix.util.GenericCallback;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.crypto.SecretKey;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

/**
 * Local web server implementation for handling Dynamix web client requests.
 *
 * @author Darren Carlson
 */
public class WebConnector extends DynamixNanoHTTPD {
    // Private data
    private static final int PAIRING_TIME_OUT_MILLIS = 10 * 24 * 60 * 60 * 1000; // 10 days
    private static WebConnector server;
    private static Map<String, WebAppManager<String>> webAppManagers = new HashMap<String, WebAppManager<String>>();
    static final String TAG = WebConnector.class.getSimpleName();
    private static ContextManager contextMgr;
    private String[] schemes = {"http", "https", "file", "content"}; // DEFAULT schemes = "http", "https", "ftp"
    private UrlValidator urlValidator = new UrlValidator(schemes);
    private static List<TrustedCert> authorizedCerts = new ArrayList<TrustedCert>();
    private final static Map<String, Long> pairingCodes = new HashMap<>();
    private static int port;
    private static String ip = "0.0.0.0";
    private static KeyPair dynamixRSAKeyPair;
    private static Context context;

    /**
     * Singleton constructor.
     */
    private WebConnector(int port) throws IOException {
        // public SimpleWebServer(String host, int port, File wwwroot, boolean quiet) {
        super(port);
        this.port = port;
    }

    /**
     * Starts the WebConnector and related services.
     *
     * @param port             The port the server should use.
     * @param checkPeriodMills The period (in milliseconds) for determining web client timeouts.
     * @param timeoutMills     The time (in milliseconds) that a web client must interact with the WebConnector before the web client
     *                         times out.
     * @throws IOException
     */
    protected synchronized static void startServer(Context c, ContextManager contextMgr, int port, int checkPeriodMills,
                                                   int timeoutMills) throws IOException {
        startServer(c, contextMgr, port, checkPeriodMills, timeoutMills, null);
    }

    /**
     * Starts the WebConnector and related services.
     *
     * @param port             The port the server should use.
     * @param checkPeriodMills The period (in milliseconds) for determining web client timeouts.
     * @param timeoutMills     The time (in milliseconds) that a web client must interact with the WebConnector before the web client
     *                         times out.
     * @param authCerts        A list of authorized X509Certificates for validating web calls.
     * @throws IOException
     */
    public synchronized static void startServer(Context c, final ContextManager contextMgr, int port, int checkPeriodMills,
                                                int timeoutMills, List<TrustedCert> authCerts) throws IOException {
        if (server == null) {
            context = c;
            WebConnector.contextMgr = contextMgr;
            server = new WebConnector(port);
            ListenerMonitor.start(checkPeriodMills, timeoutMills);
            setAuthorizedCerts(authCerts);
            server.start(10000);
            updateInstanceIp();
        }
        Log.d(TAG, "Dynamix Web Connector Running");
    }

    /**
     * Stops the WebConnector and related services. Clears all authorized certificates.
     */
    public synchronized static void stopServer() {
        if (server != null) {
            server.stop();
            server = null;
            ListenerMonitor.stop();
            // Close all sessions
            synchronized (webAppManagers) {
                for (WebAppManager<String> m : webAppManagers.values()) {
                    m.setDead(true);
                    m.getWebBinder().closeSession();
                }
                webAppManagers.clear();
            }
            clearAuthorizedCerts();
        }
        Log.d(TAG, "Dynamix Web Connector Stopped");
    }

    /**
     * Returns true if the WebConnector is started; false otherwise.
     */
    public synchronized static boolean isStarted() {
        return server != null;
    }

    /**
     * Sets the time period (in milliseconds) between checks for web client timeouts.
     */
    public static void setWebClientTimeoutCheckPeriod(int checkPeriodMills) {
        ListenerMonitor.setCheckPeriod(checkPeriodMills);
    }

    /**
     * Sets the web client timeout duration (in milliseconds).
     */
    public static void setWebClientTimeoutMills(int timeoutMills) {
        ListenerMonitor.setTimeoutMills(timeoutMills);
    }

    /**
     * Pauses timeout checking for web clients.
     */
    public static synchronized void pauseTimeoutChecking() {
        if (server != null) {
            ListenerMonitor.pause();
        } else
            Log.v(TAG, "Not started... ignoring pause request");
    }

    /**
     * Resumes timeout checking for web clients.
     */
    protected static synchronized void resumeTimeoutChecking() {
        if (server != null) {
            ListenerMonitor.resume();
        } else
            Log.v(TAG, "Not started... ignoring resume request");
    }

    /**
     * Returns the list of authorized certificates, which are used to validate web calls.
     */
    protected static List<TrustedCert> getAuthorizedCerts() {
        return authorizedCerts;
    }

    /**
     * Adds an authorized certificate, which is used to validate web calls.
     */
    protected static void addAuthorizedCert(TrustedCert authorizedCert) {
        synchronized (authorizedCerts) {
            if (!authorizedCerts.contains(authorizedCert))
                authorizedCerts.add(authorizedCert);
        }
    }

    /**
     * Adds a list of authorized certificates, which are used to validate web calls.
     */
    protected static void setAuthorizedCerts(List<TrustedCert> authCerts) {
        if (authCerts != null)
            synchronized (authorizedCerts) {
                for (TrustedCert cert : authCerts) {
                    if (!authorizedCerts.contains(cert))
                        authorizedCerts.add(cert);
                }
            }
    }

    /**
     * Removes an authorized certificate, which is used to validate web calls.
     */
    protected static void removeAuthorizedCert(TrustedCert authorizedCert) {
        synchronized (authorizedCerts) {
            boolean success = authorizedCerts.remove(authorizedCert);
            Log.i(TAG, "Removing cert " + authorizedCert.getAlias() + " result " + success);
        }
    }

    /**
     * Clears the list of authorized certificates, which are used to validate web calls.
     */
    protected static void clearAuthorizedCerts() {
        synchronized (authorizedCerts) {
            authorizedCerts.clear();
        }
    }

    /**
     * Returns a simple http ok response.
     */
    public Response createOkResponse() {
        return createOkResponse("OK");
    }

    /**
     * Returns an http ok response using the incoming message.
     */
    public Response createOkResponse(String message) {
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "createOkResponse with: " + message);
        if (message == null) {
            Log.w(TAG, "createOkResponse with null message");
            message = "";
        }
        Response r = newFixedLengthResponse(message);
        addCorsHeaders(r);
        return r;
    }

    /**
     * Returns an error message.
     */
    public Response createErrorResponse(String message, Status status, int errorCode) {
        if (message == null) {
            Log.w(TAG, "createErrorResponse with null message");
            message = "";
        }
        Response r = newFixedLengthResponse(status, NanoHTTPD.MIME_PLAINTEXT, message);
        addCorsHeaders(r);
        return r;
    }

    /**
     * Returns an error message.
     */
    public Response errorResponse(String message, Status status, int errorCode, Exception e) {
        if (e != null)
            Log.e(TAG, message, e);
        else
            Log.w(TAG, message, e);
        // Create a map for return values
        Map<Object, Object> jsonMap = new HashMap<Object, Object>();
        jsonMap.put("message", message);
        jsonMap.put("errorCode", errorCode);
        Response r = newFixedLengthResponse(status, NanoHTTPD.MIME_PLAINTEXT, WebUtils.encodeMapAsJson(jsonMap));
        addCorsHeaders(r);
        return r;
    }

    /**
     * Handles web server processing for commands sent by Dynamix web clients. This method is called by NanoHTTPD for
     * each client call. Each call to this method runs on its own thread, so it's ok to block.
     */
    @Override
    public Response serve(IHTTPSession session) {
        // Access the origin (this may be null, e.g., for remotely paired apps)
        String origin = session.getHeaders().get("origin");
        // Access the referer (this may be null, e.g., for remotely paired apps)
        // String referer = session.getHeaders().get("referer");
        // Handle detailed logging
        if (DynamixPreferences.isDetailedLoggingEnabled())
            Log.d(TAG, "WebConnector request from: " + origin + " to uri " + session.getUri() + " and method "
                    + session.getMethod());
        /*
         * Return HTTP_OK for OPTIONS requests to support CORS pre-flighting.
		 */
        if (session.getMethod() == Method.OPTIONS) {
            return createOkResponse();
        }
        // Access the socket for the session's inputstream
        Socket socket = null;
        try {
            socket = getSocket(session);
        } catch (Exception e1) {
            Log.w(TAG, e1);
        }
        // Check if the call is from a verified local app, such as Google Chrome, etc.
        boolean verifiedLocal = isVerifiedLocalRequest(socket);
        /*
         * Handle "HELLO" requests, which are sent from remotely pairing web agents for use in setting up encryption.
		 */
        if (session.getUri().equalsIgnoreCase(RESTHandler.HELLO) && session.getMethod() == Method.GET) {
            Map<Object, Object> jsonMap = new HashMap<Object, Object>();
            try {
                jsonMap.put("instancePublicKey", CryptoUtils.createStringFromPublicKey(dynamixRSAKeyPair.getPublic()));
                String responseText = WebUtils.encodeMapAsJson(jsonMap);
                return createOkResponse(responseText);
            } catch (Exception e) {
                return errorResponse("Security Error", Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION, e);
            }
        }

        // Ensure we got the socket
        if (socket == null)
            return errorResponse("INTERNAL_ERROR: Could not access request socket", Status.INTERNAL_ERROR,
                    ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, null);

        // If verified, use the referer as the origin.
//        if(verifiedLocal){
//            origin = referer;
//        }
        /**
         * If we don't have an origin, the caller may be a remotely paired app,
         * so use the mac address as the origin. This requires that the caller be on the same subnet.
         */
        if (origin == null) {
            String MAC = WebUtils.getMacAddress(socket.getInetAddress());
            if (MAC != null) {
                origin = MAC;
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.w(TAG, "Security warning! Using MAC address as ID for remote client: " + origin);
            } else {
                origin = socket.getInetAddress().getHostAddress();
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.w(TAG,
                            "Security warning! Unable to find MAC address for remote client. Using IP address instead: "
                                    + origin);
            }
        }

		/*
         * Access the Dynamix app's http token (this may be null, e.g., for unbound local callers and unpaired remote
		 * callers). We support both 'authorization' and 'httptoken' for backwards compatibility with older js.
		 */
        String httpToken = null;
        if (session.getHeaders().containsKey("authorization"))
            httpToken = session.getHeaders().get("authorization");
        else {
            httpToken = session.getHeaders().get("httptoken");
        }
        // Check for a WebAppManager using the httpToken token
        final WebAppManager<String> waMgr = getWebAppManagerForToken(origin, httpToken);
        if (waMgr == null || waMgr.isDead()) {
            Log.d(TAG, "No WebAppManager found for " + origin);

            /*
             * Check for a remotely paired app associated with the httpToken.
			 */
            DynamixApplication pairedApp = DynamixService.getRemotelyPairedDynamixApplication(httpToken);
            if (pairedApp != null) {

                /*
                 * Handle unpair for apps without a WebAppManager
				 */
                if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_UNPAIR)) {
                    if (DynamixPreferences.isDetailedLoggingEnabled())
                        Log.d(TAG, "Processing DYNAMIX_UNPAIR for " + origin);
                    pairedApp.setRemotePairing(null);
                    DynamixService.updateApplication(pairedApp);
                    return createOkResponse();
                } else {
                    /*
                     * Otherwise, setup the paired app.
					 */
                    try {
                        if (DynamixPreferences.isDetailedLoggingEnabled())
                            Log.d(TAG, "Found remote pairing for " + origin + " using httpToken " + httpToken);
						/*
						 * In this case, the session.getParms() call will return a VALID map since the original uri was
						 * unencrypted, meaning that NanoHTTPD could extract the values.
						 */
                        return setupPairedApp(origin, session.getParms(), pairedApp.getRemotePairing());
                    } catch (Exception e) {
                        return errorResponse("Bind failed: " + e, Status.INTERNAL_ERROR,
                                ErrorCodes.APPLICATION_EXCEPTION, e);
                    }
                }
            } else {
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "No paired applications for httpToken: " + httpToken);
				/*
				 * If we don't have a valid http token we only allow DYNAMIX_BIND/DYNAMIX_UNBIND requests (from verified
				 * local callers) and DYNAMIX_PAIR requests (from callers with valid credentials).
				 */
                if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_UNBIND)) {
					/*
					 * Handle DYNAMIX_UNBIND requests. Since we don't have a WebAppManager, there's nothing to do except
					 * return OK.
					 */
                    synchronized (webAppManagers) {
                        webAppManagers.remove(origin);
                    }
                    return createOkResponse();
                } else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_BIND) && verifiedLocal) {
                    // Handle verified local DYNAMIX_BIND requests
                    try {
                        // Create httpToken
                        httpToken = UUID.randomUUID().toString();
                        // Add a new WebappManager
                        addWebappManager(origin, httpToken);
                        Log.d(TAG, "Returning HTTP_OK for new WebAppManager: " + origin + " with no encryption");
                        return createOkResponse(httpToken);
                    } catch (Exception e) {
                        return errorResponse(e.getMessage(), Status.FORBIDDEN, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
                    }
                } else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_PAIR)) {
                    // Handle DYNAMIX_PAIR requests
                    try {
                        // Update app name
                        session.getParms().put("appName", origin);
                        // Handle pairing
                        return pair(origin, session.getParms(), verifiedLocal);
                    } catch (Exception e) {
                        return errorResponse("pairing failed", Status.INTERNAL_ERROR, ErrorCodes.APPLICATION_EXCEPTION,
                                e);
                    }
                } else {
                    return errorResponse("Not Authorized", Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED, null);
                }
            }
        } else {
            // Ping the WebAppManager
            waMgr.ping();
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "Incoming http token " + httpToken + " was mapped to WebAppManager " + waMgr + " for app "
                        + waMgr.getWebAppUrl());
            // Base case uses the session's uri directly (without decypting)
            String decryptedUri = session.getUri();
            /**
             * Possible problem:
             * If an app pairs from dynamix.io and uses encryption a *second* app from that same domain
             * without encryption will be unable to connect, since Dynamix will try to use the
             */
            try {
				/*
				 * Handle remotely paired case, if needed. Note that WebAppManagers will not have an associated Dynamix
				 * Application if the caller hasn't opened a session for the first time.
				 */
                if ((waMgr.hasDynamixApplication() && waMgr.getDynamixApp().isRemotelyPaired())
                        && !session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_BIND)) {
					/*
					 * Since the uri was encrypted, session.getParms() will return an empty (INVALID) map since
					 * NanoHTTPD could NOT extract the values. Decrypt the uri and reconstruct the params map here.
					 */
                    session.getParms().clear();
                    String tmpUri = decryptUriAES(waMgr, session.getUri().substring(1));
                    if (tmpUri != null)
                        decryptedUri = parseUri("/" + tmpUri, session.getParms());
                    else {
                        Log.w(TAG, "Unable to properly decrypt URI: " + session.getUri().substring(1));
                    }
					/*
					 * TODO: Decrypt body
					 */
                }
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "decryptedUri: " + decryptedUri);
            } catch (Exception e) {
                String message = "Decrypt exception: " + e;
                return errorResponse(message, Status.INTERNAL_ERROR, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
            }
            if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_BIND)) {
				/*
				 * Caller is binding and we've got a previously created WebappManager.
				 */
                try {
                    if (waMgr.getDynamixApp().isRemotelyPaired()) {
                        return setupPairedApp(origin, session.getParms(), waMgr.getDynamixApp().getRemotePairing());
                    } else {
                        Log.d(TAG, "Bind: Returning HTTP_OK for existing WebAppManager: " + origin + ", encryption "
                                + waMgr.getDynamixApp().isRemotelyPaired());
                        return createOkResponse(waMgr.getToken());
                    }
                } catch (Exception e) {
                    return errorResponse("Binding application failed: " + e.toString(), Status.BAD_REQUEST,
                            ErrorCodes.REQUEST_CANCELLED, null);
                }
            } else if (session.getUri().equalsIgnoreCase(RESTHandler.DYNAMIX_UNPAIR)) {
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "Processing DYNAMIX_UNPAIR for " + waMgr.getDynamixApp());
                // Null any existing pairing
                waMgr.getDynamixApp().setRemotePairing(null);
                // Update Dynamix's state
                DynamixService.updateApplication(waMgr.getDynamixApp());
                // Process unbind to get ride of the stale WebAppManager
                return unbind(waMgr);
            } else {
                // Check for DYNAMIX_UNBIND
                if (decryptedUri.equalsIgnoreCase(RESTHandler.DYNAMIX_UNBIND)) {
                    return unbind(waMgr);
                }
                // Check for IS_DYNAMIX_TOKEN_VALID
                else if (decryptedUri.equalsIgnoreCase(RESTHandler.IS_DYNAMIX_TOKEN_VALID)) {
                    return isTokenvalid(waMgr);
                }
                // Check for IS_DYNAMIX_SESSION_OPEN
                else if (decryptedUri.equalsIgnoreCase(RESTHandler.IS_DYNAMIX_SESSION_OPEN)) {
                    return isSessionOpen(waMgr);
                }
                // Otherwise, process Dynamix REST API call
                else {
                    try {
                        // Use the RESTHandler to process the request
                        return waMgr.getRestHandler().processRequest(waMgr, session, decryptedUri);
                    } catch (Exception e) {
                        Log.w(TAG, "RestHandler.processRequest exception: " + e);
                        // RESTHandler Error: Dynamix could not handle the request
                        return errorResponse("RESTHandler Error: " + e.toString(), Status.INTERNAL_ERROR,
                                ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
                    }
                }
            }
        }
    }

    private String decryptUriRSA(String uri, Map<String, String> parms) throws Exception {
        return parseUri(CryptoUtils.decryptRsa(dynamixRSAKeyPair.getPrivate(), uri), parms);
    }

    private String decryptUriAES(WebAppManager<String> manager, String uri) throws Exception {
        RemotePairing pairing = manager.getDynamixApp().getRemotePairing();
        if (pairing != null) {
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "decryptUriAES for " + manager.getWebAppUrl() + " for uri " + uri);
			/*
			 * Using the commented code gets us a partial decryption.
			 */
            // byte[] iv = CryptoUtils.getSHA1bytes(pairing.getEncryptionKey(), 16);
            // return CryptoUtils.decryptAesBase64String(
            // Base64.decode(pairing.getEncryptionKey().getBytes("UTF-8"), Base64.NO_WRAP), iv, uri);
            return CryptoUtils.decryptAesBase64String(
                    Base64.decode(pairing.getEncryptionKey().getBytes("UTF-8"), Base64.NO_WRAP),
                    Base64.decode(pairing.getEncryptionKey().getBytes("UTF-8"), Base64.NO_WRAP), uri);
        } else {
            throw new Exception("decryptUriAES called on application with no remote pairing!");
        }
    }

    /*
     * TODO: Decode parameters from the URI. This is a first hack, based on the NanoHTTP implementation for this.
     */
    private String parseUri(String uri, Map<String, String> parms) {
        int qmi = uri.indexOf('?');
        if (qmi >= 0) {
            decodeParms(uri.substring(qmi + 1), parms);
            uri = decodePercent(uri.substring(0, qmi));
        } else {
            uri = decodePercent(uri);
        }
        return uri;
    }

    private Response isSessionOpen(WebAppManager<String> waMgr) {
        // Ping the manager to keep it alive
        waMgr.ping();
        try {
            // Create a map for return values
            Map<Object, Object> jsonMap = new HashMap<Object, Object>();
            jsonMap.put("sessionOpen", waMgr.getWebBinder().isSessionOpen());
            return createOkResponse(WebUtils.encodeMapAsJson(jsonMap));
        } catch (RemoteException e) {
            Log.w(TAG, e);
            return errorResponse(e.toString(), Status.INTERNAL_ERROR, ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, e);
        }
    }

    private Response isTokenvalid(WebAppManager<String> waMgr) {
        // Ping the manager to keep it alive
        waMgr.ping();
		/*
		 * Return true since we'll only reach this point if we have a waMgr, meaning that the token is valid.
		 */
        // Create a map for return values
        Map<Object, Object> jsonMap = new HashMap<Object, Object>();
        jsonMap.put("tokenValid", true);
        return createOkResponse(WebUtils.encodeMapAsJson(jsonMap));
    }

    private Response unbind(WebAppManager<String> waMgr) {
        Log.d(TAG, "Processing unbind for: " + waMgr.getWebAppUrl());
        // Ping the manager to keep it alive
        waMgr.ping();
        // Close session with no callback
        waMgr.getWebBinder().closeSession();
        // Set the manager to dead so it can't be used
        waMgr.setDead(true);
        // Remove WebAppManager
        synchronized (webAppManagers) {
            webAppManagers.remove(waMgr.getWebAppUrl());
        }
        // Return UNAUTHORIZED to signal unbound
        return createErrorResponse("UNAUTHORIZED: Dynamix is now unbound.", Status.UNAUTHORIZED, ErrorCodes.NOT_READY);
    }

    /**
     * Verifies the incoming signature.
     */
    private boolean isSignatureValid(String signature, String encryptionKey, String seed) {
        boolean success = false;
        if (seed != null && signature != null) {
            String concat = encryptionKey + seed;
            if (DynamixPreferences.isDetailedLoggingEnabled())
                Log.d(TAG, "isSignatureValid: concat encryptionKey " + encryptionKey + " with seed " + seed
                        + " to form " + concat);
            try {
                // Try SHA-256
                String sha256 = CryptoUtils.shaHash(concat, "SHA-256");
                success = signature.equalsIgnoreCase(sha256);
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG, "isSignatureValid: comparing signature " + signature + " against sha256 hash " + sha256
                            + " with success " + success);
                if (!success) {
                    // Try SHA-512
                    String sha512 = CryptoUtils.shaHash(concat, "SHA-512");
                    success = signature.equalsIgnoreCase(sha512);
                    if (DynamixPreferences.isDetailedLoggingEnabled())
                        Log.d(TAG, "isSignatureValid: comparing signature " + signature + " against sha512 hash "
                                + sha512 + " with success " + success);
                }
            } catch (NoSuchAlgorithmException e) {
                Log.w(TAG, "Could not load MessageDigest: " + e.getMessage() + " . Remote pairing not available.");
                return false;
            }
        } else {
            Log.w(TAG, "isSignatureValid: Missing seed and/or signature.");
        }
        return success;
    }

    /*
     * Utility for setting up a paired app.
     */
    private Response setupPairedApp(String origin, Map<String, String> parms, RemotePairing pairing) {
        Log.d(TAG, "setupPairedApp for " + origin + " and remote pairing " + pairing);
        if (pairing != null) {
            try {
                // Create a map for return values
                Map<Object, Object> jsonMap = new HashMap<Object, Object>();
                String data = parms.get("data");
                String tempClientPublicKey = parms.get("tempClientPublicKey");
                String clearTextUri = CryptoUtils.decryptRsa(dynamixRSAKeyPair.getPrivate(), data);
                Map<String, String> signatureInfo = new HashMap<>();
                decodeParms(clearTextUri, signatureInfo);
                String signature = signatureInfo.get("signature");
                String RNc = signatureInfo.get("rnc");
                if (isSignatureValid(signature, pairing.getEncryptionKey(), RNc)) {
                    // Return the new access token
                    jsonMap.put("masterKey", pairing.getEncryptionKey());
                    jsonMap.put("rnc", RNc);
                    String responseText = WebUtils.encodeMapAsJson(jsonMap);
                    String encryptedResponse = CryptoUtils.encryptRsa(
                            CryptoUtils.createPublicKeyFromString(tempClientPublicKey), responseText);
                    addWebappManager(origin, pairing.getHttpToken());
                    Log.d(TAG, "setupPairedApp: Returning HTTP_OK for new encrypted WebAppManager: " + origin);
                    return createOkResponse(encryptedResponse);
                } else {
                    Log.w(TAG, "setupPairedApp: Invalid signature!");
                    return errorResponse("setupPairedApp: Invalid signature!", Status.UNAUTHORIZED,
                            ErrorCodes.NOT_AUTHORIZED, null);
                }
            } catch (Exception e) {
                Log.w(TAG, "setupPairedApp: Error - " + e);
                return errorResponse("setupPairedApp: Error - " + e, Status.INTERNAL_ERROR,
                        ErrorCodes.DYNAMIX_FRAMEWORK_ERROR, null);
            }
        } else {
            Log.w(TAG, "setupPairedApp: No remote pairing!");
            return errorResponse("setupPairedApp: No remote pairing!", Status.UNAUTHORIZED, ErrorCodes.NOT_AUTHORIZED,
                    null);
        }
    }

    private Response pair(String origin, Map<String, String> parms, boolean verifiedLocal) throws Exception {
        String tempClientPublicKey = parms.get("tempClientPublicKey");
        String appName = parms.get("appName");
        boolean temp = Boolean.parseBoolean(parms.get("temporary"));
        String signature = parms.get("signature");
        Log.d(TAG, "Encryption signature: " + signature);
        Log.d(TAG,
                "Encryption generated public key: "
                        + CryptoUtils.createStringFromPublicKey(dynamixRSAKeyPair.getPublic()));
        String clearTextUri = CryptoUtils.decryptRsa(dynamixRSAKeyPair.getPrivate(), signature);
        Map<String, String> pairingInfo = new HashMap<>();
        decodeParms(clearTextUri, pairingInfo);
        String pairingCode = pairingInfo.get("pairingCode");
        String RNc = pairingInfo.get("rnc");
        if (verifiedLocal || isPairingCodeValid(pairingCode)) {
            // Create a name if the pairing process doesn't include one
            if (appName == null)
                appName = "Remotely Paired App";
            if (origin == null)
                origin = appName;
            // SecretKey initKey = CryptoUtils.generateAesKey(256);
            SecretKey initKey = CryptoUtils.generateAesKey(128);
            String initKeyString = CryptoUtils.createStringFromAESKey(initKey);
            // Only log initKey at DEBUG or lower so that it's stripped in production
            Log.d(TAG, "Generated new initKey: " + initKeyString);
            String httpToken = UUID.randomUUID().toString();
			/*
			 * Make sure Dynamix knows about the app and/or pairing.
			 */
            DynamixApplication app = DynamixService.getDynamixApplication(origin);
            RemotePairing pairing = new RemotePairing(temp, initKeyString, httpToken);
            if (app != null) {
                app.setRemotePairing(pairing);
                DynamixService.updateApplication(app);
                Log.d(TAG, "Pair: Updated existing application: " + app);
            } else {
                app = new DynamixApplication(origin, appName, pairing);
                DynamixService.addDynamixApplicationToSettings(app);
                Log.d(TAG, "Pair: Created new application: " + app);
            }
            // boolean dynamixApplication = DynamixService.addDynamixApplicationToSettings());
            Map<Object, Object> jsonMap = new HashMap<Object, Object>();
            jsonMap.put("initKey", initKeyString);
            jsonMap.put("httpToken", httpToken);
            jsonMap.put("rnc", RNc);
            String responseText = WebUtils.encodeMapAsJson(jsonMap);
            String cryptoResponse = CryptoUtils.encryptRsa(CryptoUtils.createPublicKeyFromString(tempClientPublicKey),
                    responseText);
            Log.d(TAG, "App " + origin + " paired successful with httptoken " + httpToken);
            removePairingCode(pairingCode);
            addWebappManager(origin, httpToken);
            return createOkResponse(cryptoResponse);
        } else {
            Log.w(TAG, "FORBIDDEN: caller is not verified local and does not have a valid pairing code!");
            return createErrorResponse("FORBIDDEN: Pairing code invalid!", Status.FORBIDDEN, ErrorCodes.NOT_AUTHORIZED);
        }
    }

    /**
     * Returns true if the socket is bound to the loopback address and the calling app is approved to interact with
     * Dynamix (i.e., Dynamix trusts its certificate).
     */
    private boolean isVerifiedLocalRequest(Socket socket) {
        // Otherwise, verify that the request is local
        if (socket.getInetAddress().isLoopbackAddress()) {
            // Get requesting app information using the socket
            //ApplicationInfo app = WebUtils.getAppProcessForSocket2(context, socket);
            ApplicationInfo app = WebUtils.getAppProcessForSocket(socket);
            if (app != null) {
                // Log.i(TAG, "Total Certs is: " + authorizedCerts.size());
				/*
				 * Verify that the requesting app has a valid certificate. According to the Android docs, multiple
				 * versions of an app (e.g., upgrades) should be signed by the same cert. There will typically be one
				 * packageName for an app. See http://developer.android.com/tools/publishing/app-signing.html
				 */
                // Get the cert for the app's packageName
                X509Certificate cert = WebUtils.getCertForApp(app.packageName);
                try {
                    // Ensure it's valid for this time period
                    cert.checkValidity(new Date());
                    // Check against the list of authorized certs
                    for (TrustedCert authorized : authorizedCerts) {
                        // Try to verify the app's cert
                        try {
                            authorized.getCert().verify(cert.getPublicKey());
                            // App is verified
                            return true;
                        } catch (Exception e) {
                            // Log.w(TAG, "doAuthorizedServe exception: " + e);
                            // e.printStackTrace();
                        }
                    }
                    // If we reach this point, no authorized cert could be found for the app
                    Log.w(TAG, "No certificate found for " + app.processName);
						/*
						 * If 'collectCerts' is true, auto-authorize the calling app process and store its cert.
						 */
                    if (DynamixPreferences.collectCerts()) {
                        Log.i(TAG, "Web call from: " + app.processName + " with UID " + app.uid);
                        Log.w(TAG, "Auto-authorizing cert for " + app);
                        try {
                            DynamixService.storeAuthorizedCert(app.packageName, cert);
                            addAuthorizedCert(new TrustedCert(app.packageName, cert));
                            return true;
                        } catch (Exception e) {
                            Log.w(TAG, "Error storing cert: " + e);
                        }
                    } else {
                        return false;
                    }
                } catch (CertificateExpiredException e1) {
                    Log.w(TAG, "Cert expired for " + app);
                } catch (CertificateNotYetValidException e1) {
                    Log.w(TAG, "Cert not yet valid for " + app);
                }
            } else {
                Log.w(TAG, "App not found");
            }
        } else {
            Log.w(TAG, "Non-localhost request");
        }
        return false;
    }

    /**
     * Adds the web app, which is identified by both the origin, http token, and (optionally) a master encryption key.
     *
     * @param httpToken
     */
    protected boolean addWebappManager(String origin, String httpToken) {
        if (httpToken != null && httpToken.length() > 0) {
            synchronized (webAppManagers) {
                if (!webAppManagers.keySet().contains(origin)) {
                    // Create the Web app's binder
                    WebFacadeBinder binder = new WebFacadeBinder(DynamixService.getAndroidContext(), contextMgr, false,
                            origin);
                    // Create the Web app's Rest Handler
                    RESTHandler restHandler = new RESTHandler(binder, this);
                    WebAppManager<String> wlm = new WebAppManager<String>(origin, httpToken, binder, restHandler);
                    webAppManagers.put(origin, wlm);
                    Log.d(TAG, "WebAppManager added for origin " + origin + " using token " + httpToken);
                    return true;
                } else {
                    Log.d(TAG, "WebAppManager found for origin " + origin + " using token " + httpToken);
                    return true;
                }
            }
        } else {
            Log.w(TAG, "addWebappManager missing httpToken.");
            return false;
        }
    }

    /**
     * Returns true if the listener's token is registered; false otherwise.
     */
    protected boolean containsListener(String webAgentId) {
        synchronized (webAppManagers) {
            return webAppManagers.containsKey(webAgentId);
        }
    }

    /**
     * Returns the listener manager associated with the token.
     */
    protected WebAppManager getListener(String webAgentId) {
        synchronized (webAppManagers) {
            return webAppManagers.get(webAgentId);
        }
    }

    /**
     * Returns the WebAppManager associated with the incoming token, or null if not found.
     */
    private WebAppManager<String> getWebAppManagerForToken(String origin, String token) {
        synchronized (webAppManagers) {
            for (WebAppManager<String> mgr : webAppManagers.values()) {
                if (DynamixPreferences.isDetailedLoggingEnabled())
                    Log.d(TAG,
                            "Checking origin " + origin + " with token " + token + " against WebappManager "
                                    + mgr.getWebAppUrl() + " with token " + mgr.getToken());
                if (mgr.getToken().equalsIgnoreCase(token))
                    return mgr;
            }
        }
        return null;
    }

    /**
     * Monitor class that removes dead listeners as needed.
     *
     * @author Darren Carlson
     */
    private static class ListenerMonitor {
        private static int checkPeriod = 5000;
        private static int timeoutMills = 15000;
        private static boolean done = true;
        private static boolean paused = false;
        private static Thread t = null;

        /**
         * Stops the ListenerMonitor
         */
        public synchronized static void stop() {
            done = true;
            paused = false;
        }

        /**
         * Sets the check period (in milliseconds).
         */
        public synchronized static void setCheckPeriod(int checkPeriodMills) {
            if (checkPeriod > 0)
                ListenerMonitor.checkPeriod = checkPeriodMills;
            else
                ListenerMonitor.checkPeriod = 5000;
        }

        /**
         * Pauses timeout checking for web clients.
         */
        public synchronized static void pause() {
            if (!done) {
                if (!paused) {
                    Log.d(TAG, "Pausing timeout checking for web clients");
                    paused = true;
                } else
                    Log.w(TAG, "Already paused");
            } else
                Log.w(TAG, "Not started");
        }

        /**
         * Resumes timeout checking for web clients.
         */
        public synchronized static void resume() {
            if (paused) {
                // Ping all the listeners, since we've been paused
                synchronized (webAppManagers) {
                    for (WebAppManager<String> waMgr : webAppManagers.values())
                        // Ping the WebAppManager
                        waMgr.ping();
                }
                Log.d(TAG, "Resuming timeout checking for web clients");
                paused = false;
            } else
                Log.w(TAG, "Not paused");
        }

        /**
         * Sets the timeout period (in milliseconds).
         *
         * @param timeoutMills
         */
        public synchronized static void setTimeoutMills(int timeoutMills) {
            if (timeoutMills > 0)
                ListenerMonitor.timeoutMills = timeoutMills;
            else
                ListenerMonitor.timeoutMills = 15000;
        }

        /**
         * Starts monitoring web clients for timeouts.
         *
         * @param checkPeriod  The check period in milliseconds.
         * @param timeoutMills The timeout duration in milliseconds.
         */
        public synchronized static void start(int checkPeriod, int timeoutMills) {
            setCheckPeriod(checkPeriod);
            setTimeoutMills(timeoutMills);
            if (done) {
                done = false;
                t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "ListenerMonitor started");
                        while (!done) {
                            try {
                                // Sleep for the check period
                                Thread.sleep(ListenerMonitor.checkPeriod);
                            } catch (InterruptedException e) {
                            }
                            if (!paused) {
                                // Create an ArrayList of tokens to remove
                                List<String> remove = new ArrayList<String>();
                                // Remember the current time
                                Date now = new Date();
                                synchronized (webAppManagers) {
                                    // First, check for any existing dead listeners
                                    for (String listener : webAppManagers.keySet()) {
                                        WebAppManager<String> m = webAppManagers.get(listener);
                                        if (m.isDead())
                                            remove.add(listener);
                                    }
                                    // Next, remove the dead listeners
                                    for (String listener : remove) {
                                        Log.d(TAG, "Removing dead listener: " + listener);
                                        WebAppManager<String> m = webAppManagers.remove(listener);
                                    }
                                    // Finally, check for any listener timeouts
                                    for (String listener : webAppManagers.keySet()) {
                                        WebAppManager<String> m = webAppManagers.get(listener);
                                        if ((now.getTime() - m.getLastAccess().getTime()) > ListenerMonitor.timeoutMills) {
                                            m.setDead(true);
                                            m.getWebBinder().closeSession();
                                        }
                                    }
                                }
                            }
                        }
                        paused = false;
                        Log.d(TAG, "ListenerMonitor stopped");
                    }
                });
                t.setDaemon(true);
                t.start();
            } else
                Log.w(TAG, "ListenerMonitor is already running!");
        }
    }

    /**
     * Add a new pairing code to the list of valid pairing codes. Expires after 30 seconds.
     *
     * @param pairingCode the pairing code to add
     */
    public static boolean addPairingCode(String pairingCode) {
        synchronized (pairingCodes) {
			/*
			 * Enforce the minimum pairing code length
			 */
            if (pairingCode != null && pairingCode.length() >= 11) {
                pairingCodes.put(pairingCode, System.currentTimeMillis());
                return true;
            } else {
                Log.w(TAG, "Pairing code rejected due to inadequate length");
                return false;
            }
        }
    }

    /**
     * Check if a given pairing code is valid. This method also cleans up all expired pairing codes and removes them
     * from the map
     *
     * @param pairingCode the pairing code to validate
     * @return true if the pairing code is valid
     */
    public static boolean isPairingCodeValid(String pairingCode) {
        synchronized (pairingCodes) {
            cleanPairingCodes();
            if (pairingCodes.containsKey(pairingCode)) {
                return true;
            }
            return false;
        }
    }

    /**
     * cleans the pairing code list and removes all expired Codes
     */
    public static void cleanPairingCodes() {
        synchronized (pairingCodes) {
            long currentTimeMillis = System.currentTimeMillis();
            List<String> removeMe = new ArrayList<String>();
            for (Map.Entry<String, Long> code : pairingCodes.entrySet()) {
                if (currentTimeMillis - code.getValue() > PAIRING_TIME_OUT_MILLIS)
                    removeMe.add(code.getKey());
            }
            for (String obsoleteCode : removeMe) {
                pairingCodes.remove(obsoleteCode);
            }
        }
    }

    /**
     * Removes a pairing code from the pairings list, e.g. call this after a successful pairing to invalidate the used
     * code
     *
     * @param pairingCode the code to remove
     */
    public void removePairingCode(String pairingCode) {
        synchronized (pairingCodes) {
            pairingCodes.remove(pairingCode);
        }
    }

    public static int getPort() {
        return port;
    }

    public static void makePOSTRequestNew(final String urlString, final List<NameValuePair> nameValuePairs,
                                          final String method, final GenericCallback<String> callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL url;
                String response = "";
                try {
                    url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod(method);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    Log.d(TAG, "Instance query: " + getQuery(nameValuePairs));
                    writer.write(getQuery(nameValuePairs));
                    writer.flush();
                    writer.close();
                    os.close();
                    InputStream responseStream = new BufferedInputStream(conn.getInputStream());
                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();
                    conn.connect();
                    response = stringBuilder.toString();
                    if (callback != null)
                        callback.onSuccess(response);
                } catch (IOException e) {
                    if (callback != null)
                        callback.onFailure(e.getMessage());
                }
            }
        }).start();
    }

    private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    private static void updateInstanceIpTable(String ip, GenericCallback<String> callback) throws Exception {
        NameValuePair instancePubKey = new BasicNameValuePair("instancePublicKey",
                CryptoUtils.createStringFromPublicKey(dynamixRSAKeyPair.getPublic()));
        NameValuePair instanceId = new BasicNameValuePair("instanceId", DynamixPreferences.getDynamixInstanceId());
        NameValuePair instanceIp = new BasicNameValuePair("instanceIp", ip);
        NameValuePair instancePort = new BasicNameValuePair("instancePort", "" + WebConnector.getPort());
        final ArrayList<NameValuePair> params = new ArrayList<>();
        params.add(instanceId);
        params.add(instancePubKey);
        params.add(instanceIp);
        params.add(instancePort);
        WebConnector.makePOSTRequestNew(DynamixPreferences.getInstanceIpServerURL(), params, "POST", callback);
    }

    public static void updateInstanceIp() {
        if (contextMgr == null)
            return;
        String currentIp = Utils.getLocalIpAddress();
        if (!currentIp.equals(ip)) {
            Log.d(TAG, "Found new ip address : " + currentIp);
            ip = currentIp;
            try {
                WebConnector.updateInstanceIpTable(ip, new GenericCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        Log.d(getClass().getSimpleName(), "Updated Instance Ip to: " + ip + " response: " + result);
                    }

                    @Override
                    public void onFailure(String message) {
                        Log.e(getClass().getSimpleName(), "couldn't update instance ip: " + message);
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "couldn't update instance ip: " + e.getMessage());
            }
        }
    }

    public static void setDynamixRSAKeyPair(KeyPair dynamixRSAKeyPair) {
        WebConnector.dynamixRSAKeyPair = dynamixRSAKeyPair;
        try {
            Log.i(TAG, "Dynamix Public Key: " + CryptoUtils.createStringFromPublicKey(dynamixRSAKeyPair.getPublic()));
        } catch (Exception e) {
        }
    }

    /**
     * Decodes parameters in percent-encoded URI-format ( e.g. "name=Jack%20Daniels&pass=Single%20Malt" ) and adds them
     * to given Map. NOTE: this doesn't support multiple identical keys due to the simplicity of Map.
     */
    protected void decodeParms(String parms, Map<String, String> p) {
        if (parms == null) {
            // p.put(QUERY_STRING_PARAMETER, "");
            return;
        }
        // p.put(QUERY_STRING_PARAMETER, parms);
        StringTokenizer st = new StringTokenizer(parms, "&");
        while (st.hasMoreTokens()) {
            String e = st.nextToken();
            int sep = e.indexOf('=');
            if (sep >= 0) {
                p.put(decodePercent(e.substring(0, sep)).trim(), decodePercent(e.substring(sep + 1)));
            } else {
                p.put(decodePercent(e).trim(), "");
            }
        }
    }

    /*
     * Utility method that adds all required CORS headers to the response.
     */
    private void addCorsHeaders(Response r) {
        // Configure CORS for the response
        r.addHeader("Access-Control-Allow-Origin", "*");
        r.addHeader("Access-Control-Allow-Credentials", "true");
        r.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        r.addHeader("Access-Control-Allow-Headers",
                "Origin,httptoken,Accept,Content-Type,Content-Length,Cookie,Authorization");
        r.addHeader("Cache-Control", "no-cache");
        r.addHeader("Pragma", "no-cache");
        r.addHeader("Expires", "0");
		/*
		 * 2015.07.29: While refactoring, NanoHTTPD complained about 'broken pipe' issues from time to time, and setting
		 * both 'setKeepAlive' and 'setChunkedTransfer' to false seemed to fix the problem. However, this issue may have
		 * been related to returning improper values during the refactoring. Once we fixed the return values these
		 * errors dissapeared. Leaving the comments for now.
		 */
        // r.setKeepAlive(false);
        // r.setChunkedTransfer(false);
    }
}
