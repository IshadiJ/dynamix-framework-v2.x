/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility for mapping ApplicationInfo to socket connections. Not working reliably.
 */
public class PortMapper {
    /**
     * Notes:
     * Partially based on https://github.com/zyrorl/sandrop/tree/master/projects/SandroProxyLib/src/org/sandroproxy/utils/network
     * Partially based on https://github.com/dextorer/AndroidTCPSourceApp/blob/master/src/com/megadevs/tcpsourceapp/TCPSourceApp.java (regex parsing not working properly)
     */
    private static boolean LOCAL_LOGGING = true;
    private static String TAG = PortMapper.class.getSimpleName();
    /*
    * In a Linux-based OS, each active TCP socket is mapped in the following
    * two files. A socket may be mapped in the '/proc/net/tcp' file in case
    *  of a simple IPv4 address, or in the '/proc/net/tcp6' if an IPv6 address
    *  is available.
    */
    private static final String TCP_4_FILE_PATH = "/proc/net/tcp";
    private static final String TCP_6_FILE_PATH = "/proc/net/tcp6";
    public static final String TCP_6_PATTERNb = "\\s+\\d+:\\s([0-9A-F]{32}):([0-9A-F]{4})\\s([0-9A-F]{32}):([0-9A-F]{4})\\s([0-9A-F]{2})\\s[0-9]{8}:[0-9]{8}\\s[0-9]{2}:[0-9]{8}\\s[0-9]{8}\\s+([0-9]+)";
    public static final String TCP_4_PATTERNb = "\\s+\\d+:\\s([0-9A-F]{8}):([0-9A-F]{4})\\s([0-9A-F]{8}):([0-9A-F]{4})\\s([0-9A-F]{2})\\s[0-9A-F]{8}:[0-9A-F]{8}\\s[0-9]{2}:[0-9]{8}\\s[0-9A-F]{8}\\s+([0-9]+)";
    /*
     * Optimises the socket lookup by checking if the connected network
     * interface has a 'valid' IPv6 address (a global address, not a link-local
     * one).
     */
    private static boolean checkConnectedIfaces = false;

    /*
     * Alternative method that receives a Socket object and just extracts the
     * port from it, subsequently calling the overloaded method.
     */
    public static ApplicationInfo getApplicationInfo(Context context, Socket socket) {
        return getApplicationInfo(context, socket.getPort());
    }

    /**
     * The main method of the TCPSourceApp library. This method receives an
     * Android Context instance, which is used to access the PackageManager.
     * It parses the /proc/net/tcp* files, looking for a socket entry that
     * matches the given port. If it finds an entry, this method extracts the
     * PID value and it uses the PackageManager.getPackagesFromPid() method to
     * find the originating application.
     *
     * @param context a valid Android Context instance
     * @param port    the (logical) port of the socket
     * @return an AppDescriptor object, representing the found application; null
     * if no application could be found
     */
    @SuppressWarnings("unused")
    public static ApplicationInfo getApplicationInfo(Context context, int port) {
        File tcp;
        BufferedReader reader;
        String line;
        StringBuilder builder;
        String content;
        try {
            boolean hasIPv6 = true;
            // if true, checks for a connected network interface with a valid
            // IPv4 / IPv6 address
            if (checkConnectedIfaces) {
                String ipv4Address = getIPAddress(true);
                String ipv6Address = getIPAddress(false);
                hasIPv6 = (ipv6Address.length() > 0);
            }
            tcp = new File(TCP_6_FILE_PATH);
            reader = new BufferedReader(new FileReader(tcp));
            line = "";
            builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            content = builder.toString();
            Matcher m6 = Pattern.compile(TCP_6_PATTERNb, Pattern.CASE_INSENSITIVE | Pattern.UNIX_LINES | Pattern.DOTALL).matcher(content);
            if (hasIPv6)
                while (m6.find()) {
                    String srcAddressEntry = m6.group(1);
                    String srcPortEntry = m6.group(2);
                    String dstAddressEntry = m6.group(3);
                    String dstPortEntry = m6.group(4);
                    String status = m6.group(5);
                    int uidEntry = Integer.valueOf(m6.group(6));
                    int srcPort = Integer.valueOf(srcPortEntry, 16);
                    int dstPort = Integer.valueOf(dstPortEntry, 16);
                    int connStatus = Integer.valueOf(status, 16);
//                    if (LOCAL_LOGGING)
//                        Log.d(TAG, "PortMapper checking pid " + uidEntry + " with port " + srcPort + " against requested port " + port);
                    if (srcPort == port) {
                        PackageManager manager = context.getPackageManager();
                        String[] packagesForUid = manager.getPackagesForUid(uidEntry);
                        if (packagesForUid != null) {
                            String packageName = packagesForUid[0];
                            ApplicationInfo app = manager.getApplicationInfo(packageName, 0);
                            if (LOCAL_LOGGING)
                                Log.d(TAG, "PortMapper matched app " + app + " with port " + srcPort + " with requested port " + port);
                            return app;
                        }
                    }
                }
        } catch (Exception e) {
            Log.w(TAG, e);
            e.printStackTrace();
        }
        // From here, no connection with the given port could be found in the tcp6 file
        // So let's try the tcp (IPv4) one
        try {
            tcp = new File(TCP_4_FILE_PATH);
            reader = new BufferedReader(new FileReader(tcp));
            line = "";
            builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            content = builder.toString();
            //Log.d(TAG, "PortMapper TCP_4_FILE_PATH + " + content);
            Matcher m4 = Pattern.compile(TCP_4_PATTERNb, Pattern.CASE_INSENSITIVE | Pattern.UNIX_LINES | Pattern.DOTALL).matcher(content);
            while (m4.find()) {
                String srcAddressEntry = m4.group(1);
                String srcPortEntry = m4.group(2);
                String dstAddressEntry = m4.group(3);
                String dstPortEntry = m4.group(4);
                String connStatus = m4.group(5);
                int uidEntry = Integer.valueOf(m4.group(6));
                int srcPort = Integer.valueOf(srcPortEntry, 16);
                int dstPort = Integer.valueOf(dstPortEntry, 16);
                int status = Integer.valueOf(connStatus, 16);
//                if (LOCAL_LOGGING)
//                    Log.d(TAG, "PortMapper checking pid " + uidEntry + " with port " + srcPort + " against requested port " + port);
                if (srcPort == port) {
                    PackageManager manager = context.getPackageManager();
                    String[] packagesForUid = manager.getPackagesForUid(uidEntry);
                    if (packagesForUid != null) {
                        String packageName = packagesForUid[0];
                        PackageInfo pInfo = manager.getPackageInfo(packageName, 0);
                        ApplicationInfo app = manager.getApplicationInfo(packageName, 0);
                        if (LOCAL_LOGGING)
                            Log.d(TAG, "PortMapper matched app " + app + " with port " + srcPort + " with requested port " + port);
                        return app;
                    }
                }
            }
        } catch (Exception e) {
            Log.w(TAG, e);
            e.printStackTrace();
        }
        if (LOCAL_LOGGING)
            Log.d(TAG, "PortMapper could not find app bound to port " + port);
        return null;
    }

    @SuppressLint("DefaultLocale")
    public static String getIPAddress(boolean useIPv4) throws SocketException {
        List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        for (NetworkInterface intf : interfaces) {
            List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
            for (InetAddress addr : addrs) {
                if (!addr.isLoopbackAddress()) {
                    String sAddr = addr.getHostAddress().toUpperCase();
                    boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                    if (useIPv4) {
                        if (isIPv4)
                            return sAddr;
                    } else {
                        if (!isIPv4) {
                            if (sAddr.startsWith("fe80") || sAddr.startsWith("FE80")) // skipping link-local addresses
                                continue;
                            int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                            return delim < 0 ? sAddr : sAddr.substring(0, delim);
                        }
                    }
                }
            }
        }
        return "";
    }

    /*
     * Sets the connected interfaces optimisation.
     */
    public static void setCheckConnectedIfaces(boolean value) {
        checkConnectedIfaces = value;
    }
}
