/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ISessionListener;

import android.os.RemoteException;
import android.util.Log;

/**
 * Web-based implementation of the ISessionListener interface.
 * 
 * @see ISessionListener
 * @author Darren Carlson
 */
public class WebSessionListener extends ISessionListener.Stub {
	private final String TAG = this.getClass().getSimpleName();
	private WebAppManager<String> wlMgr;
	private String listenerId;

	/**
	 * Creates a WebSessionListener.
	 */
	public WebSessionListener(WebAppManager<String> wlMgr, String listenerId) {
		this.wlMgr = wlMgr;
		this.listenerId = listenerId;
	}

	/**
	 * Returns the URL associated with this WebSessionListener.
	 */
	public String getWebAppUrl() {
		return this.wlMgr.getWebAppUrl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSessionOpened(String sessionId) throws RemoteException {
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("sessionId", sessionId);
		WebEventHandler.sendEvent(wlMgr, listenerId, "onSessionOpened", map);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSessionClosed() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, listenerId, "onSessionClosed", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixFrameworkActive() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, listenerId, "onDynamixFrameworkActive", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDynamixFrameworkInactive() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, listenerId, "onDynamixFrameworkInactive", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDiscoveryStarted() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginDiscoveryStarted", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDiscoveryFinished() throws RemoteException {
		WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginDiscoveryFinished", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginInstalled(ContextPluginInformation plugin) throws RemoteException {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("plugin", plugin);
			WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginInstalled", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginEnabled(ContextPluginInformation plugin) throws RemoteException {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("plugin", plugin);
			WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginEnabled", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginDisabled(ContextPluginInformation plugin) throws RemoteException {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("plugin", plugin);
			WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginDisabled", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginUninstalled(ContextPluginInformation plugin) throws RemoteException {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("plugin", plugin);
			WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginUninstalled", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextPluginError(ContextPluginInformation plugin, String message, int errorCode)
			throws RemoteException {
		try {
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("plugin", plugin);
			map.put("message", message);
			map.put("errorCode", errorCode);
			WebEventHandler.sendEvent(wlMgr, listenerId, "onContextPluginError", map);
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}
}
