/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.util.BasicBinder;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * Web-based implementation of the IContextListener interface.
 * 
 * @see IContextListener
 * @author Darren Carlson
 */
public class WebContextListener implements IContextListener {
	private final String TAG = this.getClass().getSimpleName();
	private WebAppManager<String> wlMgr;
	private String callbackId;
	private IBinder binder;

	/**
	 * Creates a WebContextListener.
	 */
	public WebContextListener(WebAppManager<String> wlMgr, String callbackId) {
		this.wlMgr = wlMgr;
		this.callbackId = callbackId;
		this.binder = new BasicBinder(callbackId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IBinder asBinder() {
		return binder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextListenerRemoved() throws RemoteException {
		sendEvent("onContextListenerRemoved", null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextSupportRemoved(ContextSupportInfo supportInfo, String message, int errorCode)
			throws RemoteException {
		Map<Object, Object> map = new HashMap<>();
		map.put("supportInfo", supportInfo);
		map.put("message", message);
		map.put("errorCode", errorCode);
		sendEvent("onContextSupportRemoved", map);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onContextResult(ContextResult result) throws RemoteException {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.d(TAG, "Handling onContextResult: " + result);
		Map<Object, Object> encoded = WebUtils.encodeContextResult(result);
		if (encoded != null)
			sendEvent("onContextResult", WebUtils.encodeContextResult(result));
		else {
			if (DynamixPreferences.isDetailedLoggingEnabled())
				Log.d(TAG, "Context result could not be encoded: " + result);
		}
	}

	/**
	 * Sends the event using the incoming method name and params.
	 */
	private void sendEvent(String methodName, Map<Object, Object> params) {
		WebEventHandler.sendEvent(wlMgr, callbackId, methodName, params);
	}
}