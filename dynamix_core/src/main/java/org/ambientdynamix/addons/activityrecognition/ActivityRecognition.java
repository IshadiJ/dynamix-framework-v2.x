/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.addons.activityrecognition;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.ambientdynamix.addons.GenericIntentService;
import org.ambientdynamix.core.IAndroidEventProvider;

/**
 * Created by shivam on 8/27/15.
 */
public class ActivityRecognition implements IAndroidEventProvider, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    private ActivityRecognition() {
    }

    public ActivityRecognition(Context context) {
        this.context = context;
    }

    private Context context;
    protected GoogleApiClient mGoogleApiClient;
    private PendingIntent mActivityDetectionPendingIntent;
    private String TAG = getClass().getSimpleName();
    public static final String INTENT_TYPE = "org.ambientdynamix.android.events.ACTIVITY_RECOGNITION";

    public void start() {
        // Build the google api client
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(com.google.android.gms.location.ActivityRecognition.API)
                .build();
        Log.d(TAG, "Connecting GoogleApiClient for Activity Detection: " + mGoogleApiClient);
        // Connect the google api client
        mGoogleApiClient.connect();
    }

    public void stop() {
        Log.d(TAG, "Disconnecting GoogleApiClient for Activity Detection: " + mGoogleApiClient);
        // Remove activity updates
        com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(
                mGoogleApiClient,
                getActivityDetectionPendingIntent());
        // Disconnect the google api client
        mGoogleApiClient.disconnect();
    }

    /**
     * Creates a pending intent for this class for receiving activity results.
     */
    private PendingIntent getActivityDetectionPendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mActivityDetectionPendingIntent != null) {
            Log.d(TAG, "Pending intent was null");
            return mActivityDetectionPendingIntent;
        }
        Log.d(TAG, "Creating new Pending Intent");
        Intent intent = new Intent(context, GenericIntentService.class);
        intent.putExtra("INTENT_TYPE", INTENT_TYPE);
        PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mActivityDetectionPendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mActivityDetectionPendingIntent;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "GoogleApiClient onConnected: " + mGoogleApiClient);
        com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(
                mGoogleApiClient,
                Constants.DETECTION_INTERVAL_IN_MILLISECONDS,
                getActivityDetectionPendingIntent()
        ).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "GoogleApiClient onConnectionSuspended: " + mGoogleApiClient);
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.w(TAG, "GoogleApiClient onConnectionFailed!");
    }

    @Override
    public void onResult(Status status) {
    }
}
