/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 * Binder for Dynamix Framework Updates.
 * 
 * @author Darren Carlson
 * 
 */
@Element(name = "DynamixUpdates")
public class DynamixUpdatesBinder {
	@Attribute(required = false)
	String version;
	@Attribute(required = false)
	Boolean update;
	@Attribute(required = false)
	Boolean updateRequired;
	@Attribute(required = false)
	String updateMessage;
	@Attribute(required = false)
	String updateUrl;
	@ElementList(entry = "trustedWebConnectorCerts", required = false)
	List<TrustedCertBinder> trustedWebConnectorCerts;
	@ElementList(entry = "trustedRepos", required = false)
	List<TrustedRepoBinder> trustedRepos;

	public String getVersion() {
		if (version != null)
			return version;
		else
			return "";
	}

	/**
	 * Returns true if Dynamix has an update; false otherwise.
	 */
	public Boolean hasUpdate() {
		if (update != null)
			return update;
		else
			return false;
	}

	/**
	 * Returns true if the update is required; false otherwise.
	 */
	public Boolean isUpdateRequired() {
		if (updateRequired != null)
			return updateRequired;
		else
			return false;
	}

	/**
	 * Returns the update message, if hasUpdate() is true.
	 */
	public String getUpdateMessage() {
		if (updateMessage != null)
			return updateMessage;
		else
			return "";
	}

	/**
	 * Gets the update url
	 */
	public String getUpdateUrl() {
		if (updateUrl != null)
			return updateUrl;
		else
			return "";
	}

	/**
	 * Returns an updates list of trusted web connecter certs.
	 */
	public List<TrustedCertBinder> getTrustedWebConnectorCerts() {
		return trustedWebConnectorCerts;
	}

	/**
	 * Returns an updated list of trusted repositories.
	 */
	public List<TrustedRepoBinder> getTrustedRepos() {
		return trustedRepos;
	}
}
