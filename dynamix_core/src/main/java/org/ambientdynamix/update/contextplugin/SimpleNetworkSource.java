/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants.PLATFORM;
import org.ambientdynamix.util.Repository;
import org.ambientdynamix.web.WebUtils;
import org.apache.commons.validator.routines.UrlValidator;

import android.util.Log;

/**
 * Handles plug-in discovery from specific repository sources. This class does not support Dynamix boot-strapping.
 * 
 * @author Darren Carlson
 */
public class SimpleNetworkSource extends SimpleSourceBase implements IContextPluginConnector, Serializable {
	// Private data
	private static final long serialVersionUID = 876374968867546657L;
	private final String TAG = this.getClass().getSimpleName();
	private List<Repository> repositoryServers = new ArrayList<Repository>();
	private boolean cancel;
	private String[] schemes = { "http", "https", "file", "content" }; // DEFAULT schemes = "http", "https", "ftp"
	private UrlValidator urlValidator = new UrlValidator(schemes);

	public SimpleNetworkSource() {
	}

	/**
	 * Creates a SimpleNetworkSource using the list of repository servers. The repo will use the repository servers in
	 * the List *in order* (0, 1, 2,... n), failing over if necessary to the next in the list.
	 * 
	 * @param repositoryServers
	 *            A list of repository URLs.
	 */
	public SimpleNetworkSource(List<Repository> repositoryServers) {
		if (repositoryServers != null && repositoryServers.size() > 0)
			this.repositoryServers.addAll(repositoryServers);
		else
			throw new RuntimeException("Null or empty repo list!");
	}

	/**
	 * Creates a SimpleNetworkSource using the incoming repository server.
	 * 
	 * @param repositoryServer
	 *            The URL of the repository server.
	 */
	public SimpleNetworkSource(Repository repositoryServer) {
		if (repositoryServer != null)
			repositoryServers.add(repositoryServer);
		else
			throw new RuntimeException("Null repo!");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cancel() {
		cancel = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		else
			return true;
	}

	/**
	 * Discovers plug-ins using the specified repository URL(s). This class automatically fails-over to the next
	 * repository server in the list if a particular server is unavailable.
	 */
	@Override
	public List<PendingContextPlugin> getContextPlugins(PLATFORM platform, VersionInfo platformVersion,
			VersionInfo frameworkVersion) throws Exception {
		List<PendingContextPlugin> updates = new ArrayList<PendingContextPlugin>();
		cancel = false;
		InputStream stream = null;
		for (Repository repo : repositoryServers) {
			// Create a base url for the repo
			URL repoUrl = new URL(repo.getUrl());
			StringBuilder baseUrl = new StringBuilder();
			baseUrl.append(repoUrl.getProtocol());
			baseUrl.append("://" + repoUrl.getAuthority());
			if (repoUrl.getPath().contains("."))
				baseUrl.append(repoUrl.getPath().substring(0, repoUrl.getPath().lastIndexOf("/")));
			else
				baseUrl.append(repoUrl.getPath());
			if (!baseUrl.toString().endsWith("/"))
				baseUrl.append("/");
			if (cancel)
				break;
			try {
				Log.i(TAG, "Checking for context plug-ins using: " + repo.getAlias());
				Log.i(TAG, "Repository URL is: " + WebUtils.getNoCacheUrl(repo.getUrl()));
				Log.d(TAG, "Repository Base URL is: " + baseUrl);
				URL server = new URL(WebUtils.getNoCacheUrl(repo.getUrl()));
				stream = server.openStream();
				List<PendingContextPlugin> tmp = createDiscoveredPlugins(repo, stream, platform, platformVersion,
						frameworkVersion, false);
				if (cancel)
					break;
				for (PendingContextPlugin update : tmp) {
					if (!updates.contains(update)) {
						/*
						 * Check for valid install url. If the url is invalid, assume it's relative to the baseUrl of
						 * the repo.
						 */
						String installUrl = update.getPendingContextPlugin().getInstallUrl();
						if (!urlValidator.isValid(installUrl)) {
							// Setup install url as relative to the baseUrl of the repo.
							String relativeUrl = baseUrl.toString() + installUrl;
							Log.d(TAG, update + " has invalid install url - assigning relative url to repo: "
									+ relativeUrl);
							update.getPendingContextPlugin().setInstallUrl(relativeUrl);
						}
						// Add to the updates
						updates.add(update);
					}
				}
			} catch (Exception e) {
				Log.w(TAG, e);
				updates.add(new PendingContextPlugin(e.toString()));
			} finally {
				if (stream != null)
					stream.close();
			}
		}
		return updates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.getClass().hashCode();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getLastModified() {
		return UpdateUtils.getLastModified(WebUtils.getNoCacheUrl(repositoryServers.get(0).getUrl()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getConnectorId() {
		// We're using the id of the first repo as the representative
		return repositoryServers.get(0).getUrl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getConnectorId() + ", enabled=" + repositoryServers.get(0).isEnabled();
	}
}