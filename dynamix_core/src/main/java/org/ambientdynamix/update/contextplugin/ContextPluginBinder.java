/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextType;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.AndroidFeatureInfo;
import org.ambientdynamix.api.contextplugin.ContextPluginDependency;
import org.ambientdynamix.api.contextplugin.PluginConstants.PLATFORM;
import org.ambientdynamix.api.contextplugin.PluginConstants.UpdatePriority;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.Utils;
import org.ambientdynamix.util.Repository;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import android.util.Log;

/**
 * Simple Framework binder class for XML parsing.
 * 
 * @author Darren Carlson
 * 
 */
@Element(name = "contextPlugin")
public class ContextPluginBinder {
	private String TAG = getClass().getSimpleName();
	@Attribute(required = false)
	String metadataVersion;
	@Attribute(required = false)
	String repoType;
	@Attribute
	String id;
	@Attribute
	String pluginVersion;
	@Attribute(required = false)
	String pluginType;
	@Attribute
	String provider;
	@Attribute
	String platform;
	@Attribute
	String minPlatformVersion;
	@Attribute(required = false)
	String maxPlatformVersion;
	@Attribute(required = false)
	String minFrameworkVersion; // Maintain for older metadata
	@Attribute(required = false)
	String minDynamixVersion; // Update to 'minFrameworkVersion'
	@Attribute(required = false)
	String maxFrameworkVersion; // Maintain for older metadata
	@Attribute(required = false)
	String maxDynamixVersion; // Update to 'maxFrameworkVersion'
	@Attribute(required = false)
	boolean requiresConfiguration;
	@Attribute(required = false)
	String runtimeFactoryClass;
	@Element
	String name;
	@Element
	String description;
	@ElementList(entry = "supportedContextTypes")
	List<ContextTypeBinder> supportedContextTypes;
	@ElementList(entry = "usesFeature", required = false)
	List<FeatureDependencyBinder> featureDependencies;
	@ElementList(entry = "permissions", required = false)
	List<String> permissions;
	@ElementList(entry = "dynamixFeatures", required = false)
	List<DynamixFeatureBinder> dynamixFeatures;
	@Element(required = false)
	UpdateMessageBinder updateMessage;
	@Element(required = false)
	String installUrl;
	@Element(required = false)
	String updateUrl;
	@ElementList(entry = "contextPluginDependency", required = false)
	List<PluginDependencyBinder> contextPluginDependencies;
	@Attribute(required = false)
	boolean dynamixManaged = true;
	@Attribute(required = false)
	boolean backgroundService = false;

	public PendingContextPlugin createPendingPlugin(Repository source) throws Exception {
		ContextPlugin newPlug = new ContextPlugin();
		Map<String, String> extras = new HashMap<String, String>();
		extras.put(ContextPluginInformation.EXTRAS_DYNAMIX_REPO, Boolean.toString(source.isDynamixRepo()));
		newPlug.setExtras(extras);
		newPlug.setId(id);
		newPlug.setRepoSource(source);
		newPlug.setTargetPlatform(PLATFORM.getPlatformFromString(platform));
		newPlug.setVersionInfo(VersionInfo.createVersionInfo(pluginVersion));
		newPlug.setMinPlatformVersion(VersionInfo.createVersionInfo(minPlatformVersion));
		// Check for optional max platform
		if (maxPlatformVersion != null && maxPlatformVersion.length() > 0)
			newPlug.setMaxPlatformVersion(VersionInfo.createVersionInfo(maxPlatformVersion));
		// Handle minimum Dynamix version
		VersionInfo minDynamix = null;
		if (minFrameworkVersion != null)
			minDynamix = VersionInfo.createVersionInfo(minFrameworkVersion);
		if (minDynamixVersion != null)
			minDynamix = VersionInfo.createVersionInfo(minDynamixVersion);
		if (minDynamix == null)
			minDynamix = new VersionInfo(0, 0, 0);
		newPlug.setMinDynamixVersion(minDynamix);
		// Handle minimum Dynamix version
		if (maxFrameworkVersion != null && maxFrameworkVersion.length() > 0)
			newPlug.setMaxFrameworkVersion(VersionInfo.createVersionInfo(maxFrameworkVersion));
		if (maxDynamixVersion != null && maxDynamixVersion.length() > 0)
			newPlug.setMaxFrameworkVersion(VersionInfo.createVersionInfo(maxDynamixVersion));
		newPlug.setProvider(provider);
		newPlug.setName(name);
		newPlug.setDescription(description);
		// Set initial type
		ContextPluginType type = ContextPluginType.NORMAL;
		try {
			// Check for specified type
			if (pluginType != null)
				type = Utils.getEnumFromString(ContextPluginType.class, pluginType);
		} catch (Exception e) {
			// Log.w(TAG, "Could not find plug-in type for: " + pluginType + ". Defaulting to type NORMAL.");
		}
		newPlug.setContextPluginType(type);
		if (newPlug.getContextPluginType() != ContextPluginType.LIBRARY) {
			if (runtimeFactoryClass == null)
				throw new Exception("Missing runtime factory class");
		}
		newPlug.setRequiresConfiguration(requiresConfiguration);
		newPlug.setRuntimeFactoryClass(runtimeFactoryClass);
		// Setup supported context types
		List<ContextType> contextTypes = new ArrayList<ContextType>();
		for (ContextTypeBinder ctb : supportedContextTypes) {
			if (ctb.id != null) {
				contextTypes.add(new ContextType(ctb.id, ctb.name, ctb.description));
			} else {
				contextTypes.add(new ContextType(ctb.description, "Name not found (update xml)",
						"Description not found (update xml)"));
			}
		}
		newPlug.setSupportedContextTypes(contextTypes);
		if (dynamixFeatures != null) {
			List<DynamixFeature> features = new ArrayList<DynamixFeature>();
			for (DynamixFeatureBinder f : dynamixFeatures) {
				DynamixFeature tmp = new DynamixFeature(newPlug.getId(), newPlug.getVersion(), f.name, f.description,
						f.runtimeMethod, f.categories);
				features.add(tmp);
			}
			newPlug.setDynamixFeatures(features);
		}
		newPlug.setPermissions(new LinkedHashSet<Permission>());
		if (permissions == null || permissions.size() == 0) {
			Log.v(TAG, "No permissions required for " + newPlug.getId());
		} else {
			// Setup permissions
			for (String permissionString : permissions) {
				Permission p = Permission.createPermission(permissionString);
				if (p != null) {
					// TODO: For now we grant all permissions - update this
					p.setPermissionGranted(true);
					newPlug.getPermissions().add(p);
					Log.v(TAG, "For testing, we are automatically granting Permission: " + permissionString + " to "
							+ newPlug);
				}
			}
		}
		// Setup feature dependencies
		newPlug.setFeatureDependencies(new LinkedHashSet<AndroidFeatureInfo>());
		if (featureDependencies != null)
			for (FeatureDependencyBinder f : featureDependencies) {
				newPlug.getFeatureDependencies().add(new AndroidFeatureInfo(f.name, f.required));
			}
		newPlug.setInstallUrl(installUrl);
		newPlug.setUpdateUrl(updateUrl);
		// Set the plug-in's dependencies
		if (contextPluginDependencies != null) {
			List<ContextPluginDependency> dep = new ArrayList<ContextPluginDependency>();
			for (PluginDependencyBinder p : contextPluginDependencies) {
				if (p.pluginVersion != null) {				
					 dep.add(new ContextPluginDependency(p.pluginId, VersionInfo.createVersionInfo(p.pluginVersion),
					 p.pluginName, p.installUrl));
				} else {					
					dep.add(new ContextPluginDependency(p.pluginId, null, p.pluginName, p.installUrl));
				}
			}
			newPlug.setPluginDependencies(dep);
		} else
			Log.v(TAG, "No Dependencies for " + newPlug.getId());
		newPlug.setDynamixManaged(dynamixManaged);
		newPlug.setBackgroundService(backgroundService);
		// Validate the plugin and add it to the list of ContextPluginUpdates
		Log.v(TAG, "Validating new plug-in: " + newPlug);
		if (Utils.validateContextPlugin(newPlug)) {
			Log.v(TAG, "Plug-in is valid: " + newPlug);
			if (updateMessage != null) {
				String messsage = updateMessage.message;
				UpdatePriority priority = UpdatePriority.valueOf(updateMessage.priority);
				return new PendingContextPlugin(newPlug, messsage, priority);
			} else
				return new PendingContextPlugin(newPlug);
		} else
			throw new Exception("Context Plugin Invalid");
	}
}
