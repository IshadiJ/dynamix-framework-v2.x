/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.security;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import android.util.Log;
import org.ambientdynamix.core.DynamixPreferences;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

/**
 * Various encryption utilities. Code collected from around the web (see comments).
 */
public class CryptoUtils {
	private static String TAG = CryptoUtils.class.getSimpleName();
	private static String PUBLIC_KEY_HEADER = "-----BEGIN PUBLIC KEY-----";
	private static String PUBLIC_KEY_FOOTER = "-----END PUBLIC KEY-----";
	private static String PRIVATE_KEY_HEADER = "-----BEGIN PRIVATE KEY-----";
	private static String PRIVATE_KEY_FOOTER = "-----END PRIVATE KEY-----";
	private static String RSA_CONFIGURATION = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
	private static String RSA_PROVIDER = "BC";
	// private static String AES_CONFIGURATION = "AES/ECB/PKCS7Padding";
	private static String AES_CONFIGURATION = "AES/CBC/PKCS5Padding";
	private static String AES_PROVIDER = "BC";

	public static SecretKey generateAesKey(int bitLength) throws Exception {
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(bitLength);
		return keyGen.generateKey();
	}

	public static String createStringFromAESKey(SecretKey masterKey) {
		return Base64.encodeToString(masterKey.getEncoded(), Base64.NO_WRAP);
	}

	/*
	 * https://sites.google.com/site/clementwiki/software-development/aes-crytography-on-android-and-java
	 * http://www.androidsnippets.com/encryptdecrypt-strings
	 * http://www.developer.com/ws/android/encrypting-with-android-cryptography-api.html
	 * http://crypto.stackexchange.com/questions/12730/at-what-stage-is-dhe-and-rsa-used-during-the-ssl-tls-handshake
	 */
	public static String encryptStringAES(byte[] key, byte[] iv, String clearText) throws Exception {
		byte[] encodedBytes = encryptAESBytes(key, iv, clearText.getBytes("UTF-8"));
		return bytesToHex(encodedBytes);
	}

	public static String encryptStringAES(String key, String iv, String clearText) throws Exception {
		byte[] encodedBytes = encryptAESBytes(Base64.decode(key.getBytes("UTF-8"), Base64.NO_WRAP),
				Base64.decode(iv.getBytes("UTF-8"), Base64.NO_WRAP), clearText.getBytes("UTF-8"));
		return bytesToHex(encodedBytes);
	}

	public static byte[] encryptAESBytes(byte[] key, byte[] iv, byte[] clear) throws Exception {
		Cipher cipher = Cipher.getInstance(AES_CONFIGURATION, AES_PROVIDER);
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
		return cipher.doFinal(clear);
	}

	public static String decryptAesBase64String(byte[] key, byte[] iv, String ciphertext) throws Exception {
		byte[] hexByteArray = hexStringToByteArray(ciphertext);
		if (hexByteArray != null) {
			byte[] decodedBytes = decryptAESBytes(key, iv, hexByteArray);
			return new String(decodedBytes, "UTF-8");
		} else
			return null;
	}

	public static byte[] decryptAESBytes(byte[] key, byte[] iv, byte[] coded) throws Exception {
		Cipher cipher = Cipher.getInstance(AES_CONFIGURATION, AES_PROVIDER);
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
		return cipher.doFinal(coded);
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		if (len % 2 == 0) {
			byte[] data = new byte[len / 2];
			for (int i = 0; i < len; i += 2) {
				data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
			}
			return data;
		} else {
			Log.w(TAG, "hexStringToByteArray found improper string length: " + len);
			return null;
		}
	}

	/*
	 * Generate a new entry in the KeyStore by using the KeyPairGenerator API. We have to specify the attributes for a
	 * self-signed X.509 certificate here so the KeyStore can attach the public key part to it. It can be replaced later
	 * with a certificate signed by a Certificate Authority (CA) if needed. Code below requires Android 4.3 (API level
	 * 18) and higher
	 */
	// public static KeyPair generateRsaKeyPairForKeystore(int keySize, Context context, String alias) throws Exception
	// {
	// Calendar cal = Calendar.getInstance();
	// Date now = cal.getTime();
	// cal.add(Calendar.YEAR, 1);
	// Date end = cal.getTime();
	// KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
	// kpg.initialize(new KeyPairGeneratorSpec.Builder(context).setAlias(alias).setStartDate(now).setEndDate(end)
	// .setSerialNumber(BigInteger.valueOf(1)).setSubject(new X500Principal("CN=test1")).setKeySize(keySize)
	// .build());
	// KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
	// ks.load(null);
	// ArrayList<String> aliases = Collections.list(ks.aliases());
	// for (String aliase : aliases) {
	// Log.d(TAG, "generated key: " + aliase);
	// }
	// return kpg.generateKeyPair();
	// }
	public static KeyPair generateRsaKeyPair(int keySize) throws Exception {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(keySize);
		return kpg.generateKeyPair();
	}

	public static PrivateKey getDynamixPrivateRsaKeyFromKeystore() throws Exception {
		KeyStore.PrivateKeyEntry privateKeyEntry = getPrivateKeyEntryFromKeystore();
		if (privateKeyEntry == null)
			return null;
		return privateKeyEntry.getPrivateKey();
	}

	public static PublicKey getDynamixPublicRsaKeyFromKeystore() throws Exception {
		KeyStore.PrivateKeyEntry privateKeyEntry = getPrivateKeyEntryFromKeystore();
		if (privateKeyEntry == null)
			return null;
		return privateKeyEntry.getCertificate().getPublicKey();
	}

	private static KeyStore.PrivateKeyEntry getPrivateKeyEntryFromKeystore() throws KeyStoreException, IOException,
			NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
		KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
		ks.load(null);
		KeyStore.Entry entry = ks.getEntry(DynamixPreferences.DYNAMIX_KEY, null);
		if (!ks.entryInstanceOf(DynamixPreferences.DYNAMIX_KEY, KeyStore.PrivateKeyEntry.class)) {
			Log.w(TAG, "Not an instance of a PrivateKeyEntry");
			return null;
		}
		KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) entry;
		return privateKeyEntry;
	}

	public static void removeRSAKeyPairFromKeystore(String alias) throws KeyStoreException, CertificateException,
			NoSuchAlgorithmException, IOException {
		KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
		ks.load(null);
		ks.deleteEntry(alias);
	}

	public static PublicKey createPublicKeyFromString(String publicKeyString) throws Exception {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		publicKeyString = publicKeyString.replace(PUBLIC_KEY_HEADER, "");
		publicKeyString = publicKeyString.replace(PUBLIC_KEY_FOOTER, "");
		return keyFactory.generatePublic(new X509EncodedKeySpec(Base64.decode(publicKeyString, Base64.NO_WRAP)));
	}

	public static PrivateKey createPrivateKeyFromString(String privateKeyString) throws Exception {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		privateKeyString = privateKeyString.replace(PRIVATE_KEY_HEADER, "");
		privateKeyString = privateKeyString.replace(PRIVATE_KEY_FOOTER, "");
		return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(Base64.decode(privateKeyString, Base64.NO_WRAP)));
	}

	public static String encryptRsa(Key key, String clearText) throws Exception {
		Cipher c = Cipher.getInstance(RSA_CONFIGURATION, RSA_PROVIDER);
		c.init(Cipher.ENCRYPT_MODE, key, new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256,
				PSource.PSpecified.DEFAULT));
		byte[] encodedBytes = Base64.encode(c.doFinal(clearText.getBytes("UTF-8")), Base64.DEFAULT);
		String cypherText = new String(encodedBytes, "UTF-8");
		return cypherText;
	}

	public static String decryptRsa(Key key, String base64cypherText) throws Exception {
		Cipher c = Cipher.getInstance(RSA_CONFIGURATION, RSA_PROVIDER);
		c.init(Cipher.DECRYPT_MODE, key, new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256,
				PSource.PSpecified.DEFAULT));
		byte[] decodedBytes = c.doFinal(Base64.decode(base64cypherText.getBytes("UTF-8"), Base64.DEFAULT));
		String clearText = new String(decodedBytes, "UTF-8");
		return clearText;
	}

	public static void decryptTest() throws Exception {
		String publicKeyString = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApoGmGNRXR2Brrh8wrme27sQAgzO3P/zaZrcM85ZToTwdHTFAuWNDY2BHxERmkFIkMQg/F1NZNywD0ya9KCdoMLB/yLTygKbajuNfUnC9P0pV2kWEl2obrMgYVHVgHgwrCl8F0lBZ+8CWlb005AuBCjs7jColPV1iGGf8vYt3NJaw0xzpKPrtYx0uwBI5GCPH/rGahTVTVojqoz+DP5yAqVB0ay72uPaq9w/k3y0/PwebQqOXLRnmI1uJHlQ4HAXuQlK14USmYFjq48U7ag1vhAGZYigNuDVskvDuOtRK2+VnOe5oLPUZ1kYOA0DThg1A6BUnC85ahoIzH/x9tGwK7QIDAQAB-----END PUBLIC KEY-----";
		Key publicKey = createPublicKeyFromString(publicKeyString);
		String privateKeyString = "-----BEGIN PRIVATE KEY-----MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmgaYY1FdHYGuuHzCuZ7buxACDM7c//NpmtwzzllOhPB0dMUC5Y0NjYEfERGaQUiQxCD8XU1k3LAPTJr0oJ2gwsH/ItPKAptqO419ScL0/SlXaRYSXahusyBhUdWAeDCsKXwXSUFn7wJaVvTTkC4EKOzuMKiU9XWIYZ/y9i3c0lrDTHOko+u1jHS7AEjkYI8f+sZqFNVNWiOqjP4M/nICpUHRrLva49qr3D+TfLT8/B5tCo5ctGeYjW4keVDgcBe5CUrXhRKZgWOrjxTtqDW+EAZliKA24NWyS8O461Erb5Wc57mgs9RnWRg4DQNOGDUDoFScLzlqGgjMf/H20bArtAgMBAAECggEAPIx48h6Ffpc5kSAWe9WLWFg+N3fe843nfeKi+xF3Z/KMQ8ldWA0YvFKOxnh5M5IrRwlQqBLPJpkC6w3zS17JLKuCTGJArTf/0mYsi7Yjm5hUkAMnrEgRuEsrTctBIKwcGrFjVI+TqVDncZxUV3k3e5j+loHc+Ou0eFuYNPU6bAyqKF8+8crBLID26t37p+DioKVjcmDui12CJh+VIM06nborCzP4h/G9DK6drFOURj3DiucuOOBWHdrlTaEDnavdACoq4tC11sgaC0RXw4CzcGzA4mycEX+pQXlsu017q53aDM578kgc/KDDp+ugQZnFYRIROAnxN3v+RLaO/GvboQKBgQDOd65NJy+NG8OJs+Fgmpoq/RNsYupP7kjOi+zBORh6rYyJy24RpL15Gcye9iqUEtcmjITniP9b1pGZhBqr8LGAuHRuEPxIjdtLVBYNNW5N5V5igMt7yyIcsA3x2OstaiDHB96mLyGjVeFCHd8iS/m1h2qSEPvLJHYBlClNXZPqJwKBgQDOc7yPRwopodEOtNQNRwGNLYfLDbUiCjGzCigZasbCECqZs51zFljv4h8ntyedO0q7mbtrUnskUI8fYGigYMlxRsMXovTUrPWUWoIIOVfRFivANozLsdOEz1nEY1kDZPKKQOwsMuPiYoM24Km0La4Q+c7oZmEG0TD/cEYhRMRyywKBgQCdzNj5/M4aEjMzxrilpePYoU48E6mz55Hb0xPjZijwcj4sfr+kWn251d8PS2OlgnTP8KwPbiSFY4wqgc4ExdRUZiYOjYrURR9rBlxIQiMJBBpMPNXIyqtKs8AkrL6qGiftFnW0G+egt52Pc9c7lkfG1zW7Z6GaVEag1EVw7OmFFwKBgBKbpew4F/dmqWQs8kv8EWy7JNktdB0MVCxyBuk7kYyeTmiqIs/jvMJqXCqFPKHUXUkJczYaKhU82ZmXY/bIon9+RElWG8Erkbr8aWn6WSr0V58si4Y0kf8PfWTRG43NxHqnnWHieF3ISVFh8InX8C+BhJ+30pj9s+/iG8AV5ar/AoGAWfUH97pXmXqD9E181GZkh+hbpS/26AIJDFMSjoI938vSbewj8cwn00abFxluL67dFIusKYOuFxu32E7k4ns6llM5LRhSan/cRq324vVLE6bSEi88av2a17LwXu63alGIKH5AB89IZrQcte1li7tPNgg31ivRnqzbeVE4kC5zhWE=-----END PRIVATE KEY-----";
		Key privateKey = createPrivateKeyFromString(privateKeyString);
		Log.i(TAG,
				"RSA Test: "
						+ decryptRsa(
								privateKey,
								"g1VMKj3GRjf+bZkhysdTIHTO24Cl5tVClUSWb2Zg35mSq9k64lUHPdksQq9b8CyB/U2JliCUfAcrfJKYXygeJPbrpIUopRbF4BxFPV2EdOcI98qNK6uTj6bk93zakzF6qbKNx0vLE9FbBknzYpBK8UKOcD3Uu/c2ssZDExZMCBwUXhhMFZv7rt9aXGMGGwYoH26U2xdwRCQAWI0qDB+W+oviM0G0crPQMlLI4AYdYzPhq3ItFDYtXrrbi91nJYzSZtM1cb+ummhmGuL49NohiwZI6m3I/cuXhULP/QkuHZrNgjFlXPMVrC1kA19zB494GuXGwqZSARqVLtx8zAYxtA=="));
		String aesKey = "i2zLQ9yiBAw8+817//Czhw==";
		byte[] aesKeyBytes = Base64.decode(aesKey, Base64.NO_WRAP);
		String aesCleartext = "This is cleartext for the AES test.";
		String aesEncryptedString = encryptStringAES(aesKeyBytes, aesKeyBytes, aesCleartext);
		String aesDecryptedString = decryptAesBase64String(aesKeyBytes, aesKeyBytes, aesEncryptedString);
		Log.i(TAG, "AES Encrypted String: " + aesEncryptedString);
		Log.i(TAG, "AES Decrypted String: " + aesDecryptedString);
		String hex = "cc28bc142647666751843130857bf10a68ab4de72d577bd8fc24f1953536ade5";
		byte[] dHex = decryptAESBytes(aesKeyBytes, aesKeyBytes, hexStringToByteArray(hex));
		String dString = new String(dHex, "UTF-8");
		Log.i(TAG, "AES Decrypted Hex String: " + dString);
		String finalTest = "If this works, we've got aes fully working in both js and java!";
		String encodedFinalString = encryptStringAES(aesKeyBytes, aesKeyBytes, finalTest);
		Log.i(TAG, encodedFinalString);
		String testAgain = "uLjB/9WbtH86mBftb585LqgWwqyJ+HNMuUJIQKJWdZSRmwCSji4fSF/FMFRNRcOItGp8RlbpFMW23Qh/sTMD/w==";
		String decodedAgain = decryptAesBase64String(aesKeyBytes, aesKeyBytes, testAgain);
		Log.i(TAG, "Again: " + decodedAgain);
	}

	/*
	 * http://www.developer.com/ws/android/encrypting-with-android-cryptography-api.html
	 */
	public static void encryptionTest() throws Exception {
		// Generate RSA KeyPair
		String clearText = "This is cleartext, and it's amazing!";
		Log.i(TAG, "Started generating RSA Key-pair: " + new Date());
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(2048);
		KeyPair kp = kpg.genKeyPair();
		Key publicKey = kp.getPublic();
		Key privateKey = kp.getPrivate();
		Log.i(TAG, "Finished generating RSA Key-pair: " + new Date());
		// Log RSA public Key
		String publicKeyString = createStringFromPublicKey(publicKey);
		Log.i(TAG, "Public Key (Base64): " + publicKeyString);
		// Log RSA Private Key
		String privateKeyString = createStringFromPrivateKey(privateKey);
		Log.i(TAG, "Private Key (Base64): " + privateKeyString);
		// RSA Encrypt using key reconstructed from the publicKeyString
		Key reconstructedPublicKey = createPublicKeyFromString(publicKeyString);
		Cipher c = Cipher.getInstance(RSA_CONFIGURATION, RSA_PROVIDER);
		c.init(Cipher.ENCRYPT_MODE, reconstructedPublicKey);
		byte[] encodedBytes = Base64.encode(c.doFinal(clearText.getBytes("UTF-8")), Base64.NO_WRAP);
		String cypherText = new String(encodedBytes, "UTF-8");
		Log.i(TAG, "RSA Generated cypherText: " + cypherText);
		// RSA Decrypt using key reconstructed from the privateKeyString
		Key reconstructedPrivateKey = createPrivateKeyFromString(privateKeyString);
		c.init(Cipher.DECRYPT_MODE, reconstructedPrivateKey);
		byte[] decodedBytes = c.doFinal(Base64.decode(cypherText.getBytes("UTF-8"), Base64.NO_WRAP));
		String decodedString = new String(decodedBytes, "UTF-8");
		Log.i(TAG, "RSA Decoded String: " + decodedString);
		// AES Key Generation
		// Cipher cipher = Cipher.getInstance(AES_CONFIGURATION, AES_PROVIDER);
		String aesKeyString = Base64.encodeToString(getSHA1bytes("This is a test!", 16), Base64.NO_WRAP);
		// SecretKeySpec aesKey = new SecretKeySpec(getSHA1bytes("This is a test!", 16), "AES");
		Log.i(TAG, "AES Base64 Encoded 128 Bit Key: " + aesKeyString);
		// AES Encrypt
		// cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		// byte[] aesEncodedBytes = cipher.doFinal(clearText.getBytes("UTF-8"));
		// String aesCypherText = Base64.encodeToString(aesEncodedBytes, Base64.NO_WRAP);
		String aesCypherText = encryptStringAES(Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP),
				Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP), clearText);
		Log.i(TAG, "AES Cleartext Message: " + clearText);
		Log.i(TAG, "AES Encrypted Message: " + aesCypherText);
		// AES Decrypt
		String aesDecryptedText = decryptAesBase64String(Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP),
				Base64.decode(aesKeyString.getBytes("UTF-8"), Base64.NO_WRAP), aesCypherText);
		Log.i(TAG, "AES Decrypted Message: " + aesDecryptedText);
	}

	public void ListSupportedAlgorithms() {
		String result = "";
		// get all the providers
		Provider[] providers = Security.getProviders();
		for (int p = 0; p < providers.length; p++) {
			// get all service types for a specific provider
			Set<Object> ks = providers[p].keySet();
			Set<String> servicetypes = new TreeSet<String>();
			for (Iterator<Object> it = ks.iterator(); it.hasNext();) {
				String k = it.next().toString();
				k = k.split(" ")[0];
				if (k.startsWith("Alg.Alias."))
					k = k.substring(10);
				servicetypes.add(k.substring(0, k.indexOf('.')));
			}
			// get all algorithms for a specific service type
			int s = 1;
			for (Iterator<String> its = servicetypes.iterator(); its.hasNext();) {
				String stype = its.next();
				Set<String> algorithms = new TreeSet<String>();
				for (Iterator<Object> it = ks.iterator(); it.hasNext();) {
					String k = it.next().toString();
					k = k.split(" ")[0];
					if (k.startsWith(stype + "."))
						algorithms.add(k.substring(stype.length() + 1));
					else if (k.startsWith("Alg.Alias." + stype + "."))
						algorithms.add(k.substring(stype.length() + 11));
				}
				int a = 1;
				for (Iterator<String> ita = algorithms.iterator(); ita.hasNext();) {
					result += ("[P#" + (p + 1) + ":" + providers[p].getName() + "]" + "[S#" + s + ":" + stype + "]"
							+ "[A#" + a + ":" + ita.next() + "]\n");
					a++;
				}
				s++;
			}
		}
	}

	public void generateFrameworkKeys(Context c, String alias) throws Exception {
		/*
		 * Generate a new entry in the KeyStore by using the KeyPairGenerator API. We have to specify the attributes for
		 * a self-signed X.509 certificate here so the KeyStore can attach the public key part to it. It can be replaced
		 * later with a certificate signed by a Certificate Authority (CA) if needed.
		 */
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		cal.add(Calendar.YEAR, 10);
		Date end = cal.getTime();
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
		// Note: requires API level 18
		// kpg.initialize(new KeyPairGeneratorSpec.Builder(c).setAlias(alias).setStartDate(now).setEndDate(end)
		// .setSerialNumber(BigInteger.valueOf(1)).setSubject(new X500Principal("CN=test1")).build());
		KeyPair kp = kpg.generateKeyPair();
	}

	public static String createStringFromPublicKey(Key publicKey) throws Exception {
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
		return PUBLIC_KEY_HEADER + new String(Base64.encode(x509EncodedKeySpec.getEncoded(), Base64.NO_WRAP), "UTF-8")
				+ PUBLIC_KEY_FOOTER;
	}

	public static String createStringFromPrivateKey(Key privateKey) throws Exception {
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
		return PRIVATE_KEY_HEADER
				+ new String(Base64.encode(pkcs8EncodedKeySpec.getEncoded(), Base64.NO_WRAP), "UTF-8")
				+ PRIVATE_KEY_FOOTER;
	}

	public static byte[] getSHA1bytes(String hashMe, int bytes) throws Exception {
		byte[] key = hashMe.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		return Arrays.copyOf(key, 16);
	}

	public static String shaHash(String value, String algorithm) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(value.getBytes());
		byte byteData[] = md.digest();
		return bytesToHex(byteData);
		// String base64 = Base64.encodeToString(byteData, Base64.NO_WRAP);
		// return base64;
	}

	final private static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}