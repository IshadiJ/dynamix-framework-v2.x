package org.ambientdynamix.util;

/**
 * Created by maxpagel on 20.03.15.
 */
public interface GenericCallback<T> {
    public void onSuccess(T result);
    public void onFailure(String message);
}
