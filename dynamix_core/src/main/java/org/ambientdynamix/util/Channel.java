/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.util;

import java.util.List;

import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.core.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication;

/**
 * Represents a communication channel between an app and a handler (and possibly an associated listener).
 * 
 * @author Darren Carlson
 */
public class Channel {
	// Private data
	private DynamixApplication app;
	private IContextHandler handler;
	private String contextType;
	private List<ContextPlugin> plugins;
	private Type type;

	public enum Type {
		CONTEXT_REQUEST, CONTEXT_SUBSCRIPTION
	}

	/**
	 * Creates a channel for the specified DynamixApplication.
	 */
	public Channel(DynamixApplication app, IContextHandler handler, List<ContextPlugin> plugins, String contextType,
			Type type) {
		this.app = app;
		this.handler = handler;
		this.plugins = plugins;
		this.contextType = contextType;
		this.type = type;
	}

	/**
	 * Returns the Channel type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Returns the context type associated with the request.
	 */
	public String getContextType() {
		return contextType;
	}

	/**
	 * Returns the DynamixApplication that made the request.
	 */
	public DynamixApplication getApp() {
		return app;
	}

	/**
	 * Returns the IContextHandler that made the request.
	 */
	public IContextHandler getContextHandler() {
		return handler;
	}

	/**
	 * Returns the Context Plug-ins handling this context request.
	 */
	public List<ContextPlugin> getAssociatedContextPlugins() {
		return this.plugins;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + app.hashCode() + handler.hashCode() + contextType.hashCode();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		Channel other = (Channel) candidate;
		return this.app.equals(other.app) && handler.asBinder().equals(other.handler.asBinder())
				&& this.contextType.equalsIgnoreCase(other.contextType);
	}
}
