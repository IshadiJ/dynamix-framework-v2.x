/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.RepositoryInfo;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.adapters.PendingPluginsAdapter;
import org.ambientdynamix.ui.dataobject.PendingPluginListItemType;
import org.ambientdynamix.ui.dataobject.PluginsListViewItem;
import org.ambientdynamix.update.contextplugin.PluginInstallCallback;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class PendingPluginsFragment extends Fragment {
	Activity activity;
	private String LOGTAG = getClass().getSimpleName();
	private ArrayList<RepositoryInfo> pluginRepositories;
	private ArrayList<ContextPluginInformation> pendingContextPlugins;
	private HashMap<RepositoryInfo, ArrayList<ContextPluginInformation>> contextPluginInformationMap;
	final private ArrayList<PluginsListViewItem> pendingPluginListItems = new ArrayList<PluginsListViewItem>();
	PendingPluginsAdapter pendingPluginsAdapter;
	HashMap<ContextPluginInformation, Integer> installProgressMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layoutView = inflater.inflate(R.layout.pending_plugins_fragment, container, false);
		activity = getActivity();
		pluginRepositories = new ArrayList<RepositoryInfo>();
		pendingContextPlugins = new ArrayList<ContextPluginInformation>();
		contextPluginInformationMap = new HashMap<RepositoryInfo, ArrayList<ContextPluginInformation>>();
		installProgressMap = new HashMap<ContextPluginInformation, Integer>();
		pendingPluginsAdapter = new PendingPluginsAdapter(activity, pendingPluginListItems, installProgressMap);
		ListView listView = (ListView) layoutView.findViewById(R.id.listOfPlugins);
		listView.setAdapter(pendingPluginsAdapter);
		reInitList();
		final Button installButton = (Button) layoutView.findViewById(R.id.install);
		installButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				installButton.setEnabled(false);
				ArrayList<ContextPluginInformation> pluginsToInstall = new ArrayList<ContextPluginInformation>(
						pendingPluginsAdapter.getPluginsToInstall());
				for (ContextPluginInformation plugin : pluginsToInstall) {
					// Initialize with 1 so that the install progress bar shows up.
					installProgressMap.put(plugin, new Integer(1));
				}
				DynamixService.installPlugins(pluginsToInstall, new PluginInstallCallback() {
					@Override
					public void onInstallStarted(ContextPluginInformation plugin) {
						// Do not need to do anything currently. We've already added all plugins to the progress map.
					}

					@Override
					public void onInstallProgress(ContextPluginInformation plugin, int percentComplete) {
						installProgressMap.put(plugin, new Integer(percentComplete));
						notifyDataSetChangedCustom();
					}

					@Override
					public void onInstallFailed(ContextPluginInformation plugin, String errorMessage, int errorCode) {
						installProgressMap.remove(plugin);
						if (installProgressMap.size() == 0) {
							installButton.setEnabled(true);
						}
						showToast("Could not install " + plugin.getPluginName() + " : " + errorMessage);
						notifyDataSetChangedCustom();
						Log.d(LOGTAG, "Failed to install plugin : " + plugin.getPluginName());
					}

					@Override
					public void onInstallComplete(ContextPluginInformation plugin) {
						Log.d(LOGTAG, "Successfully installed plugin : " + plugin.getPluginName());
						installProgressMap.remove(plugin);
						if (installProgressMap.size() == 0) {
							installButton.setEnabled(true);
						}
						notifyDataSetChangedCustom();
					}
				});
			}
		});
		return layoutView;
	}

	public void reInitList() {
		if (DynamixService.isFrameworkInitialized()) {
			synchronized (pendingPluginListItems) {
				pluginRepositories.clear();
				pluginRepositories.addAll(DynamixService.getRepositoryInfo());
				pendingContextPlugins.clear();
				pendingContextPlugins.addAll(DynamixService.getPendingContextPluginInfo());
				contextPluginInformationMap.clear();
				for (RepositoryInfo info : pluginRepositories) {
					contextPluginInformationMap.put(info, new ArrayList<ContextPluginInformation>());
				}
				for (ContextPluginInformation info : pendingContextPlugins) {
					contextPluginInformationMap.get(info.getRepository()).add(info);
				}
			}
			refreshList();
		}
	}

	private void refreshList() {
		Log.i(LOGTAG, "Refreshing Pending Plugins List");
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (DynamixService.isFrameworkInitialized()) {
					synchronized (pendingPluginListItems) {
						pendingPluginListItems.clear();
						// Traverse HashMap and add views accordingly
						for (Map.Entry<RepositoryInfo, ArrayList<ContextPluginInformation>> entry : contextPluginInformationMap
								.entrySet()) {
							ArrayList<ContextPluginInformation> contextPluginInformationArrayList = contextPluginInformationMap
									.get(entry.getKey());
//							Log.d(LOGTAG, "Num of Plugins for " + entry.getKey().getAlias() + " : "
//									+ contextPluginInformationArrayList.size());
							pendingPluginListItems.add(new PluginsListViewItem(PendingPluginListItemType.HEADER_VIEW,
									entry.getKey().getAlias()));
							//Log.d(LOGTAG, "Adding header view ");
							if (contextPluginInformationArrayList.size() == 0) {
								pendingPluginListItems.add(new PluginsListViewItem(
										PendingPluginListItemType.NO_ITEMS_VIEW, null));
								// Log.d(LOGTAG, "Adding No items view ");
							} else {
								for (ContextPluginInformation info : contextPluginInformationArrayList) {
									pendingPluginListItems.add(new PluginsListViewItem(
											PendingPluginListItemType.CONTEXT_PLUGIN_VIEW, info));
									//Log.d(LOGTAG, "Adding context plugin view ");
								}
							}
						}
						notifyDataSetChangedCustom();
					}
				}
			}
		});
	}

	public void onPluginUninstalled(ContextPluginInformation plugin) {
		synchronized (pendingPluginListItems) {
			if (!contextPluginInformationMap.get(plugin.getRepository()).contains(plugin))
				contextPluginInformationMap.get(plugin.getRepository()).add(plugin);
		}
		refreshList();
	}

	public void onPluginInstalled(ContextPluginInformation plugin) {
		synchronized (pendingPluginListItems) {
			if (contextPluginInformationMap.get(plugin.getRepository()).contains(plugin))
				contextPluginInformationMap.get(plugin.getRepository()).remove(plugin);
			pendingPluginsAdapter.onPluginInstalled(plugin);
		}
		refreshList();
	}

	private void showToast(final String message) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			}
		});
	}

	private void notifyDataSetChangedCustom() {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				pendingPluginsAdapter.notifyDataSetChanged();
			}
		});
	}
}
