/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.fragments;

import java.util.ArrayList;

import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.R;
import org.ambientdynamix.ui.adapters.RepositoriesListAdapter;
import org.ambientdynamix.util.Repository;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class RepositoriesFragment extends Fragment {
	public RepositoriesFragment() {
	}

	ArrayList<Repository> repoURLList;
	RepositoriesListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_external_repo_settings, container, false);
		TextView noRepoText = (TextView) rootView.findViewById(R.id.noRepoText);
		ListView externalReposList = (ListView) rootView.findViewById(R.id.externalRepoList);
		repoURLList = new ArrayList<Repository>(DynamixService.getContextPluginRepos());
		if (repoURLList.size() > 0)
			noRepoText.setVisibility(View.GONE);
		else
			noRepoText.setVisibility(View.VISIBLE);
		adapter = new RepositoriesListAdapter(getActivity(), repoURLList);
		externalReposList.setAdapter(adapter);
		return rootView;
	}

	public void addToList(Repository info) {
		repoURLList.add(info);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onPause() {
		super.onPause();
		DynamixService.refreshPluginsFromRepos();
	}
}
