package org.ambientdynamix.ui.dataobject;

/**
 * Created by shivam on 12/15/14.
 */
public class PendingPluginListItemType {
	final public static int HEADER_VIEW = 0;
	final public static int CONTEXT_PLUGIN_VIEW = 1;
	final public static int NO_ITEMS_VIEW = 2;
}