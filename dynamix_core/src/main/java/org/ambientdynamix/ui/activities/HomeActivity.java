/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.core.DynamixApplication;
import org.ambientdynamix.core.DynamixPreferenceActivity;
import org.ambientdynamix.core.DynamixPreferences;
import org.ambientdynamix.core.DynamixService;
import org.ambientdynamix.core.IDynamixFrameworkListener;
import org.ambientdynamix.core.R;
import org.ambientdynamix.core.Utils;
import org.ambientdynamix.ui.adapters.FragmentsListAdapter;
import org.ambientdynamix.util.Eula;
import org.ambientdynamix.util.Eula.IEulaListener;

public class HomeActivity extends FragmentActivity implements IDynamixFrameworkListener, IEulaListener {
    private String LOGTAG = getClass().getSimpleName();
    public static Activity activity;
    private Menu optionsMenu;
    FragmentsListAdapter fragmentsAdapter;
    Boolean pluginRefreshing = false;
    Boolean dynamixFrameworkStateChanging = false;
    private static final int NUM_ITEMS = 4;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_new);
        activity = this;
        // Handle Eula
        Eula.addListener(this);
        Eula.show(this);
        pager = (ViewPager) findViewById(R.id.pager);
        fragmentsAdapter = new FragmentsListAdapter(getSupportFragmentManager());
        pager.setAdapter(fragmentsAdapter);
        pager.setOffscreenPageLimit(NUM_ITEMS);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setIndicatorColor(0xFF8db63d);
        tabs.setUnderlineColor(0xFF666666);
        tabs.setDividerColor(0xFF666666);
        tabs.setShouldExpand(true);
        tabs.setIndicatorHeight(6);
        tabs.setUnderlineHeight(1);
        tabs.setTabPaddingLeftRight(12);
        tabs.setTextColor(0xFFFFFFFF);
        tabs.setViewPager(pager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (DynamixService.isFrameworkInitialized())
            showStartupToast();
    }

    @Override
    public void onEulaAgree() {
        // Clear Eula listeners
        Eula.clearListeners();
        // Boot Dynamix
        DynamixService.addDynamixFrameworkListener(this);
        DynamixService.boot(this, true, false, false);
        if (DynamixService.isFrameworkStarted() && DynamixPreferences.isDynamixEnabled()) {
            reInitFragments();
        }
    }

    @Override
    public void onEulaRefuse() {
        // Clear Eula listeners
        Eula.clearListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        menu.clear();
        // Setup Change Settings
        MenuItem item1 = menu.add(1, Menu.FIRST, Menu.NONE, "Settings");
        item1.setIcon(R.drawable.ic_action_settings);
        // MenuItemCompat.setShowAsAction(item1,
        // MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        item1.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(HomeActivity.this, DynamixPreferenceActivity.class));
                return true;
            }
        });
        MenuItem item2 = menu.add(2, Menu.FIRST + 1, Menu.NONE, "Refresh");
        item2.setIcon(R.drawable.ic_action_refresh);
        if (pluginRefreshing) {
            MenuItemCompat.setActionView(item2, R.layout.actionbar_indeterminate_progress);
            // item2.setActionView(R.layout.actionbar_indeterminate_progress);
            item2.setEnabled(false);
        } else {
            MenuItemCompat.setActionView(item2, null);
            item2.setEnabled(true);
        }
        MenuItemCompat.setShowAsAction(item2, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        item2.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                DynamixService.refreshPluginsFromRepos();
                if (fragmentsAdapter != null && fragmentsAdapter.getApplicationsFragment() != null) {
                    fragmentsAdapter.getApplicationsFragment().refreshList();
                }
                return true;
            }
        });
        // Activate / Deactivate Button
        if (DynamixService.isFrameworkInitialized()) {
            MenuItem item3 = menu.add(3, Menu.FIRST + 2, Menu.NONE, "Activate/Deactivate Dynamix");
            if (DynamixService.isFrameworkStarted()) {
                item3.setIcon(R.drawable.power_on);
            } else {
                item3.setIcon(R.drawable.power_off);
            }
            MenuItemCompat.setShowAsAction(item3, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
            item3.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (DynamixService.isFrameworkStarted()) {
                        DynamixService.stopFramework();
                    } else {
                        DynamixService.startFramework();
                    }
                    return true;
                }
            });
            if (dynamixFrameworkStateChanging) {
                // MenuItemCompat.setActionView(item3, R.layout.actionbar_indeterminate_progress);
                // item3.setActionView(R.layout.actionbar_indeterminate_progress);
                // item3.setEnabled(false);
            } else {
                MenuItemCompat.setActionView(item3, null);
                // item3.setActionView(null);
                item3.setEnabled(true);
            }
        }
        // Setup Default Settings
        MenuItem item4 = menu.add(4, Menu.FIRST + 3, Menu.NONE, "Shut Down Dynamix");
        item4.setIcon(R.drawable.ic_action_replay);
        item4.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Utils.showGlobalAlert(HomeActivity.this, "Shutdown Options", true);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDynamixInitializing() {
    }

    @Override
    public void onDynamixInitializingError(String message) {
        Log.d(LOGTAG, "onDynamixInitializingError : " + message);
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onDynamixInitialized(DynamixService dynamix) {
        Log.d(LOGTAG, "onDynamixInitialized");
        // if (!DynamixService.isFrameworkStarted() && DynamixPreferences.isDynamixEnabled()
        // && DynamixPreferences.autoStartDynamix()) {
        // DynamixService.startFramework();
        // }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showStartupToast();
                reInitFragments();
                supportInvalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onDynamixStarting() {
        dynamixFrameworkStateChanging = true;
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onDynamixStarted() {
        Log.d(LOGTAG, "onDynamixStarted");
        dynamixFrameworkStateChanging = false;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reInitFragments();
                Toast.makeText(HomeActivity.this, "Dynamix Framework Started", Toast.LENGTH_LONG).show();
            }
        });
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onDynamixStopping() {
        dynamixFrameworkStateChanging = true;
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onDynamixStopped() {
        dynamixFrameworkStateChanging = false;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MenuItem mi = optionsMenu.getItem(3);
                mi.setIcon(R.drawable.power_off);
            }
        });
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onDynamixError(String message) {
        Log.d(LOGTAG, "onDynamixError : " + message);
    }

    @Override
    public void onPluginDiscoveryStarted() {
        pluginRefreshing = true;
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onPluginDiscoveryCompleted() {
        Log.d(LOGTAG, "Plugin discovery completed");
        pluginRefreshing = false;
        reInitFragments();
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onPluginDiscoveryError(String message) {
        pluginRefreshing = false;
        invalidateOptionsMenuOnUiThread();
    }

    @Override
    public void onPluginStopTimeout() {
    }

    private void invalidateOptionsMenuOnUiThread() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                supportInvalidateOptionsMenu();
                // invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onPluginInstalled(ContextPluginInformation plug) {
        Log.d(LOGTAG, "New Plugin Installed : " + plug.getPluginName());
        if (fragmentsAdapter != null) {
            if (fragmentsAdapter.getInstalledPluginsFragment() != null)
                fragmentsAdapter.getInstalledPluginsFragment().onPluginInstalled(plug);
            if (fragmentsAdapter.getPendingPluginsFragment() != null)
                fragmentsAdapter.getPendingPluginsFragment().onPluginInstalled(plug);
            reInitFragments();
        }
    }

    @Override
    public void onPluginUninstalled(ContextPluginInformation plug) {
        Log.d(LOGTAG, "Plugin Uninstalled : " + plug.getPluginName());
        if (fragmentsAdapter != null) {
            if (fragmentsAdapter.getInstalledPluginsFragment() != null)
                fragmentsAdapter.getInstalledPluginsFragment().onPluginUninstalled(plug);
            if (fragmentsAdapter != null && fragmentsAdapter.getPendingPluginsFragment() != null)
                fragmentsAdapter.getPendingPluginsFragment().onPluginUninstalled(plug);
            reInitFragments();
        }
    }

    @Override
    public void onPluginUpdated(ContextPluginInformation plug) {
    }

    @Override
    public void onDynamixApplicationAdded(DynamixApplication app) {
        Log.d(LOGTAG, "Application was added : " + app.getName());
        if (fragmentsAdapter != null && fragmentsAdapter.getApplicationsFragment() != null) {
            fragmentsAdapter.getApplicationsFragment().refreshList();
        }
    }

    private void reInitFragments() {
        if (fragmentsAdapter != null) {
            if (fragmentsAdapter.getInstalledPluginsFragment() != null)
                fragmentsAdapter.getInstalledPluginsFragment().reInitList();
            else {
                Log.e(LOGTAG, "Installed plugins fragment was null");
            }
            if (fragmentsAdapter.getPendingPluginsFragment() != null) {
                fragmentsAdapter.getPendingPluginsFragment().reInitList();
            } else {
                Log.e(LOGTAG, "Pending plugins fragment was null");
            }
            if (fragmentsAdapter.getFeaturesFragment() != null)
                fragmentsAdapter.getFeaturesFragment().refreshList();
            else {
                Log.e(LOGTAG, "Features fragment was null");
            }
            if (fragmentsAdapter.getApplicationsFragment() != null) {
                fragmentsAdapter.getApplicationsFragment().refreshList();
            } else {
                Log.e(LOGTAG, "Applications fragment was null");
            }
        }
    }

    @Override
    public void onDynamixApplicationConnected(DynamixApplication app) {
        Log.d(LOGTAG, "Application was connected : " + app.getName());
        // Update app with the incoming 'live' version from memory
        if (fragmentsAdapter != null && fragmentsAdapter.getApplicationsFragment() != null) {
            fragmentsAdapter.getApplicationsFragment().refreshList();
        }
    }

    @Override
    public void onDynamixApplicationDisconnected(DynamixApplication app) {
        Log.d(LOGTAG, "Application was disconnected : " + app.getName());
        // Update app with the incoming 'live' version from memory
        if (fragmentsAdapter != null && fragmentsAdapter.getApplicationsFragment() != null) {
            fragmentsAdapter.getApplicationsFragment().refreshList();
        }
    }

    @Override
    public void onDynamixApplicationRemoved(DynamixApplication app) {
        // Update app with the incoming 'live' version from memory
        Log.d(LOGTAG, "Application was removed : " + app.getName());
        if (fragmentsAdapter != null && fragmentsAdapter.getApplicationsFragment() != null) {
            fragmentsAdapter.getApplicationsFragment().refreshList();
        }
    }

    private void showStartupToast() {
        if (DynamixService.isInRecoveryMode()) {
            Toast.makeText(HomeActivity.this, "Dynamix Recovery Mode: Framework Not Started.", Toast.LENGTH_LONG)
                    .show();
        }
    }
}
