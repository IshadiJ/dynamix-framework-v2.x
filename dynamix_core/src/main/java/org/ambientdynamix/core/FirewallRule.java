/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.api.application.ContextPluginInformation;

import android.os.Parcel;
import android.os.Parcelable;

public class FirewallRule implements Parcelable {
	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<FirewallRule> CREATOR = new Parcelable.Creator<FirewallRule>() {
		@Override
		public FirewallRule createFromParcel(Parcel in) {
			return new FirewallRule(in);
		}

		@Override
		public FirewallRule[] newArray(int size) {
			return new FirewallRule[size];
		}
	};

	public enum FirewallAccess {
		PENDING, ALLOWED_ALWAYS, ALLOWED_SESSION, DENIED, BLOCKED
	}

	private FirewallAccess access = FirewallAccess.PENDING;
	private String contextType = "";
	private String contextTypeName = "";
	private String contextTypeDescription = "";
	private List<ContextPluginInformation> plugins = new ArrayList<ContextPluginInformation>();

	/**
	 * Creates a FirewallRule.
	 */
	public FirewallRule() {
	}

	/**
	 * Creates a FirewallRule.
	 */
	public FirewallRule(String contextType, String contextTypeName, String contextTypeDescription,
			ContextPluginInformation plugin) {
		this.contextType = contextType;
		this.contextTypeName = contextTypeName;
		this.contextTypeDescription = contextTypeDescription;
		plugins.add(plugin);
		this.access = FirewallAccess.PENDING;
	}

	/**
	 * Creates a FirewallRule.
	 */
	public FirewallRule(String contextType, String contextTypeName, String contextTypeDescription,
			List<ContextPluginInformation> plugins) {
		this.contextType = contextType;
		this.contextTypeName = contextTypeName;
		this.contextTypeDescription = contextTypeDescription;
		this.plugins = plugins;
		this.access = FirewallAccess.PENDING;
	}

	/**
	 * Creates a FirewallRule.
	 */
	public FirewallRule(String contextType, String contextTypeName, String contextTypeDescription,
			List<ContextPluginInformation> plugins, FirewallAccess access) {
		this.contextType = contextType;
		this.contextTypeName = contextTypeName;
		this.contextTypeDescription = contextTypeDescription;
		this.plugins = plugins;
		this.access = access;
	}

	/**
	 * Returns the context type id associated with this rule.
	 */
	public String getContextTypeId() {
		return contextType;
	}

	/**
	 * Returns the context type name associated with this rule.
	 */
	public String getContextTypeName() {
		return this.contextTypeName;
	}

	/**
	 * Returns the context type description associated with this rule.
	 */
	public String getContextTypeDescription() {
		return this.contextTypeDescription;
	}

	/**
	 * Returns the firewall access specified by this rule.
	 */
	public FirewallAccess getFirewallAccess() {
		return access;
	}

	/**
	 * Sets the firewall access specified by this rule.
	 */
	public void setAccess(FirewallAccess access) {
		this.access = access;
	}

	/**
	 * Returns the list of plug-ins associated with this rule.
	 */
	public List<ContextPluginInformation> getPlugins() {
		return plugins;
	}

	/**
	 * Returns true if this rule is granted; false otherwise. Note that this returns true for both ALLOWED_ALWAYS and
	 * ALLOWED_SESSION.
	 */
	public boolean isAccessGranted() {
		return getFirewallAccess() == FirewallAccess.ALLOWED_ALWAYS
				|| getFirewallAccess() == FirewallAccess.ALLOWED_SESSION;
	}

	@Override
	public String toString() {
		StringBuilder plugins = new StringBuilder();
		for (ContextPluginInformation plug : getPlugins())
			plugins.append(plug.getPluginId() + "-v" + plug.getVersion() + " ");
		return this.getContextTypeId() + " has access " + getFirewallAccess() + " for plugins: " + plugins;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... cast and check 'em
		FirewallRule other = (FirewallRule) candidate;
		if (contextType.equalsIgnoreCase(other.contextType)) {
			if (plugins.size() == other.plugins.size()) {
				return plugins.containsAll(other.plugins);
			}
		}
		return false;
	}

	// HashCode Example: http://www.javafaq.nu/java-example-code-175.html
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.contextType.hashCode();
		for (ContextPluginInformation plug : plugins) {
			result += plug.hashCode();
		}
		return result;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(contextType);
		dest.writeString(contextTypeName);
		dest.writeString(contextTypeDescription);
		dest.writeList(plugins);
		dest.writeInt(getAccessInt(access));
	}

	private FirewallRule(Parcel p) {
		this.contextType = p.readString();
		this.contextTypeName = p.readString();
		this.contextTypeDescription = p.readString();
		p.readList(plugins, getClass().getClassLoader());
		this.access = getAccessFromInt(p.readInt());
	}

	private FirewallAccess getAccessFromInt(int i) {
		switch (i) {
		case 1:
			return FirewallAccess.ALLOWED_ALWAYS;
		case 2:
			return FirewallAccess.ALLOWED_SESSION;
		case 3:
			return FirewallAccess.BLOCKED;
		case 4:
			return FirewallAccess.DENIED;
		case 5:
			return FirewallAccess.PENDING;
		default:
			return null;
		}
	}

	private int getAccessInt(FirewallAccess access) {
		switch (access) {
		case ALLOWED_ALWAYS:
			return 1;
		case ALLOWED_SESSION:
			return 2;
		case BLOCKED:
			return 3;
		case DENIED:
			return 4;
		case PENDING:
			return 5;
		default:
			return 0;
		}
	}
}
