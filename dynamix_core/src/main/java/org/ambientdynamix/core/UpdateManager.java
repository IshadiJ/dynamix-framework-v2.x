/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.AndroidFeatureInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.update.DynamixUpdates;
import org.ambientdynamix.update.DynamixUpdatesBinder;
import org.ambientdynamix.update.contextplugin.ContextPluginConnectorFactory;
import org.ambientdynamix.update.contextplugin.IContextPluginConnector;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.Repository;
import org.ambientdynamix.web.WebUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.content.Context;
import android.content.pm.FeatureInfo;
import android.util.Log;

/**
 * Manages update discovery and notification for the Dynamix Framework. Currently, this class only discovers updates for
 * ContextPlugins; however, long term, all dynamically updatable parts of the framework will be managed through this
 * class.
 * <p>
 * ContextPlugin XML descriptions must adhere to the specification as described in the Dynamix developer documentation.
 * The class will automatically use a backup server (if provided) if access to the primary update server is fails
 * (automatic failover).
 * 
 * @author Darren Carlson
 */
class UpdateManager {
	// Private data
	private static final String TAG = UpdateManager.class.getSimpleName();
	private static IContextPluginConnector currentPlugConnector;
	private static volatile boolean cancelled;
	private static volatile boolean processingContextPluginUpdates;
	// In memory list of pending plug-ins
	private static List<PendingContextPlugin> inMemoryPendingPlugins = new ArrayList<PendingContextPlugin>();

	// Singleton constructor
	private UpdateManager() {
	}

	/**
	 * Checks for Dynamix Framework updates.
	 */
	public static synchronized void refreshDynamixFrameworkUpdates(final Context c, final String updateUrl,
			final IDynamixUpdateListener listener, boolean runAsynchronously) {
		Utils.dispatch(runAsynchronously, new Runnable() {
			@Override
			public void run() {
				Log.i(TAG, "Starting Dynamix Framework Update: " + WebUtils.getNoCacheUrl(updateUrl));
				listener.onUpdateStarted();
				try {
					URL server = new URL(WebUtils.getNoCacheUrl(updateUrl));
					InputStream input = server.openStream();
					Serializer serializer = new Persister();
					SAXReader reader = new SAXReader(); // dom4j SAXReader
					reader.setStripWhitespaceText(true);
					/*
					 * TODO: Using the dom4j Document here, since it can load input from a variety of sources
					 * automatically (file and network). We could explore providing our own low-overhead transport
					 * mechanisms, if the Document gets too heavy.
					 */
					Document document = reader.read(input);
					String xml = document.asXML();
					input.close();
					Reader metaReader = new StringReader(xml);
					DynamixUpdatesBinder updatesBinder = serializer.read(DynamixUpdatesBinder.class, metaReader, false);
					// Create a DynamixUpdates object from the binder
					DynamixUpdates updates = new DynamixUpdates(updatesBinder);
					listener.onUpdateComplete(updates);
				} catch (Exception e) {
					Log.w(TAG, "Dynamix Update Failed: " + e);
					listener.onUpdateError(e.getMessage());
				}
			}
		});
	}

	/**
	 * Asynchronously checks for context plugin updates using the repositories, notifying the specified callback with
	 * results (or errors).
	 * 
	 * @param plugRepos
	 *            The List of IContextPluginConnector entities to check for updates
	 * @param platform
	 *            The device platform
	 * @param platformVersion
	 *            The device platform version
	 * @param frameworkVersion
	 *            The Dynamix version
	 * @param handler
	 *            The IUpdateStatusListener to notify with results (or errors)
	 */
	public static synchronized void refreshContextPluginsFromRepos(final Context c, final List<Repository> plugRepos,
			final PluginConstants.PLATFORM platform, final VersionInfo platformVersion,
			final VersionInfo frameworkVersion, final IContextPluginUpdateListener callback,
			final FeatureInfo[] availableFeatures, boolean runAsynchronously) {
		if (!processingContextPluginUpdates) {
			Log.d(TAG, "Checking for plug-in updates with repo count: " + plugRepos.size());
			// Setup state
			processingContextPluginUpdates = true;
			cancelled = false;
			// Handle notifications
			SessionManager.notifyAllContextPluginDiscoveryStarted();
			// Dispatch the update handler
			Utils.dispatch(runAsynchronously, new Runnable() {
				@Override
				public void run() {
					Log.d(TAG, "Started checking for contect plug-in updates...");
					// Stop the Dynamix update timer
					DynamixService.stopContextPluginUpdateTimer();
					// Grab the current list of plug-ins
					List<PendingContextPlugin> existingPlugs = getPendingContextPlugins();
					// Notify the callback that we've started the update
					if (callback != null) {
						Utils.dispatch(true, new Runnable() {
							@Override
							public void run() {
								callback.onUpdateStarted();
							}
						});
					}
					// Create an update list (to be filled by the repos)
					List<PendingContextPlugin> updates = new ArrayList<PendingContextPlugin>();
					// Create a map of errors
					final Map<Repository, String> errors = new HashMap<Repository, String>();
					// Process each plug-in repo
					for (Repository repo : plugRepos) {
						// Check for cancelled
						if (cancelled) {
							Log.w(TAG, "checkForContextPluginUpdates was cancelled: " + cancelled);
							break;
						}
						// Check for enabled
						if (repo.isEnabled()) {
							try {
								// Update the currentPlugConnector
								currentPlugConnector = ContextPluginConnectorFactory.makeContextPluginConnector(repo);
								Date lastModifiedLocal = DynamixService.getRepoLastModified(currentPlugConnector
										.getConnectorId());
								if (lastModifiedLocal == null)
									lastModifiedLocal = new Date();
								Date lastModifiedRemote = currentPlugConnector.getLastModified();
								Log.d(TAG, "lastModifiedLocal " + lastModifiedLocal.toGMTString()
										+ " lastModifiedRemote " + lastModifiedRemote.toGMTString());
								// if (lastModifiedRemote.getTime() > lastModifiedLocal.getTime()) {
								/*
								 * TODO: We should get the last modified check working properly. Currently, we always
								 * check for updates, regardless of last modified.
								 */
								if (true) {
									// Local information is stale... do the update
									Log.d(TAG, "Repo is modified: " + currentPlugConnector.getConnectorId());
									Log.i(TAG, "Updating from: " + currentPlugConnector);
									// Use the connector to get the list of plug-ins from the repo
									List<PendingContextPlugin> potentialUpdates = currentPlugConnector
											.getContextPlugins(platform, platformVersion, frameworkVersion);
									// Scan the potentialUpdates list and remove problem plug-ins
									List<PendingContextPlugin> remove = new ArrayList<PendingContextPlugin>();
									for (PendingContextPlugin update : potentialUpdates) {
										ContextPlugin plug = update.getPendingContextPlugin();
										// Check for update error
										if (update.hasError()) {
											errors.put(repo, update.getErrorMessage());
											remove.add(update);
											break;
										}
										// Check for plug-in validation errors
										if (!Utils.validateContextPlugin(plug)) {
											errors.put(repo, "Plug-in validation error");
											remove.add(update);
											break;
										}
										// Check for framework dependencies
										if (plug.hasMaxFrameworkVersion()) {
											if (DynamixService.getDynamixFrameworkVersion().compareTo(
													plug.getMaxDynamixVersion()) >= 0) {
												Log.d(TAG, "Removing framework incompatible plug: " + plug);
												remove.add(update);
											}
										}
										// Check for feature dependencies
										if (plug.hasFeatureDependencies()) {
											for (AndroidFeatureInfo featureDependency : plug.getFeatureDependencies()) {
												if (featureDependency != null && featureDependency.isRequired()) {
													boolean featureFound = false;
													for (FeatureInfo feature : availableFeatures) {
														if (feature.name != null
																&& feature.name.equalsIgnoreCase(featureDependency
																		.getName())) {
															featureFound = true;
															break;
														}
													}
													/*
													 * If we didn't find a required feature, remove the plugin
													 */
													if (!featureFound) {
														Log.w(TAG, "Removing feature incompatible plug: " + plug);
														remove.add(update);
													}
												}
											}
										}
									}
									// Remove problematic updates
									potentialUpdates.removeAll(remove);
									// Finally, add all compatible updates.
									updates.addAll(potentialUpdates);
									// Set the last modified time for this repo
									DynamixService.setRepoLastModified(currentPlugConnector.getConnectorId(),
											lastModifiedRemote);
								} else {
									// Local info is up-to-date... re-add what we've already found
									Log.d(TAG, "Repo is unmodified: " + currentPlugConnector.getConnectorId());
									for (PendingContextPlugin plug : DynamixService.getPendingContextPlugins()) {
										if (plug.getPendingContextPlugin().getRepoSource().getUrl()
												.equalsIgnoreCase(currentPlugConnector.getConnectorId()))
											updates.add(plug);
									}
								}
								currentPlugConnector = null;
							} catch (Exception e) {
								Log.w(TAG, "Exception during update: " + e.toString());
								e.printStackTrace();
								errors.put(repo, e.toString());
							}
						} else {
							Log.i(TAG, "Repo disabled (ignoring): " + repo);
						}
					}
					// All repos have been processed, so finish up
					if (cancelled) {
						Utils.dispatch(true, new Runnable() {
							@Override
							public void run() {
								if (callback != null)
									callback.onUpdateCancelled();
							}
						});
					} else {
						/*
						 * Add discovered updates to the in-memory list if not known; however, remove anything in the
						 * in-memory list that is not in the databaseList.
						 */
						synchronized (inMemoryPendingPlugins) {
							// List<PendingContextPlugin> updateList = new ArrayList<PendingContextPlugin>();
							for (PendingContextPlugin plug : updates) {
								if (inMemoryPendingPlugins.contains(plug)) {
									inMemoryPendingPlugins.remove(plug);
									inMemoryPendingPlugins.add(plug);
								} else
									inMemoryPendingPlugins.add(plug);
							}
							inMemoryPendingPlugins.retainAll(updates);
						}
						final List<PendingContextPlugin> finalResults = new ArrayList<PendingContextPlugin>(
								inMemoryPendingPlugins);
						if (callback != null) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									callback.onUpdateComplete(Utils.getSortedDiscoveredPluginList(finalResults), errors);
								}
							});
						}
					}
					Log.d(TAG, "Completed checking for context plug-in updates");
					// Restart the update timer, if necessary
					DynamixService.startContextPluginUpdateTimer();
					processingContextPluginUpdates = false;
				}
			}, new Runnable() {
				@Override
				public void run() {
					Log.w(TAG, "Exception occurred!");
					Utils.dispatch(true, new Runnable() {
						@Override
						public void run() {
							callback.onUpdateCancelled();
						}
					});
					// Restart the update timer, if necessary
					DynamixService.startContextPluginUpdateTimer();
					processingContextPluginUpdates = false;
				}
			});
		} else {
			Log.w(TAG, "Already discovering plug-ins!");
		}
	}

	public void updateInMemoryPlugin(List<PendingContextPlugin> pending) {
	}

	/**
	 * Cancels an existing repo update operation.
	 */
	public static void cancelContextPluginRepoUpdate() {
		Log.d(TAG, "cancelContextPluginUpdate");
		cancelled = true;
		// if (currentPlugConnector != null) {
		// currentPlugConnector.cancel();
		// }
		// currentPlugConnector = null;
	}

	/**
	 * Returns a List of PendingContextPlugins that are pending updates to already installed plug-ins. Note that this
	 * method requires that 'checkForContextPluginUpdates' has already successfully stored a list of context plug-in
	 * updates in the SettingsManager.
	 */
	static List<PendingContextPlugin> getContextPluginUpdates() {
		// Create a list of updates to return
		List<PendingContextPlugin> results = new ArrayList<PendingContextPlugin>();
		// Scan through the list of pending plug-ins
		for (PendingContextPlugin pendingPlugin : getPendingContextPlugins()) {
			if (pendingPlugin != null) {
				boolean found = false;
				// Scan through the list of existing context plugins, looking for a plugin associated with the update
				for (ContextPlugin target : DynamixService.getInstalledContextPluginsFromDatabase()) {
					ContextPlugin plug = pendingPlugin.getPendingContextPlugin();
					// Check for an ID match
					if (target.getId().equalsIgnoreCase(plug.getId())) {
						found = true;
						/*
						 * We have a matching ID. Now check if the new plugin's version is greater than the existing
						 * plug-in.
						 */
						if (plug.getVersion().compareTo(target.getVersion()) > 0) {
							// The plugin listed in the update is newer... add it as the target of the update
							pendingPlugin.setUpdateTarget(target);
							results.add(pendingPlugin);
						} else
							// Log.d(TAG, "Currently installed ContextPlugin " + target + " is >= " + clonePlug);
							break;
					}
				}
			} else
				Log.e(TAG, "SettingsManager contained a NULL PendingContextPlugin");
		}
		return results;
	}

	/**
	 * Returns the in-memory list of pending plug-ins, which is updated using the database. This is used to maintain
	 * plug-in state (e.g., during installs). If we keep grabbing plug-ins from the database, their state will be
	 * continually reset, which messes up state handling.
	 */
	public synchronized static List<PendingContextPlugin> getPendingContextPlugins() {
		synchronized (inMemoryPendingPlugins) {
			// Grab known plug-ins from the database
			List<PendingContextPlugin> databaseList = DynamixService.getPendingContextPluginsFromDatabase();
			// If the in-memory list is empty, load it with the database list
			if (inMemoryPendingPlugins.isEmpty()) {
				inMemoryPendingPlugins.addAll(databaseList);
			} else {
				/*
				 * Add pending plug-ins in the databaseList to the in-memory list if not known; however, remove anything
				 * in the in-memory list that is not in the databaseList.
				 */
				for (PendingContextPlugin plug : databaseList) {
					// Remove existing plug-in from memory to make sure they're fully refreshed
					if (inMemoryPendingPlugins.contains(plug)) {
						inMemoryPendingPlugins.remove(plug);
						inMemoryPendingPlugins.add(plug);
					} else
						inMemoryPendingPlugins.add(plug);
				}
				// for (PendingContextPlugin plug : databaseList)
				// if (!inMemoryPendingPlugins.contains(plug)) {
				// // Log.d(TAG, "[UPDATE] Plug-in not in memory, adding: " + plug);
				// inMemoryPendingPlugins.add(plug);
				// } else {
				// // Log.d(TAG, "[UPDATE] Plug-in already in memory, ignoring: " + plug);
				// }
				inMemoryPendingPlugins.retainAll(databaseList);
			}
			return Utils.getSortedDiscoveredPluginList(new ArrayList<PendingContextPlugin>(inMemoryPendingPlugins));
		}
	}

	/**
	 * Base interface for update listeners
	 * 
	 * @author Darren Carlson
	 */
	interface IBaseUpdateListener {
		/**
		 * Raised if the update is cancelled.
		 */
		void onUpdateCancelled();

		/**
		 * Raised if there was an update error.
		 */
		void onUpdateError(String message);

		/**
		 * Raised when the update is started.
		 */
		void onUpdateStarted();
	}

	/**
	 * Interface for listeners interested in receiving updates about plug-in updates.
	 * 
	 * @author Darren Carlson
	 */
	interface IContextPluginUpdateListener extends IBaseUpdateListener {
		/**
		 * Raised when the update is complete. Provides a list of UpdateResults and (possibly) a Map of
		 * IContextPluginConnector error messages.
		 */
		void onUpdateComplete(List<PendingContextPlugin> incomingUpdates, Map<Repository, String> errors);
	}

	/**
	 * Interface for listeners interested in receiving updates about Dynamix updates.
	 * 
	 * @author Darren Carlson
	 */
	interface IDynamixUpdateListener extends IBaseUpdateListener {
		void onUpdateComplete(DynamixUpdates updates);
	}
}