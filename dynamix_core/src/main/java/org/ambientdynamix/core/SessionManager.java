/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.ambientdynamix.api.application.ContextHandler;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.DynamixFacade;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextHandlerCallback;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IPluginInstallCallback;
import org.ambientdynamix.api.application.ISessionCallback;
import org.ambientdynamix.api.application.ISessionListener;
import org.ambientdynamix.core.EventCommand.ContextPluginDiscoveryFinishedCommand;
import org.ambientdynamix.core.EventCommand.ContextPluginDiscoveryStartedCommand;
import org.ambientdynamix.core.EventCommand.ContextPluginEnabledStateChangeCommand;
import org.ambientdynamix.core.EventCommand.ContextPluginErrorCommand;
import org.ambientdynamix.core.EventCommand.ContextPluginInstalled;
import org.ambientdynamix.core.EventCommand.ContextPluginUninstalledCommand;
import org.ambientdynamix.core.EventCommand.DynamixFrameworkActiveCommand;
import org.ambientdynamix.core.EventCommand.DynamixFrameworkInactiveCommand;
import org.ambientdynamix.core.EventCommand.ListenerRemovedCommand;
import org.ambientdynamix.core.EventCommand.SessionClosedCommand;
import org.ambientdynamix.core.EventCommand.SessionOpenedCommand;

import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

/**
 * Manages sessions for Dynamix applications. Includes remote communications facilities. Apps are assigned to one
 * session, but may have multiple listeners. Closing a session automatically removes all associated listeners.
 * 
 * @author Darren Carlson
 */
public class SessionManager {
	// Private data
	private final static String TAG = SessionManager.class.getSimpleName();
	private static SessionListenerList sessionListenerList = new SessionListenerList();
	private static ContextHandlerCallbackList contextHandlerList = new ContextHandlerCallbackList();
	private static Handler sessionHandler = new Handler();
	private static Handler contextHandler = new Handler();
	private static Handler callbackHandler = new Handler();
	private static Map<String, DynamixSession> sessionMap = new ConcurrentHashMap<String, DynamixSession>();
	private static List<OpenSessionCallback> sessionOpenedCallbacks = new ArrayList<OpenSessionCallback>();

	/*
	 * Local struct-like class for holding session callbacks and associated facades.
	 */
	private static class OpenSessionCallback {
		private ISessionCallback callback;
		private IDynamixFacade facade;

		public OpenSessionCallback(ISessionCallback callback, IDynamixFacade facade) {
			this.callback = callback;
			this.facade = facade;
		}
	}

	// Singleton constructor
	private SessionManager() {
	}

	/**
	 * Clears all session opened callbacks.
	 */
	public static void clearSessionOpenedCallbacks() {
		sessionOpenedCallbacks.clear();
	}

	/**
	 * Adds the IContextHandler to the application's session.
	 */
	public static void addContextHandler(final String appId, final IContextHandler handler,
			final IContextHandlerCallback callback) {
		sessionHandler.post(new Runnable() {
			@Override
			public void run() {
				Log.d(TAG, "SessionManager is adding handler " + handler + " with appId " + appId);
				if (handler != null) {
					// Register the listener
					contextHandlerList.register(handler);
					// Update the sessionMap
					synchronized (sessionMap) {
						// Update the sessionMap
						if (sessionMap.containsKey(appId)) {
							// Access the session, since it already exists
							DynamixSession session = sessionMap.get(appId);
							// Add the handler to the session
							session.addContextHandler(handler, callback);
						} else {
							// Cannot addContextHandler without an open session
							sendContextHandlerCallbackFailure(callback, "SESSION_NOT_FOUND",
									ErrorCodes.SESSION_NOT_FOUND);
						}
					}
				} else
					Log.w(TAG, "Listener was null in addDynamixListener");
			}
		});
	}

	/**
	 * Refreshes each session's app using the Dynamix database, which updates the app's in memory details.
	 */
	protected static void refreshApps() {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				session.refreshApp();
			}
		}
	}

	/**
	 * Returns the collection of all sessions.
	 */
	protected static Collection<DynamixSession> getAllSessions() {
		return sessionMap.values();
	}

	/**
	 * Returns a DynamixSession for the specified DynamixApplication.
	 */
	protected static DynamixSession getSession(DynamixApplication app) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen() && session.getApp().equals(app)) {
					return session;
				}
			}
		}
		Log.v(TAG, "getSession could not find a session for app: " + app);
		return null;
	}

	/**
	 * Returns true if the app has an open sesion; false otherwise.
	 */
	protected static boolean hasOpenSession(DynamixApplication app) {
		DynamixSession session = getSession(app);
		if (session != null)
			return session.isSessionOpen();
		else
			return false;
	}

	/**
	 * Returns all context support provided by the specified ContextPlugin.
	 */
	protected static List<ContextSupport> getAllContextSupport(ContextPluginInformation plug) {
		List<ContextSupport> subs = new ArrayList<ContextSupport>();
		synchronized (sessionMap) {
			for (DynamixSession s : sessionMap.values()) {
				for (ContextSupport sup : s.getAllContextSupport()) {
					if (sup.containsContextPlugin(plug)) {
						subs.add(sup);
					}
				}
			}
		}
		return subs;
	}

	/**
	 * Returns true if the context support is registered; false otherwise.
	 */
	protected static boolean hasContextSupport(DynamixSession session, ContextSupportInfo supportInfo) {
		return getContextSupport(supportInfo) != null;
	}

	/**
	 * Returns the context support object registered to the specified context support info object, or null if there is
	 * no association.
	 */
	protected static ContextSupport getContextSupport(ContextSupportInfo supportInfo) {
		synchronized (sessionMap) {
			for (DynamixSession s : sessionMap.values()) {
				ContextSupport sup = s.getContextSupport(supportInfo);
				if (sup != null)
					return sup;
			}
		}
		return null;
	}

	/**
	 * Returns all context support registered to the specified handler.
	 */
	protected static List<ContextSupport> getContextSupport(IContextHandler handler) {
		List<ContextSupport> subs = new ArrayList<ContextSupport>();
		synchronized (sessionMap) {
			for (DynamixSession s : sessionMap.values()) {
				if (s.hasContextHandler(handler))
					subs.addAll(s.getContextSupport(handler));
			}
		}
		return subs;
	}

	/**
	 * Returns all context support registered to the specified app, handler and contextType.
	 */
	protected static List<ContextSupport> getContextSupport(DynamixApplication app, IContextHandler handler,
			String contextType) {
		List<ContextSupport> subs = new ArrayList<ContextSupport>();
		synchronized (sessionMap) {
			for (DynamixSession s : sessionMap.values()) {
				if (s.getApp().equals(app) && s.hasContextHandler(handler))
					subs.addAll(s.getAllContextSupport(contextType));
			}
		}
		return subs;
	}

	/**
	 * Returns the number of context support registrations held by the specified plug-in.
	 */
	protected static int getContextSupportCount(ContextPluginInformation plug) {
		return getAllContextSupport(plug).size();
	}

	/**
	 * Returns all context support registered for the specified plug-in.
	 */
	protected static List<ContextSupport> getContextSupport(ContextPluginInformation plug) {
		return getAllContextSupport(plug);
	}

	/**
	 * Returns all context support registered for the specified plug-in and Dynamix Session.
	 */
	protected static List<ContextSupport> getContextSupport(ContextPluginInformation plug, UUID dynamixSessionId) {
		List<ContextSupport> returnList = new ArrayList<ContextSupport>();
		DynamixSession session = sessionMap.get(dynamixSessionId);
		if (session != null) {
			for (ContextSupport sup : getAllContextSupport(plug)) {
				if (sup.getSession().equals(session))
					returnList.add(sup);
			}
		} else
			Log.w(TAG, "getContextSupport didn't find a session for " + dynamixSessionId);
		return returnList;
	}

	/**
	 * Returns true of the plug-in has any context support registrations; false otherwise.
	 */
	protected static boolean hasContextSupportRegistrations(ContextPluginInformation plug) {
		return getAllContextSupport(plug).size() > 0;
	}

	/**
	 * Registers the facade for receiving the session opened callback.
	 */
	protected static void registerSessionOpenedCallback(final ISessionCallback callback, final IDynamixFacade facade) {
		// Register the callback
		if (callback != null)
			callbackHandler.post(new Runnable() {
				@Override
				public void run() {
					sessionOpenedCallbacks.add(new OpenSessionCallback(callback, facade));
				}
			});
	}

	/**
	 * Returns a DynamixSession for the specified appId.
	 */
	protected static DynamixSession getSession(final String appId) {
		synchronized (sessionMap) {
			if (sessionMap.containsKey(appId)) {
				DynamixSession session = sessionMap.get(appId);
				return session;
			}
		}
		Log.w(TAG, "Could not find session for appId: " + appId);
		return null;
	}

	/**
	 * Returns a DynamixSession for the specified UUID.
	 */
	protected static DynamixSession getSession(UUID sessionId) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.getSessionId().equals(sessionId))
					return session;
			}
		}
		Log.w(TAG, "Could not find session for session UUID: " + sessionId);
		return null;
	}

	/**
	 * Returns true if the session is open for the appId; false otherwise.
	 */
	protected static boolean isSessionOpen(final String appId) {
		synchronized (sessionMap) {
			if (sessionMap.containsKey(appId)) {
				DynamixSession session = sessionMap.get(appId);
				return session.isSessionOpen();
			}
		}
		return false;
	}

	/**
	 * Kills the remote listeners list and then creates a new, fresh one.
	 */
	protected static void killRemoteListeners() {
		sessionHandler.post(new Runnable() {
			@Override
			public void run() {
				sessionListenerList.kill();
				sessionListenerList = new SessionListenerList();
			}
		});
	}

	/**
	 * Utility method that notifies all applications that Dynamix is active.
	 */
	public static void notifyAllDynamixFrameworkActive() {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(), new DynamixFrameworkActiveCommand());
			}
		}
	}

	/**
	 * Calls onSuccess on the IContextSupportCallback using the broadcastHandler for thread safety.
	 */
	protected static void sendContextSupportSuccess(final IContextSupportCallback callback,
			final ContextSupportInfo supportInfo) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onSuccess(supportInfo);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onProgress on the IContextSupportCallback using the broadcastHandler for thread safety.
	 */
	protected static void sendContextSupportProgress(final IContextSupportCallback callback, final int percentComplete) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onProgress(percentComplete);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onProgress to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onWarning on the IContextSupportCallback using the broadcastHandler for thread safety.
	 */
	protected static void sendContextSupportWarning(final IContextSupportCallback callback, final String message,
			final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onWarning(message, errorCode);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onWarning to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onFailure on the IContextSupportCallback using the broadcastHandler for thread safety.
	 */
	protected static void sendContextSupportFailure(final IContextSupportCallback callback, final String message,
			final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onFailure(message, errorCode);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onFailure to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onSuccess on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendCallbackSuccess(final ICallback callback) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onSuccess();
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onFailure on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendCallbackFailure(final ICallback callback, final String message, final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onFailure(message, errorCode);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onFailure to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onSuccess on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendContextRequestCallbackSuccess(final IContextRequestCallback callback,
			final ContextResult contextResult) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						doSendResult(contextResult, null, callback);
					} catch (Exception e) {
						Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onFailure on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendContextRequestCallbackFailure(final IContextRequestCallback callback, final String message,
			final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onFailure(message, errorCode);
					} catch (RemoteException e) {
						Log.w(TAG, "Error sending callback.onFailure to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onSuccess on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendSessionCallbackSuccess(final ISessionCallback callback, final IDynamixFacade facade) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onSuccess(new DynamixFacade(facade));
					} catch (Exception e) {
						Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onSuccess on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendSessionCallbackSuccess(final ISessionCallback callback, final IDynamixFacade facade,
			boolean synchronous) {
		if (callback != null && callback.asBinder().isBinderAlive())
			if (synchronous) {
				try {
					callback.onSuccess(new DynamixFacade(facade));
				} catch (Exception e) {
					Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
				}
			} else {
				sessionHandler.post(new Runnable() {
					@Override
					public void run() {
						try {
							callback.onSuccess(new DynamixFacade(facade));
						} catch (Exception e) {
							Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
						}
					}
				});
			}
	}

	/**
	 * Calls onFailure on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendSessionCallbackFailure(final ISessionCallback callback, final String message,
			final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onFailure(message, errorCode);
					} catch (Exception e) {
						Log.w(TAG, "Error sending callback.onFailure to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onSuccess on the callback using the broadcastHandler for thread safety.
	 */
	public static void sendContextHandlerCallbackSuccess(final IContextHandlerCallback callback,
			final IContextHandler handler) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onSuccess(new ContextHandler(handler));
					} catch (Exception e) {
						Log.w(TAG, "Error sending callback.onSuccess to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onFailure on the callback using a Handler for thread safety.
	 */
	public static void sendContextHandlerCallbackFailure(final IContextHandlerCallback callback, final String message,
			final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onFailure(message, errorCode);
					} catch (Exception e) {
						Log.w(TAG, "Error sending callback.onFailure to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onInstallStarted on the callback using a Handler for thread safety.
	 */
	public static void sendPluginInstallStarted(final IPluginInstallCallback callback,
			final ContextPluginInformation plugin) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onInstallStarted(plugin);
					} catch (Exception e) {
						Log.w(TAG, "Error sending listener.onInstallStarted to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onInstallProgress on the callback using a Handler for thread safety.
	 */
	public static void sendPluginInstallProgress(final IPluginInstallCallback callback,
			final ContextPluginInformation plugin, final int percentComplete) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onInstallProgress(plugin, percentComplete);
					} catch (Exception e) {
						Log.w(TAG, "Error sending listener.onInstallProgress to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onInstallComplete on the callback using a Handler for thread safety.
	 */
	public static void sendPluginInstallComplete(final IPluginInstallCallback callback,
			final ContextPluginInformation plugin) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onInstallComplete(plugin);
					} catch (Exception e) {
						Log.w(TAG, "Error sending listener.onInstallProgress to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Calls onInstallFailed on the callback using a Handler for thread safety.
	 */
	public static void sendPluginInstallFailed(final IPluginInstallCallback callback,
			final ContextPluginInformation plugin, final String errorMessage, final int errorCode) {
		if (callback != null && callback.asBinder().isBinderAlive())
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					try {
						callback.onInstallFailed(plugin, errorMessage, errorCode);
					} catch (Exception e) {
						Log.w(TAG, "Error sending listener.onInstallProgress to " + callback + ", exception was " + e);
					}
				}
			});
	}

	/**
	 * Utility method that notifies all applications that Dynamix is inactive.
	 */
	public static void notifyAllDynamixFrameworkInactive() {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(), new DynamixFrameworkInactiveCommand());
			}
		}
	}

	/**
	 * Notifies all applications that a new context plug-in was installed.
	 */
	protected static void notifyAllNewContextPluginInstalled(ContextPlugin plug) {
		DynamixService.notifyOnPluginInstalled(plug.getContextPluginInformation());
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values())
				sendSessionListenerCommand(session.getApp(),
						new ContextPluginInstalled(plug.getContextPluginInformation()));
		}
	}

	/**
	 * Notifies all applications that the context plug-in was uninstalled.
	 */
	protected static void notifyAllContextPluginUninstalled(ContextPlugin plug) {
		DynamixService.notifyOnPluginUninstalled(plug.getContextPluginInformation());
		for (DynamixSession session : sessionMap.values()) {
			sendSessionListenerCommand(session.getApp(),
					new ContextPluginUninstalledCommand(plug.getContextPluginInformation()));
		}
	}

	/**
	 * Utility method that notifies all applications that context plug-in discovery has started.
	 */
	public static void notifyAllContextPluginDiscoveryStarted() {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(), new ContextPluginDiscoveryStartedCommand());
			}
		}
	}

	/**
	 * Utility method that notifies all applications that context plug-in discovery has finished.
	 */
	public static void notifyAllContextPluginDiscoveryFinished() {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(), new ContextPluginDiscoveryFinishedCommand());
			}
		}
	}

	/**
	 * Utility method that notifies all applications that a context plug-in has encountered and error.
	 */
	public static void notifyAllContextPluginError(ContextPlugin plug, String errorMessage, int errorCode) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(),
							new ContextPluginErrorCommand(plug.getContextPluginInformation(), errorMessage, errorCode));
			}
		}
	}

	/**
	 * Utility method that notifies all applications that a context plug-in has been enabled or disabled.
	 */
	public static void notifyAllContextPluginEnabledStateChanged(ContextPlugin plug) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen())
					sendSessionListenerCommand(session.getApp(),
							new ContextPluginEnabledStateChangeCommand(plug.getContextPluginInformation()));
			}
		}
	}

	/**
	 * Sends the incoming list of context results to the listeners specified by the incoming eventMap. This method
	 * verifies that each receiver is authorized to receive the ContextEvent before sending it. For thread safety, all
	 * interactions are handled using a single Handler. <br>
	 * <br>
	 * We update each listener using the following adaptive approach: <br>
	 * 1. Try to send the ContextData to the remote client. This will succeed only if the remote client has the proper
	 * classes available in its classpath for the Parcelable contained within the ContextData's eventData. Otherwise,
	 * we'll generate an exception. <br>
	 * 2. If sending the event causes an exception, it's most probably due to the lack of proper ContextData classes on
	 * the remote side (necessary for resolving the ContextData's Percelable eventData.) In this case, null out the
	 * eventData and re-send the event, which still contains strings. The client can still use the string representation
	 * of the eventData.
	 * 
	 * @param eventMap
	 *            A Map containing IContextHandler entities and their associated context events.
	 */
	protected static void notifyContextListeners(final Map<IContextListener, List<ContextResult>> eventMap) {
		if (DynamixPreferences.isDetailedLoggingEnabled())
			Log.v(TAG, "notifyContextListeners with eventMap of size: " + eventMap.size());
		contextHandler.post(new Runnable() {
			@Override
			public void run() {
				for (IContextListener receiver : eventMap.keySet()) {
					List<ContextResult> results = eventMap.get(receiver);
					for (ContextResult result : results) {
						if (receiver != null) {
							doSendResult(result, receiver, null);
						}
					}
				}
			}
		});
	}

	/**
	 * Notifies the application that the Dynamix Framework session is open or closed.
	 */
	protected static void notifySessionClosed(DynamixApplication app) {
		sendSessionListenerCommand(app, new SessionClosedCommand());
	}

	/**
	 * Notifies the application that the Dynamix Framework session is open or closed and clears the opened session
	 * callback list.
	 */
	public static void notifySessionOpened(DynamixApplication app, String sessionId) {
		sendSessionListenerCommand(app, new SessionOpenedCommand(sessionId));
		synchronized (sessionOpenedCallbacks) {
			// Handle any session opened callbacks
			for (OpenSessionCallback callback : sessionOpenedCallbacks)
				sendSessionCallbackSuccess(callback.callback, callback.facade);
			sessionOpenedCallbacks.clear();
		}
	}

	/**
	 * Registers the incoming session listener to receive events.
	 */
	protected static void registerSessionListener(final ISessionListener listener) {
		// Register listener
		if (listener != null)
			sessionHandler.post(new Runnable() {
				@Override
				public void run() {
					sessionListenerList.register(listener);
				}
			});
	}

	/**
	 * Registers the newListener to receive events, unregistering the oldListener if it's not null.
	 */
	protected static void updateSessionListener(final ISessionListener oldListener, final ISessionListener newListener) {
		// Register listener
		sessionHandler.post(new Runnable() {
			@Override
			public void run() {
				if (oldListener != null)
					sessionListenerList.unregister(oldListener);
				if (newListener != null)
					sessionListenerList.register(newListener);
			}
		});
	}

	/**
	 * Opens the session for the specified app.
	 * 
	 * @param app
	 *            The Dynmaix app wishing to open a session
	 * @return A active DynamixSession
	 */
	public static DynamixSession openSession(final DynamixApplication app, final ISessionListener listener,
			final ISessionCallback callback) {
		synchronized (sessionMap) {
			Log.d(TAG, "openSession for: " + app);
			// Register the session listener
			registerSessionListener(listener);
			// Check for existing session
			if (sessionMap.containsKey(app.getAppID())) {
				// Handle existing session case
				DynamixSession session = sessionMap.get(app.getAppID());
				Log.d(TAG,
						"openSession found existing session for: " + app + " with session open state: "
								+ session.isSessionOpen());
				// Open the session for the app
				if (!session.isSessionOpen())
					session.openSession(app);
				else
					Log.d(TAG, "openSession found that the session was already opened for: " + app);
				return session;
			} else {
				// Handle new session case
				DynamixSession session = new DynamixSession(app.getAppID(), listener);
				session.openSession(app);
				sessionMap.put(app.getAppID(), session);
				Log.d(TAG, "openSession created new session for: " + app);
				return session;
			}
		}
	}

	/**
	 * Closes the session for the specified app.
	 * 
	 * @param app
	 *            The Dynmaix app wishing to open a session
	 * @param notify
	 *            True to notify sessions of close; false otherwise.
	 */
	protected static void closeSession(DynamixApplication app, boolean notify, ICallback callback) {
		synchronized (sessionMap) {
			Log.d(TAG, "closeSession for: " + app);
			// Check for existing session
			if (app != null && sessionMap.containsKey(app.getAppID())) {
				DynamixSession session = sessionMap.get(app.getAppID());
				session.closeSession(notify, callback);
				sessionMap.remove(app.getAppID());
			} else {
				Log.d(TAG, "No session found for: " + app);
				SessionManager.sendCallbackSuccess(callback);
			}
		}
	}

	/**
	 * Closes all sessions
	 * 
	 * @param notify
	 *            True to notify sessions of close; false otherwise.
	 */
	protected static void closeAllSessions(boolean notify, ICallback callback) {
		Collection<DynamixSession> sessions = sessionMap.values();
		if (sessions.isEmpty())
			SessionManager.sendCallbackSuccess(callback);
		else
			for (DynamixSession session : sessions)
				closeSession(session.getApp(), notify, callback);
	}

	/**
	 * Returns a DynamixSession for the specified handler.
	 */
	protected static DynamixSession getSession(IContextHandler listener) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.hasContextHandler(listener))
					return session;
			}
		}
		Log.w(TAG, "getSession could not find a session for listener: " + listener);
		return null;
	}

	/**
	 * Removes the IContextHandler from its associated session and the RemoteCallbackList of registered listeners. Note:
	 * For thread safety, all interactions with the RemoteCallbackList are handled using a single Handler.
	 * 
	 * @param handler
	 *            The IContextHandler to remove.
	 */
	protected static void removeContextHandler(final IContextHandler handler, final ICallback callback) {
		Log.d(TAG, "removeDynamixListener for: " + handler);
		sessionHandler.post(new Runnable() {
			@Override
			public void run() {
				/*
				 * Since we don't know the session the listener is in, try to remove it from all sessions and break on a
				 * match
				 */
				boolean found = false;
				for (DynamixSession session : sessionMap.values()) {
					if (session.isContextHandlerRegistered(handler)) {
						session.removeContextSupport(handler);
						session.removeContextHandler(handler);
						contextHandlerList.unregister(handler);
						found = true;
						// Break, since we're finished updating the session
						break;
					}
				}
				// Handle Callback
				if (found)
					SessionManager.sendCallbackSuccess(callback);
				else
					SessionManager.sendCallbackFailure(callback, "Could not find handler", ErrorCodes.NOT_FOUND);
			}
		});
	}

	/**
	 * Removes all context support for the specified plug-in.
	 * 
	 * @param plug
	 *            The plug-in to remove context support from.
	 */
	protected static void removeContextSupportForPlugin(final ContextPluginInformation plug) {
		Log.d(TAG, "removeContextSupportForPlugin for: " + plug);
		synchronized (sessionMap) {
			for (DynamixSession s : sessionMap.values())
				s.removeContextSupportFromPlugin(plug);
		}
	}

	/**
	 * Notifies all context listeners attached to the handler that they have been removed, and then removes the handler
	 * the ContextHandlerCallbackList.
	 */
	protected static void notifyContextHandlerRemoved(IContextHandler handler) {
		handlerEventBroadcast(handler, new ListenerRemovedCommand(), true);
	}

	/**
	 * Notifies the context support's listener that the context support has been removed.
	 */
	protected static void notifyContextSupportRemoved(final ContextSupport support, final String message,
			final int errorCode) {
		contextHandler.post(new Runnable() {
			@Override
			public void run() {
				Utils.dispatch(true, new Runnable() {
					@Override
					public void run() {
						try {
							if (DynamixPreferences.isDetailedLoggingEnabled())
								Log.i(TAG, "notifyContextSupportRemoved: " + support);
							if (support.hasContextListener() && support.getContextListener().asBinder().isBinderAlive()) {
								support.getContextListener().onContextSupportRemoved(support.getContextSupportInfo(),
										message, errorCode);
							}
						} catch (Exception e) {
							Log.w(TAG, "Error sending event command: " + e);
						}
					}
				});
			}
		});
	}

	/**
	 * Sends the IEventCommand to ALL handlers in the session.
	 */
	protected static void sendSessionListenerCommand(final DynamixApplication app,
			final IEventCommand<ISessionListener> eventCommand) {
		DynamixSession session = getSession(app);
		if (session != null) {
			if (session.getSessionListener() != null) {
				List<ISessionListener> receiverList = new ArrayList<ISessionListener>();
				receiverList.add(session.getSessionListener());
				doSessionEventBroadcast(receiverList, eventCommand, null);
			} else
				Log.v(TAG, "Ignoring eventCommand, since the app has no session listener: " + app);
		} else
			Log.w(TAG, "Could not find session for app: " + app);
	}

	/**
	 * Broadcasts the IEventCommand to each listener in the IContextHandler.
	 */
	protected static void handlerEventBroadcast(final IContextHandler handler,
			final IEventCommand<IContextListener> eventCommand, boolean removeHandler) {
		List<IContextHandler> receiverList = new ArrayList<IContextHandler>();
		receiverList.add(handler);
		if (removeHandler)
			doHandlerEventBroadcast(receiverList, eventCommand, receiverList);
		else
			doHandlerEventBroadcast(receiverList, eventCommand, null);
	}

	/**
	 * Updates a DynamixApplication in this session while retaining the session's original binder and listeners.
	 */
	protected static boolean updateSessionApplication(DynamixApplication app) {
		synchronized (sessionMap) {
			for (DynamixSession session : sessionMap.values()) {
				if (session.isSessionOpen() && session.getApp().equals(app)) {
					Log.d(TAG, "updateSessionApplication replaced app: " + app);
					session.updateApp(app);
					return true;
				}
			}
		}
		Log.w(TAG, "updateSession could not find: " + app);
		/*
		 * Clear any session rules just in case the app had its firewall rules changed (e.g., from ALLOWED to
		 * ALLOWED_SESSION) while its session was not open.
		 */
		app.clearSessionFirewallRules();
		return false;
	}

	/**
	 * Utility for sending results. Note that this method cannot handle more than one result target, so either receiver
	 * or callack must be null.
	 * 
	 * @param result
	 *            The result to send.
	 * @param receiver
	 *            The IContextListener to send results to, or null
	 * @param callback
	 *            The IContextRequestCallback to send results to, or null
	 */
	private static void doSendResult(ContextResult result, IContextListener receiver, IContextRequestCallback callback) {
		if (receiver != null && callback != null)
			throw new RuntimeException("doSendResult cannot handle more than one target");
		// First, try sending event with context info attached...
		result.attachContextInfo(true);
		/*
		 * Setup embedded mode, if set. Since events contain IContextData from dynamically loaded OSGi classes, embedded
		 * hosts cannot cast event data properly, since the event IContextData objects will have different classloaders
		 * from the embedded host. To overcome this issue, we create a proxy object that bridges the OSGi classloader to
		 * the embedded host's classloader.
		 */
		if (DynamixService.isEmbedded() && result.hasIContextInfo()) {
			try {
				// Update the IContextInfo object with a deep proxy
				result.setIContextInfo((IContextInfo) Utils.createDeepProxy(result.getIContextInfo(), true));
			} catch (Exception e) {
				Log.w(TAG, "Could not create proxy for: " + result.getIContextInfo());
				return;
			}
		}
		IBinder binder = null;
		if (receiver != null)
			binder = receiver.asBinder();
		else {
			binder = callback.asBinder();
		}
		/*
		 * We send context results using the following adaptive approach: 1. Try to send the data to the remote client.
		 * This will succeed only if the remote client has the proper classes available in its classpath for the
		 * Parcelable contained within the eventData. Otherwise, we'll generate an exception. 2. If sending the event
		 * causes an exception, it's most probably due to the lack of proper ContextData classes at the remote side
		 * (necessary for resolving the ContextData's Percelable eventData.) In this case, remove the eventData and
		 * re-send the event, which still contains strings. The client can still use the string representation of the
		 * eventData.
		 */
		try {
			if (binder != null && binder.isBinderAlive()) {
				// Try sending the event with the context info attached
				if (receiver != null)
					receiver.onContextResult(result);
				else
					callback.onSuccess(result);
				if (DynamixPreferences.isDetailedLoggingEnabled())
					Log.v(TAG, "Full ContextEvent successfully sent!");
			}
		} catch (Exception e) {
			/*
			 * Note that in some versions of Android (>4), remote exceptions cannot be caught reliably, so this catch
			 * handler will never run if the client lacks proper data-type JARs. The ContextResult object in the App JAR
			 * now handles exceptions when de-serializing attached IContextInfo entities on the client side.
			 */
			Log.w(TAG, "Exception during onContextResult: " + e.toString());
			DynamixApplication app = DynamixService.getDynamixApplication(result.getTargetAppId());
			// Check for memory errors
			if (result.getStreamController() != null && result.getStreamController().outOfMemory()) {
				Log.w(TAG, "ContextEvent contained too much data... dropping");
			} else {
				/*
				 * Try sending the event without the attachment, since the error is most-likely related to the client
				 * not having proper classes on the class-path.
				 */
				result.attachContextInfo(false);
				try {
					if (DynamixPreferences.isDetailedLoggingEnabled())
						Log.d(TAG, "Trying to send string-only ContextEvent: " + result);
					if (binder != null && binder.isBinderAlive()) {
						if (receiver != null)
							receiver.onContextResult(result);
						else
							callback.onSuccess(result);
						if (DynamixPreferences.isDetailedLoggingEnabled())
							Log.v(TAG, "String-only ContextEvent successfully sent!");
					}
				} catch (Exception e1) {
					Log.w(TAG,
							"notifyContextListeners failed to update listener after removing eventData: "
									+ e1.getMessage());
				}
			}
		}
	}

	/**
	 * Handles broadcasting events to each the specified IContextHandler.
	 * 
	 * @param receivers
	 *            The list of receivers.
	 * @param eventCommand
	 *            The command to send to receivers.
	 * @param receiversToRemove
	 *            The list of receivers to remove (may be null)
	 */
	private static void doHandlerEventBroadcast(final List<IContextHandler> receivers,
			final IEventCommand<IContextListener> eventCommand, final List<IContextHandler> receiversToRemove) {
		/*
		 * Handle send delay for the incoming eventCommand
		 */
		if (eventCommand.hasSendDelay() && !eventCommand.deliveryDelayElapsed()) {
			Timer t = new Timer(true);
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					doHandlerEventBroadcast(receivers, eventCommand, receiversToRemove);
				}
			}, eventCommand.getDeliveryTime());
		} else {
			if (receivers != null) {
				contextHandler.post(new Runnable() {
					@Override
					public void run() {
						// Handle pre-processing for the incoming EventCommand
						eventCommand.preProcess();
						try {
							for (IContextHandler receiver : receivers) {
								if (receiver.asBinder() != null && receiver.asBinder().isBinderAlive()) {
									if (DynamixPreferences.isDetailedLoggingEnabled())
										Log.v(TAG, "Sending " + eventCommand + " to receiver: " + receiver);
									// Broadcast event to each listener
									for (IContextListener l : receiver.getContextListenerResult()
											.getIContextListenerList()) {
										eventCommand.processCommand(l);
									}
								} else {
									if (DynamixPreferences.isDetailedLoggingEnabled())
										Log.w(TAG, "Failed sending " + eventCommand + " to receiver: " + receiver
												+ ", binder problem: " + receiver.asBinder());
								}
							}
						} catch (Exception e) {
							// The RemoteCallbackList will take care of removing the dead object for us.
							Log.w(TAG, "doHandlerEventBroadcast.RemoteException: " + e);
						}
						// Handle post-processing for the incoming EventCommand
						eventCommand.postProcess();
						// Handle removal, if necessary
						if (receiversToRemove != null) {
							for (IContextHandler remove : receiversToRemove) {
								contextHandlerList.unregister(remove);
							}
						}
					}
				});
			} else
				Log.w(TAG, "doHandlerEventBroadcast received null receiver list!");
		}
	}

	private static void doSessionEventBroadcast(final List<ISessionListener> receivers,
			final IEventCommand<ISessionListener> eventCommand, final List<ISessionListener> receiversToRemove) {
		/*
		 * Handle send delay for the incoming eventCommand
		 */
		if (eventCommand.hasSendDelay() && !eventCommand.deliveryDelayElapsed()) {
			Timer t = new Timer(true);
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					doSessionEventBroadcast(receivers, eventCommand, receiversToRemove);
				}
			}, eventCommand.getDeliveryTime());
		} else {
			if (receivers != null) {
				sessionHandler.post(new Runnable() {
					@Override
					public void run() {
						// Handle pre-processing for the incoming EventCommand
						eventCommand.preProcess();
						try {
							for (ISessionListener receiver : receivers)
								if (receiver.asBinder() != null && receiver.asBinder().isBinderAlive())
									eventCommand.processCommand(receiver);
						} catch (Exception e) {
							// The RemoteCallbackList will take care of removing the dead object for us.
							Log.w(TAG, "doSessionEventBroadcast.RemoteException: " + e);
						}
						// Handle post-processing for the incoming EventCommand
						eventCommand.postProcess();
						// Handle listener removal, if necessary
						if (receiversToRemove != null) {
							for (ISessionListener l : receiversToRemove)
								sessionListenerList.unregister(l);
						}
					}
				});
			} else
				Log.w(TAG, "doSessionEventBroadcast received null receiver list!");
		}
	}

	/**
	 * Utility RemoteCallbackList that removes a session listener using the ContextManager if onCallbackDied is called.
	 * 
	 * @author Darren Carlson
	 */
	private static class SessionListenerList extends RemoteCallbackList<ISessionListener> {
		@Override
		public void onCallbackDied(ISessionListener listener) {
			super.onCallbackDied(listener);
			Log.w(TAG, "Session Listener Died: " + listener);
			String deadAppId = null;
			synchronized (sessionMap) {
				IBinder deadBinder = listener.asBinder();
				for (String appId : sessionMap.keySet()) {
					if (sessionMap.get(appId).getSessionListener() != null)
						if (deadBinder.equals(sessionMap.get(appId).getSessionListener().asBinder())) {
							deadAppId = appId;
							break;
						}
				}
				if (deadAppId != null) {
					DynamixApplication app = sessionMap.get(deadAppId).getApp();
					DynamixService.closeAppSession(app, false);
				} else
					Log.w(TAG, "Could not find session for dead listener: " + listener);
			}
		}
	}

	/**
	 * Utility RemoteCallbackList that removes a context handler if onCallbackDied is called.
	 * 
	 * @author Darren Carlson
	 */
	private static class ContextHandlerCallbackList extends RemoteCallbackList<IContextHandler> {
		@Override
		public void onCallbackDied(IContextHandler handler) {
			super.onCallbackDied(handler);
			Log.w(TAG, "Context Handler Died: " + handler);
			String deadAppId = null;
			synchronized (sessionMap) {
				IBinder deadBinder = handler.asBinder();
				for (String appId : sessionMap.keySet()) {
					if (sessionMap.get(appId).getContextHandler(deadBinder) != null) {
						deadAppId = appId;
						break;
					}
				}
				if (deadAppId != null) {
					DynamixApplication app = sessionMap.get(deadAppId).getApp();
					DynamixService.closeAppSession(app, false);
				} else
					Log.w(TAG, "Could not find session for dead handler: " + handler);
			}
		}
	}
}
