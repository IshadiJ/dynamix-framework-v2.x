/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.AppConstants.PluginInstallStatus;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextType;
import org.ambientdynamix.api.application.DynamixFeature;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.AndroidFeatureInfo;
import org.ambientdynamix.api.contextplugin.ContextPluginDependency;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.security.Permission;
import org.ambientdynamix.core.FrameworkConstants.SystemPrivilege;
import org.ambientdynamix.util.Repository;

import android.util.Log;

/**
 * Meta-data describing an installable plugin capable of performing context interactions within the Dynamix Framework.
 * ContextPlugins operate in conjunction with an associated ContextPluginRuntime, which does the actual context
 * interaction work. The ContextPlugin class provides meta-data describing the plugin's name, description, version, etc.
 * This class also holds information regarding the install status and id of the ContextPlugin's underlying
 * ContextPluginRuntime (as an OSGi bundle). ContextPlugins are capable of being serialized and stored in an
 * object-based database (if necessary); however, ContextPluginRuntimes are typically not serializable due to their
 * often complex structures (hence their separation from the ContextPlugin).
 * 
 * @author Darren Carlson
 * @see ContextPluginRuntime
 * @see org.ambientdynamix.IContextInfo.IContextInfo
 * @see org.ambientdynamix.ContextResult.ContextEvent
 */
public class ContextPlugin implements Serializable {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private static final long serialVersionUID = 7084158670931809780L;
	// Data below should be mirrored in ContextPluginInformation
	private String id;
	private String name;
	private String provider;
	private String description;
	private String installUrl;
	private String updateUrl;
	private VersionInfo version;
	private PluginInstallStatus installStat;
	private boolean enabled = true;
	private List<ContextType> supportedContextTypes;
	private VersionInfo minPlatformVersion;
	private PluginConstants.PLATFORM targetPlatform;
	private boolean requiresConfiguration;
	private boolean configured;
	private ContextPluginType plugType;
	private VersionInfo maxPlatformVersion;
	private VersionInfo minDynamixVersion;
	private VersionInfo maxDynamixVersion;
	private Repository repoSource;
	private boolean backgroundService = false;
	private Map<String, String> extras;
	// Data below should NOT be mirrored in ContextPluginInformation
	private String runtimeFactoryClass;
	private long bundleId;
	private ContextPluginSettings settings;
	private Set<Permission> permissions;
	private Set<AndroidFeatureInfo> featureDependencies;
	private boolean dynamixManaged = true;
	private List<ContextPluginDependency> pluginDependencies = new ArrayList<ContextPluginDependency>();
	private List<DynamixFeature> dynamixFeatures = new ArrayList<DynamixFeature>();
	private SystemPrivilege systemPrivilige = SystemPrivilege.NORMAL;

	/**
	 * Base constructor. Sets up a ContextPlugin with bundleId = -1 and PluginInstallStatus.NOT_INSTALLED.
	 */
	public ContextPlugin() {
		// Setup initial state
		this.bundleId = -1;
		this.installStat = PluginInstallStatus.NOT_INSTALLED;
		supportedContextTypes = new ArrayList<ContextType>();
		permissions = new LinkedHashSet<Permission>();
		featureDependencies = new LinkedHashSet<AndroidFeatureInfo>();
		pluginDependencies = new ArrayList<ContextPluginDependency>();
		extras = new HashMap<String, String>();
		systemPrivilige = SystemPrivilege.NORMAL;
	}

	/**
	 * Creates a new ContextPlugin.
	 * 
	 * @param id
	 *            The unique ID of the ContextPlugin. Note: This must be exactly the same as the 'Symbolic name'
	 *            declared in the OSGi Bundle manifest containing this plugin (or the plugin will fail to load).
	 * @param name
	 *            The name of the plugin for display purposes.
	 * @param provider
	 *            The provider of the plugin (e.g. organization)
	 * @param version
	 *            The version of plugin.
	 * @param description
	 *            A description of the plugin for display purposes.
	 * @param userControlledContextAcquisition
	 *            Whether or not this plugin requires user controlled context acquisition (e.g. through a GUI).
	 * @param supportedContextTypes
	 *            The context types that this plugin supports (as strings).
	 * @param runtimeFactoryClass
	 *            Class capable of creating this ContextPlugin's runtime.
	 * @param requiresConfiguration
	 *            Indicates if this plugin requires configuration before it can be used.
	 * @param hasConfigurationView
	 *            Indicates if thie plugin has a configuration view that the user can adjust settings with.
	 * @param installUrl
	 *            The installation URL for this plug-in.
	 * @param updateUrl
	 *            The update URL for this plug-in.
	 * @param plugType
	 *            This plug-in's ContextPluginType.
	 */
	public ContextPlugin(String id, String name, String provider, VersionInfo version, String description,
			List<ContextType> supportedContextTypes, String runtimeFactoryClass, boolean requiresConfiguration,
			String installUrl, String updateUrl, ContextPluginType plugType, Repository repoSource,
			Map<String, String> extras) {
		// Call base constructor
		this();
		this.id = id;
		this.name = name;
		this.provider = provider;
		this.description = description;
		this.version = version;
		this.supportedContextTypes = supportedContextTypes;
		this.runtimeFactoryClass = runtimeFactoryClass;
		this.requiresConfiguration = requiresConfiguration;
		this.installUrl = installUrl;
		this.updateUrl = updateUrl;
		this.plugType = plugType;
		this.repoSource = repoSource;
		this.extras = extras;
	}

	/**
	 * Creates a new ContextPlugin.
	 * 
	 * @param id
	 *            The unique ID of the ContextPlugin. Note: This must be exactly the same as the 'Symbolic name'
	 *            declared in the OSGi Bundle manifest containing this plugin (or the plugin will fail to load).
	 * @param name
	 *            The name of the plugin for display purposes.
	 * @param provider
	 *            The provider of the plugin (e.g. organization).
	 * @param version
	 *            The version of plugin.
	 * @param description
	 *            A description of the plugin for display purposes.
	 * @param userControlledContextAcquisition
	 *            Whether or not this plugin requires user controlled context acquisition (e.g. through a GUI).
	 * @param supportedContextTypes
	 *            The context types that this plugin supports (as strings).
	 * @param runtimeFactoryClass
	 *            Class capable of creating this ContextPlugin's runtime.
	 * @param requiresConfiguration
	 *            Indicates if this plugin requires configuration before it can be used.
	 * @param hasConfigurationView
	 *            Indicates if thie plugin has a configuration view that the user can adjust settings with.
	 * @param installUrl
	 *            The installation URL for this plug-in.
	 * @param updateUrl
	 *            The update URL for this plug-in.
	 * @param permissions
	 *            The permissions assigned to this plug-in.
	 * @param plugType
	 *            This plug-in's ContextPluginType.
	 */
	public ContextPlugin(String id, String name, String provider, VersionInfo version, String description,
			List<ContextType> supportedContextTypes, String runtimeFactoryClass, boolean requiresConfiguration,
			String installUrl, String updateUrl, Set<Permission> permissions, ContextPluginType plugType,
			Repository repoSource, Map<String, String> extras) {
		this(id, name, provider, version, description, supportedContextTypes, runtimeFactoryClass,
				requiresConfiguration, installUrl, updateUrl, plugType, repoSource, extras);
		this.permissions = permissions;
	}

	public Map<String, String> getExtras() {
		// Make sure extras is not null
		if (extras == null)
			extras = new HashMap<String, String>();
		// Check if the plug-in is installed
		if (installStat == PluginInstallStatus.INSTALLED && !extras.containsKey(PluginConstants.PRIVATE_DATA_DIR)) {
			// Place the private data path into the extras
			extras.put(PluginConstants.PRIVATE_DATA_DIR, Utils.getPluginPrivateDataDir(this).getAbsolutePath());
		}
		return this.extras;
	}

	public void setExtras(Map<String, String> extras) {
		this.extras = extras;
	}

	public SystemPrivilege getSystemPrivilege() {
		return systemPrivilige;
	}

	public void setsystemPrivilige(SystemPrivilege privilege) {
		this.systemPrivilige = privilege;
	}

	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Make sure the id's and version numbers are the same
		ContextPlugin other = (ContextPlugin) candidate;
		if (other.getId().equalsIgnoreCase(this.getId()))
			if (other.getVersion().equals(this.getVersion()))
				if (other.getRepoSource().equals(this.getRepoSource()))
					return true;
		return false;
	}

	/**
	 * Unique identification of a ContextPlugin requires both the String id and associated VersionInfo.
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + getId().hashCode() + getVersion().hashCode() + repoSource.hashCode();
		return result;
	}

	/**
	 * Set true if this plug-in should be automatically managed for context support registrations by Dynamix; false if a
	 * choreography plug-in will handle installs.
	 */
	public void setDynamixManaged(boolean dynamixManaged) {
		this.dynamixManaged = dynamixManaged;
	}

	/**
	 * Returns true if this plug-in should be automatically managed for context support registrations by Dynamix; false
	 * if a choreography plug-in will handle installs.
	 */
	public boolean isDynamixManaged() {
		return this.dynamixManaged;
	}

	/**
	 * Returns true if this plug-in should run constantly in the background, regardless of context support registrations
	 * or screen state.
	 */
	public boolean isBackgroundService() {
		return this.backgroundService;
	}

	/**
	 * Set true if this plug-in should run constantly in the background, regardless of context support registrations or
	 * screen state; false, otherwise.
	 */
	public void setBackgroundService(boolean isBackgroundService) {
		this.backgroundService = isBackgroundService;
	}

	/**
	 * Returns the bundle identifier associated with this ContextPlugin's ContextPluginRuntime or -1 if the bundle is
	 * not installed.
	 */
	public long getBundleId() {
		return bundleId;
	}

	/**
	 * Returns the ContextPluginInfomration associated with the ContextPlugin
	 */
	public ContextPluginInformation getContextPluginInformation() {
		// Does not support <= 2.1.7 JARs
		return new ContextPluginInformation(getId(), getName(), getDescription(), getVersion(),
				getSupportedContextTypes(), getInstallStatus(), getContextPluginType(), requiresConfiguration(),
				isConfigured(), isEnabled(), isBackgroundService(), getExtras(), getDynamixFeatures(), getRepoSource()
						.getRepositoryInfo());
	}

	/**
	 * Returns the ContextPluginSettings for this ContextPlugin, or null if there are no settings.
	 */
	public ContextPluginSettings getContextPluginSettings() {
		return settings;
	}

	/**
	 * Returns the ContextPluginType
	 */
	public ContextPluginType getContextPluginType() {
		return plugType;
	}

	/**
	 * Returns true if this plug-in is a library; false otherwise.
	 */
	public boolean isLibrary() {
		return this.plugType == ContextPluginType.LIBRARY;
	}

	/**
	 * Returns true if this plug-in originated from the official Dynamix repo; false otherwise.
	 */
	public boolean isVerifiedDynamixPlugin() {
		if (extras != null) {
			if (extras.get(ContextPluginInformation.EXTRAS_DYNAMIX_REPO) != null) {
				return Boolean.getBoolean(extras.get(ContextPluginInformation.EXTRAS_DYNAMIX_REPO));
			}
		}
		return false;
	}

	/**
	 * Returns the description of this ContextPlugin.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the set of features that this ContextPlugin depends on.
	 */
	public Set<AndroidFeatureInfo> getFeatureDependencies() {
		return featureDependencies;
	}

	/**
	 * Returns the globally unique identifier for this ContextPlugin.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the current InstallStatus for this ContextPlugin
	 * 
	 * @see PluginInstallStatus
	 */
	public PluginInstallStatus getInstallStatus() {
		return this.installStat;
	}

	/**
	 * Returns the installation URL for the associated ContextPluginRuntime's OSGi bundle.
	 */
	public String getInstallUrl() {
		return installUrl;
	}

	/**
	 * Returns the maximum Dynamix framework version for this ContextPlugin.
	 */
	public VersionInfo getMaxDynamixVersion() {
		return maxDynamixVersion;
	}

	/**
	 * Gets the maximum platform version for this ContextPlugin.
	 */
	public VersionInfo getMaxPlatformVersion() {
		return maxPlatformVersion;
	}

	/**
	 * Returns the minimum required Dynamix version
	 */
	public VersionInfo getMinDynamixVersion() {
		return minDynamixVersion;
	}

	/**
	 * Returns the minimum required platform version for this plug-in
	 */
	public VersionInfo getMinPlatformVersion() {
		return minPlatformVersion;
	}

	/**
	 * Returns the name of the ContextPlugin.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the Set of Permissions associated with this ContextPlugin.
	 */
	public Set<Permission> getPermissions() {
		return permissions;
	}

	/**
	 * Returns true if the permission identified by the permission string is granted; false otherwise.
	 * 
	 * @param permission
	 *            The permission string to check.
	 * @see org.ambientdynamix.api.contextplugin.security.Permissions
	 */
	public boolean hasPermission(String permission) {
		if (getPermissions() != null)
			for (Permission p : getPermissions()) {
				if (p.getPermissionString().equalsIgnoreCase(permission))
					return p.isPermissionGranted();
			}
		return false;
	}

	/**
	 * Returns the provider of the plug-in
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * Gets the fully qualified classname of this class' ContextPluginRuntimeFactory.
	 * 
	 * @return
	 */
	public String getRuntimeFactoryClass() {
		return runtimeFactoryClass;
	}

	/**
	 * Returns a read-only List of the ContextPlugin's supported context types.
	 */
	public List<ContextType> getSupportedContextTypes() {
		return supportedContextTypes;
	}

	/**
	 * Returns the target platform
	 */
	public PluginConstants.PLATFORM getTargetPlatform() {
		return targetPlatform;
	}

	/**
	 * Returns the update URL for the plug-in.
	 */
	public String getUpdateUrl() {
		return updateUrl;
	}

	/**
	 * Returns the version of this ContextPlugin.
	 */
	public VersionInfo getVersion() {
		return version;
	}

	/**
	 * Returns true if this ContextPlugin has feature dependencies; false otherwise.
	 */
	public boolean hasFeatureDependencies() {
		if (featureDependencies != null && featureDependencies.size() > 0)
			return true;
		else
			return false;
	}

	/**
	 * Returns true if this ContextPlugin has a maximum Dynamix framework version.
	 */
	public boolean hasMaxFrameworkVersion() {
		return maxDynamixVersion != null ? true : false;
	}

	/**
	 * Returns true if this ContextPlugin has a maximum platform api level.
	 */
	public boolean hasMaxPlatformVersion() {
		return maxPlatformVersion != null ? true : false;
	}

	/**
	 * Returns true if this ContextPlugin has required Permissions; false otherwise.
	 */
	public boolean hasPermissions() {
		if (permissions != null && permissions.size() > 0)
			return true;
		else
			return false;
	}

	/**
	 * Returns true if the plugin is configured; false otherwise. For plug-ins that don't require configuraiton this
	 * method always returns true.
	 */
	public boolean isConfigured() {
		if (!requiresConfiguration)
			// If we don't require configuration, we're configured
			return true;
		else
			return configured;
	}

	/**
	 * Returns true if this ContextPlugin is enabled; false otherwise.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Returns true if the ContextPlugin's ContextPluginRuntime is installed; false otherwise.
	 */
	public boolean isInstalled() {
		synchronized (installStat) {
			return getInstallStatus() == PluginInstallStatus.INSTALLED;
		}
	}

	/**
	 * Returns true if the plugin requires configuration; false otherwise.
	 */
	public boolean requiresConfiguration() {
		return requiresConfiguration;
	}

	/**
	 * Sets the bundle identifier associated with this ContextPlugin's ContextPluginRuntime. Note that this value should
	 * be -1 if the bundle is not installed.
	 * 
	 * @param bundleId
	 */
	public void setBundleId(long bundleId) {
		this.bundleId = bundleId;
	}

	/**
	 * Sets if the plugin is configured.
	 */
	public void setConfigured(boolean configured) {
		this.configured = configured;
	}

	/**
	 * Sets the ContextPluginSettings for this ContextPlugin.
	 * 
	 * @param settings
	 */
	public void setContextPluginSettings(ContextPluginSettings settings) {
		this.settings = settings;
	}

	/**
	 * Sets the ContextPluginType
	 * 
	 * @param plugType
	 *            The ContextPluginType
	 */
	public void setContextPluginType(ContextPluginType plugType) {
		this.plugType = plugType;
	}

	/**
	 * Sets the description of this ContextPlugin.
	 * 
	 * @param description
	 *            The new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets whether or not this ContextPlugin is enabled.
	 * 
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets the features that this ContextPlugin depends on.
	 */
	public void setFeatureDependencies(Set<AndroidFeatureInfo> featureDependencies) {
		this.featureDependencies = featureDependencies;
	}

	/**
	 * Sets the globally unique identifier for this ContextPlugin.
	 * 
	 * @param id
	 *            The new id.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets the InstallStatus for this ContextPlugin.
	 * 
	 * @see PluginInstallStatus
	 */
	public void setInstallStatus(PluginInstallStatus stat) {
		synchronized (installStat) {
			this.installStat = stat;
		}
	}

	/**
	 * Sets the installation URL for the associated ContextPluginRuntime's OSGi bundle.
	 */
	public void setInstallUrl(String installUrl) {
		this.installUrl = installUrl;
	}

	/**
	 * Sets the maximum Dynamix framework version for this ContextPlugin.
	 */
	public void setMaxFrameworkVersion(VersionInfo maxFrameworkVersion) {
		this.maxDynamixVersion = maxFrameworkVersion;
	}

	/**
	 * Sets the maximum platform version for this ContextPlugin.
	 */
	public void setMaxPlatformVersion(VersionInfo maxPlatformVersion) {
		this.maxPlatformVersion = maxPlatformVersion;
	}

	/**
	 * Sets the minimum required Dynamix version
	 */
	public void setMinDynamixVersion(VersionInfo minDynamixVersion) {
		this.minDynamixVersion = minDynamixVersion;
	}

	/**
	 * Sets the minimum required platform version for this plug-in
	 */
	public void setMinPlatformVersion(VersionInfo minPlatformVersion) {
		this.minPlatformVersion = minPlatformVersion;
	}

	/**
	 * Sets the name of the ContextPlugin.
	 * 
	 * @param name
	 *            The new name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the Permissions for this ContextPlugin.
	 * 
	 * @param permissions
	 *            the set of permissions to set
	 */
	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Sets the plug-in provider.
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * Sets if the plugin requires configuration
	 */
	public void setRequiresConfiguration(boolean requiresConfiguration) {
		this.requiresConfiguration = requiresConfiguration;
	}

	/**
	 * Sets the fully qualified classname of this class' ContextPluginRuntimeFactory. Note: runtimeFactoryClass must
	 * reference a class of type ContextPluginRuntimeFactory
	 * 
	 * @param runtimeFactoryClass
	 */
	public void setRuntimeFactoryClass(String runtimeFactoryClass) {
		this.runtimeFactoryClass = runtimeFactoryClass;
	}

	/**
	 * Sets the ContextPlugin's supported context types (as strings).
	 * 
	 * @param The
	 *            new List of supported context type strings.
	 */
	public void setSupportedContextTypes(List<ContextType> supportedContextTypes) {
		this.supportedContextTypes = supportedContextTypes;
	}

	/**
	 * Sets the target platform
	 * 
	 * @param targetPlatform
	 *            The target platform string
	 */
	public void setTargetPlatform(PluginConstants.PLATFORM targetPlatform) {
		this.targetPlatform = targetPlatform;
	}

	/**
	 * Sets the update URL for the plug-in.
	 */
	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	/**
	 * Sets the version of this ContextPlugin.
	 */
	public void setVersionInfo(VersionInfo currentVersion) {
		this.version = currentVersion;
	}

	/**
	 * Returns true if the ContextPlugin supports the context type; false otherwise.
	 * 
	 * @param contextType
	 *            The context type string to check.
	 */
	public boolean supportsContextType(String contextType) {
		synchronized (supportedContextTypes) {
			for (ContextType type : supportedContextTypes) {
				if (type.getId().equalsIgnoreCase(contextType))
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns the repository source for this plug-in.
	 */
	public Repository getRepoSource() {
		return repoSource;
	}

	/**
	 * Returns true if this plug-in has remote (i.e., network-based) dependencies; false otherwise.
	 */
	public boolean hasRemoteDependencies() {
		if (isDependent()) {
			for (ContextPluginDependency dependent : getPluginDependencies()) {
				ContextPlugin plug = DynamixService.getContextPlugin(dependent.getPluginId(),
						dependent.getPluginVersion());
				if (plug != null) {
					if (plug.getRepoSource().isNetworkSource())
						return true;
					else
						return plug.hasRemoteDependencies();
				} else {
					Log.w(TAG, "Could not find plug-in for dependent: " + dependent);
				}
			}
		}
		return false;
	}

	/**
	 * Sets the repository source for this plug-in.
	 */
	public void setRepoSource(Repository repoSource) {
		this.repoSource = repoSource;
	}

	/**
	 * Returns this plug-in's Context Plug-in dependencies.
	 */
	public List<ContextPluginDependency> getPluginDependencies() {
		return pluginDependencies;
	}

	/**
	 * Returns true if this plug-in depends on other plug-ins; false otherwise.
	 */
	public boolean isDependent() {
		return pluginDependencies != null && pluginDependencies.size() > 0;
	}

	/**
	 * Sets this plug-in's Context Plug-in dependencies.
	 */
	public void setPluginDependencies(List<ContextPluginDependency> pluginDependencies) {
		this.pluginDependencies = pluginDependencies;
	}

	/**
	 * Returns the list of Dynamix features provided by this plug-in.
	 */
	public List<DynamixFeature> getDynamixFeatures() {
		return dynamixFeatures;
	}

	/**
	 * Sets the list of Dynamix features provided by this plug-in.
	 */
	public void setDynamixFeatures(List<DynamixFeature> dynamixFeatures) {
		this.dynamixFeatures = dynamixFeatures;
	}

	@Override
	public String toString() {
		return name + " | " + id + " v" + version + " | " + repoSource + " | install state: " + installStat;
	}
}