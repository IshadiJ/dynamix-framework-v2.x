/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.ISessionCallback;
import org.ambientdynamix.api.application.ISessionListener;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

/**
 * The WebFacadeBinder provides an implementation of the IDynamixFacade API for web clients. This class is used in
 * combination with the DynamixService to handle API calls from Dynamix web applications. Note that not all
 * IDynamixFacade methods are supported for web applications.
 * 
 * @see IDynamixFacade
 * @author Darren Carlson
 */
public class WebFacadeBinder extends AppFacadeBinder {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private String webAppId;

	/**
	 * Creates a WebFacadeBinder.
	 */
	public WebFacadeBinder(Context context, ContextManager conMgr, boolean embeddedMode, String webAppId) {
		super(context, conMgr, embeddedMode, true);
		this.webAppId = webAppId;
	}

	/**
	 * Returns the Dynamix application associated with this binder.
	 */
	public DynamixApplication getDynamixApp() {
		return getAuthorizedApplication(getCallerId());
	}

	/**
	 * Not supported for web clients (throws UnsupportedOperationException).
	 */
	@Override
	public Result requestContextPluginInstallation(ContextPluginInformation plugInfo) throws RemoteException {
		// Throw Unsupported Exception
		throw new UnsupportedOperationException();
	}

	/**
	 * Not supported for web clients (throws UnsupportedOperationException).
	 */
	@Override
	public Result requestContextPluginUninstall(ContextPluginInformation plugInfo) throws RemoteException {
		// Throw Unsupported Exception
		throw new UnsupportedOperationException();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DynamixApplication createNewApplicationFromCaller(String id, boolean admin) {
		// Construct a new application for the caller
		DynamixApplication app = new DynamixApplication(APP_TYPE.WEB, id, id);
		app.setAppDescription("Web Agent");
		app.addAppDetail(DynamixApplication.VERSION_CODE, "1.0");
		return app;
	}
	
	public DynamixApplication getAuthorizedApplication(String id) {
		if (id != null) {
			return DynamixService.getDynamixApplication(id);
		} else {
			Log.w(TAG, "getAuthorizedApplication received null application id");
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getCallerId() {
		// Log.d(TAG, "Returning caller ID: " + webAppId);
		return this.webAppId;
	}
}
