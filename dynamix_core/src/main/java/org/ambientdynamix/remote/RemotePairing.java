/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.remote;

import java.util.ArrayList;
import java.util.List;

import org.ambientdynamix.api.application.ContextPluginInformation;

/**
 * Represents a pairing with a remote entity.
 * 
 * @author Darren Carlson
 *
 */
public class RemotePairing {
	public String TAG = getClass().getSimpleName();
	private String encryptionKey;
	private String httpToken;
	private boolean temporary;
	private long timestamp; // when was this pairing generated?
	private boolean dashboardEnabled = false;
	private List<ContextPluginInformation> allowedDashboardPlugins = new ArrayList<ContextPluginInformation>();
	private String dashboardThemeId;

	/**
	 * Creates a RemotePairing with an encryptionKey. Note that the encryptionKey must be Base64 encoded as per
	 * Base64.NO_WRAP (omitting all line terminators, i.e., the output will be on one long line).
	 */
	public RemotePairing(boolean temporary, String encryptionKey) {
		this.encryptionKey = encryptionKey;
		this.temporary = temporary;
		timestamp = System.currentTimeMillis();
	}

	/**
	 * Creates a RemotePairing with an encryptionKey and http token. Note that the encryptionKey must be Base64 encoded
	 * as per Base64.NO_WRAP (omitting all line terminators, i.e., the output will be on one long line).
	 */
	public RemotePairing(boolean temporary, String encryptionKey, String httpToken) {
		this(temporary, encryptionKey);
		this.httpToken = httpToken;
	}

	/**
	 * Returns the http token associated with this remote pairing.
	 */
	public String getHttpToken() {
		return this.httpToken;
	}

	/**
	 * Returns true if there is an http token associated with this remote pairing; false, otherwise.
	 */
	public boolean hasHttpToken() {
		return this.httpToken != null;
	}

	/**
	 * Sets the http token associated with this remote pairing.
	 */
	public void setHttpToken(String httpToken) {
		this.httpToken = httpToken;
	}

	/**
	 * Returns the encryption key.
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}

	/**
	 * Sets the encryption key.
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	/**
	 * Returns true if this pairing is temporary (i.e., lasting 1 session); false if permanent.
	 */
	public boolean isTemporary() {
		return temporary;
	}

	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Returns true if this remote pairing exposes a dashboard to remote clients; false otherwise.
	 */
	public boolean isDashboardEnabled() {
		return dashboardEnabled;
	}

	/**
	 * Set true if this remote pairing exposes a dashboard to remote clients; false otherwise.
	 */
	public void setDashboardEnabled(boolean dashboardEnabled) {
		this.dashboardEnabled = dashboardEnabled;
	}

	/**
	 * Returns the list of plug-ins that are allowed to be exposed by the dashboard.
	 */
	public List<ContextPluginInformation> getAllowedDashboardPlugins() {
		return allowedDashboardPlugins;
	}

	/**
	 * Sets the list of plug-ins that are allowed to be exposed by the dashboard.
	 */
	public void setAllowedDashboardPlugins(List<ContextPluginInformation> allowedDashboardPlugins) {
		this.allowedDashboardPlugins = allowedDashboardPlugins;
	}

	/**
	 * Returns the dashboard theme id for this remote pairing, or possibly null if the dashboard isn't enabled.
	 */
	public String getDashboardThemeId() {
		return dashboardThemeId;
	}

	/**
	 * Sets the dashboard theme id for this remote pairing.
	 */
	public void setDashboardThemeId(String dashboardThemeId) {
		this.dashboardThemeId = dashboardThemeId;
	}
}
