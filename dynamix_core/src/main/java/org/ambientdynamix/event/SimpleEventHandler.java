/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.event;

import android.content.Intent;
import android.util.Log;

import org.ambientdynamix.api.contextplugin.ContextInfoSet;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginContextListener;
import org.ambientdynamix.api.contextplugin.IPluginEventHandler;
import org.ambientdynamix.api.contextplugin.PluginConstants.EventType;
import org.ambientdynamix.core.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Simple implementation of the IPluginEventHandler interface. Automatically creates a snapshot of the current listener
 * list before sending events.
 *
 * @author Darren Carlson
 */
public class SimpleEventHandler implements IPluginEventHandler {
    // Private data
    private final String TAG = this.getClass().getSimpleName();
    private List<IPluginContextListener> listeners = new ArrayList<IPluginContextListener>();

    /**
     * Creates a SimpleEventHandler using the incoming listener.
     */
    public SimpleEventHandler(IPluginContextListener listener) {
        if (listener == null)
            throw new RuntimeException("Event listener cannot be null!");
        addContextListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addContextListener(IPluginContextListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeContextListener(IPluginContextListener listener) {
        listeners.remove(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendSuccess(final ContextPluginRuntime sender, final UUID responseId) {
        if (sender != null) {
            if (responseId != null) {
                List<IPluginContextListener> snapshot = new ArrayList<IPluginContextListener>(listeners);
                for (final IPluginContextListener l : snapshot) {
                    Utils.dispatch(true, new Runnable() {
                        @Override
                        public void run() {
                            l.onContextRequestSuccess(sender.getSessionId(), responseId);
                        }
                    });
                }
                snapshot.clear();
                snapshot = null;
            } else
                Log.w(TAG, "responsetId was null... aborting sendError");
        } else
            Log.w(TAG, "Sender was null... aborting sendError");
    }

    @Override
    public void sendSuccess(final ContextPluginRuntime sender, final UUID responseId, final Intent intent) {
        if (sender != null) {
            if (responseId != null) {
                List<IPluginContextListener> snapshot = new ArrayList<IPluginContextListener>(listeners);
                for (final IPluginContextListener l : snapshot) {
                    Utils.dispatch(true, new Runnable() {
                        @Override
                        public void run() {
                            l.onContextRequestSuccess(sender.getSessionId(), responseId, intent);
                        }
                    });
                }
                snapshot.clear();
                snapshot = null;
            } else
                Log.w(TAG, "responsetId was null... aborting sendError");
        } else
            Log.w(TAG, "Sender was null... aborting sendError");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendError(final ContextPluginRuntime sender, final UUID responsetId, final String errorMessage,
                          final int errorCode) {
        if (sender != null) {
            if (responsetId != null) {
                List<IPluginContextListener> snapshot = new ArrayList<IPluginContextListener>(listeners);
                for (final IPluginContextListener l : snapshot) {
                    Utils.dispatch(true, new Runnable() {
                        @Override
                        public void run() {
                            l.onContextRequestFailed(sender.getSessionId(), responsetId, errorMessage, errorCode);
                        }
                    });
                }
                snapshot.clear();
                snapshot = null;
            } else
                Log.w(TAG, "responsetId was null... aborting sendError");
        } else
            Log.w(TAG, "Sender was null... aborting sendError");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendEvent(final ContextPluginRuntime sender, final ContextInfoSet infoSet) {
        if (sender != null) {
            if (infoSet != null) {
                // Ensure that UNICAST events have a responseId
                if (infoSet.getEventType() == EventType.UNICAST && infoSet.getResponseId() == null) {
                    Log.w(TAG, "responseId was null for multicast event... aborting sendEvent");
                } else {
                    List<IPluginContextListener> snapshot = new ArrayList<IPluginContextListener>(listeners);
                    for (final IPluginContextListener l : snapshot) {
                        Utils.dispatch(true, new Runnable() {
                            @Override
                            public void run() {
                                l.onPluginContextEvent(sender.getSessionId(), infoSet);
                            }
                        });
                    }
                    snapshot.clear();
                    snapshot = null;
                }
            } else
                Log.w(TAG, "InfoSet was null... aborting sendEvent");
        } else
            Log.w(TAG, "Sender was null... aborting sendEvent");
    }
}