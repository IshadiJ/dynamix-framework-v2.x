/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.UUID;

import android.os.Bundle;

/**
 * Represents a context listener for a specific context type and optional listener configuration.
 * 
 * @author Darren Carlson
 *
 */
public class ContextListenerInformation {
	private UUID listenerId;
	private String contextType;
	private Bundle listenerConfig;

	public ContextListenerInformation(UUID listenerId, String contextType, Bundle listenerConfig) {
		this.listenerId = listenerId;
		this.contextType = contextType;
		this.listenerConfig = listenerConfig;
	}

	public UUID getListenerId() {
		return listenerId;
	}

	public String getContextType() {
		return contextType;
	}

	public Bundle getListenerConfig() {
		return listenerConfig;
	}

	@Override
	public int hashCode() {
		return listenerId.hashCode();
	}

	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Check if the id's are the same
		ContextListenerInformation other = (ContextListenerInformation) candidate;
		return listenerId.equals(other.getListenerId());
	}
}
