/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import org.ambientdynamix.api.contextplugin.security.Permissions;

import android.app.Activity;
import android.util.Log;

/**
 * This class provides plug-ins limited control of the HostActivity (e.g., for closing the host).
 * 
 * @author Darren Carlson
 *
 */
public class ActivityController {
	public final String TAG = getClass().getSimpleName();
	private Activity activity;
	private boolean isActivityAvailable = false;

	/**
	 * Creates an ActivityController.
	 */
	public ActivityController(Activity activity, boolean isActivityAvailable) {
		this.activity = activity;
		this.isActivityAvailable = isActivityAvailable;
	}

	/**
	 * Calls finish() on the underlying activity.
	 */
	public final void closeActivity() {
		Log.v(TAG, "ActivityController.closeActivity");
		activity.finish();
	}

	/**
	 * Returns the Host Activity associated with this controller, or null if the plug-in does not have
	 * Permissions.ACCESS_FULL_CONTEXT.
	 */
	public final Activity getActivity() {
		return isActivityAvailable ? activity : null;
	}
}
