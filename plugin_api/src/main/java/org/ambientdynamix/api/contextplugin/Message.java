/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.util.UUID;

import org.ambientdynamix.api.application.ContextPluginInformation;

import android.os.Bundle;

/**
 * Represents a message sent between plug-ins.
 * 
 * @author Darren Carlson
 * 
 */
public class Message {
	private ContextPluginInformation sender;
	private ContextPluginInformation receiver;
	private Bundle message;
	private IMessageResultHandler resultHandler;
	private UUID messageId;

	/**
	 * Creates a Message.
	 * 
	 * @param sender
	 *            The sender of the message.
	 * @param receiver
	 *            The inteded receiver of the message.
	 * @param message
	 *            The message contents, as a Bundle.
	 * @param resultHandler
	 *            A handler that the receiver of this message can use to send back results or failure information to the
	 *            sender.
	 */
	public Message(ContextPluginInformation sender, ContextPluginInformation receiver, Bundle message,
			IMessageResultHandler resultHandler) {
		this.sender = sender;
		this.receiver = receiver;
		this.message = message;
		this.resultHandler = resultHandler;
		this.messageId = UUID.randomUUID();
	}

	/**
	 * Returns the unique identifier associated with this message.
	 */
	public UUID getMessageId() {
		return this.messageId;
	}

	/**
	 * Returns the sender of this message.
	 */
	public ContextPluginInformation getSender() {
		return sender;
	}

	/**
	 * Returns the intended recipient of this message.
	 */
	public ContextPluginInformation getReceiver() {
		return receiver;
	}

	/**
	 * Returns the message contents, as a Bundle.
	 */
	public Bundle getMessage() {
		return message;
	}

	/**
	 * Returns a handler that the receiver of this message can use to send back results or failure information to the
	 * sender.
	 */
	public IMessageResultHandler getResultHandler() {
		return resultHandler;
	}

	/**
	 * Returns true if this message has a result handler; false otherwise.
	 */
	public boolean hasResultHandler() {
		return resultHandler != null;
	}

	@Override
	public int hashCode() {
		return this.messageId.hashCode();
	}

	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Make sure the id's and version numbers are the same
		Message other = (Message) candidate;
		return this.messageId.equals(other.messageId);
	}
}