/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.List;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IDynamixFacade;

/**
 * Provides a set of events for receiving session information from a Dynamix Framework session.
 *
 * @author Darren Carlson
 */
interface ISessionListener
{	
	/**
	 * Notification that the Dynamix session has been opened.
	 */	
	oneway void onSessionOpened(in String sessionId);
	
	/**
	 * Notification that the Dynamix session has been closed.
	 */	
	oneway void onSessionClosed();
	
	/**
	 * Notification that the Dynamix Framework is active.
	 */
	oneway void onDynamixFrameworkActive();
	
	/**
	 * Notification that the Dynamix Framework is inactive.
	 */
	oneway void onDynamixFrameworkInactive();
	
	/**
	 * Notification that context plug-in discovery has started.
	 */	
	oneway void onContextPluginDiscoveryStarted();
	
	/**
	 * Notification that context plug-in discovery has finished.
	 */		
	oneway void onContextPluginDiscoveryFinished();
	
	/**
	 * Notification that a Context Plug-in has been enabled.
	 */
	oneway void onContextPluginEnabled(in ContextPluginInformation plug);
	
	/**
	 * Notification that a Context Plug-in has been disabled.
	 */
	oneway void onContextPluginDisabled(in ContextPluginInformation plug);
	
	/**
	 * Notification that a Context Plug-in has encountered an error (and mostly likely has been disabled).
	 */
	oneway void onContextPluginError(in ContextPluginInformation plug, in String message, in int errorCode);
	
	/**
	 * Notification that a new Context Plug-in has been installed.
	 * @param plugin The Context Plug-in that was installed. 
	 */
	oneway void onContextPluginInstalled(in ContextPluginInformation plugin);
	
	/**
	 * Notification that a Context Plug-in has been uninstalled.
	 * @param plugin The Context Plug-in that was uninstalled. 
	 */
	oneway void onContextPluginUninstalled(in ContextPluginInformation plugin);
}