/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.ISessionListener;
import org.ambientdynamix.api.application.IPluginInstallCallback;
import org.ambientdynamix.api.application.IContextHandler;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.ContextPluginInformationResult;
import org.ambientdynamix.api.application.IContextHandlerCallback;
import org.ambientdynamix.api.application.ISessionCallback;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;

/**
 * This class provides the primary interface between apps and the Dynamix Framework.
 *
 * @author Darren Carlson
 */
interface IDynamixFacade
{
	/**
	 * Returns the Dynamix Framework version that is attached to this IDynamixFacade.
	 * This method executes synchronously.
	 */
	VersionInfo getDynamixVersion();
	
	/**
	 * Returns true if Dynamix is active; false otherwise.
	 * This method executes synchronously.
	 * @return True if Dynamix is active; false otherwise.
	 */
	boolean isDynamixActive();
	
	/**
	 * Returns true if the application's session is open; false otherwise.
	 * @return True if the application's session is open; false otherwise.
	 */
	boolean isSessionOpen();
	
	/**
	 * Opens the Dynamix session.
	 * This method executes asynchronously.
	 */
	void openSession();
	
	/**
	 * Opens the Dynamix session.
	 * This method executes asynchronously.
	 * @param callback The callback to update when the operation completes.
	 */
	void openSessionWithCallback(ISessionCallback callback);
	
	/**
	 * Opens the Dynamix session.
	 * This method executes asynchronously.
	 * @param listener The session listener.
	 */
	void openSessionWithListener(in ISessionListener listener);
	
	/**
	 * Opens the Dynamix session.
	 * This method executes asynchronously.
	 * @param listener The session listener.
	 * @param callback The callback to update when the operation completes.
	 */
	void openSessionWithListenerAndCallback(in ISessionListener listener, in ISessionCallback callback);
	
	/**
	 * Sets a listener for session events. Replaces the existing listener, if one was already registered (e.g., during openSessionWithListener).
	 * This method executes asynchronously.
	 * @param listener The session listener.
	 */
	void setSessionListener(in ISessionListener listener);
	
	/**
	 * Closes the Dynamix session.
	 * This method executes asynchronously.
	 */
	void closeSession();
	
	/**
	 * Closes the Dynamix session.
	 * This method executes asynchronously.
	 * @param callback The callback to update when the operation completes.
	 */
	void closeSessionWithCallback(in ICallback callback);

	/**
	 * Creates an IContextHandler using the listener.
	 * This method executes asynchronously.
	 * @param handler The listener for the handler, which receives broadcast events
	 * Multi-unicast events with subscription ids managed by Dynamix
	 */
	void createContextHandler(in IContextHandlerCallback callback);
	
	/**
	 * Removes the IContextHandler from the session. 
	 * This method executes asynchronously.
	 * @param handler The handler to remove.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result removeContextHandler(in IContextHandler handler);
	
	/**
	 * Removes the IContextHandler from the session. 
	 * This method executes asynchronously.
	 * @param handler The handler to remove.
	 * @param callback A callback that can be used to track the success or failure of this method.
	 */	
	void removeContextHandlerWithCallback(in IContextHandler handler, in ICallback callback);
	
	/**
	 * Opens the specified plug-in's default configuration view (if it has one).
	 * This method executes synchronously.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result openDefaultContextPluginConfigurationView(in String pluginId, in String pluginVersion);
	
	/**
	 * Opens the specified plug-in's configuration view (if it has one).
	 * This method executes synchronously.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result openContextPluginConfigurationView(in String pluginId, in String pluginVersion, in Bundle viewConfig);
	
	/**
	 * Request that Dynamix install a specific ContextPlugin on behalf of the Application. Such a request might be made 
	 * if an application has a dependency on a specific ContextPlugin. If the installation request is accepted by Dynamix, this 
	 * method operates asynchronously (consider using requestContextPluginInstallationWithCallback to track results).
	 * 
	 * @param plugInfo The plugin to install.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result requestContextPluginInstallation(in ContextPluginInformation plugInfo);
	
	/**
	 * Request that Dynamix install a specific ContextPlugin on behalf of the Application. Such a request might be made 
	 * if an application has a dependency on a specific ContextPlugin. If the installation request is accepted by Dynamix, this 
	 * method returns its results asynchronously using the IPluginInstallCallback. 
	 * 
	 * @param plugInfo The plugin to install.
	 * @param callback A callback that can be used to track the installation status.
	 */	
	void requestContextPluginInstallationWithCallback(in ContextPluginInformation plugInfo, in IPluginInstallCallback callback);
	
	/**
	 * Request that Dynamix uninstall a specific ContextPlugin on behalf of the Application.  If the request is accepted by Dynamix, this 
	 * method operates asynchronously (consider using requestContextPluginUninstallWithCallback to track results).
	 * 
	 * @param plugInfo The plugin to uninstall.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result requestContextPluginUninstall(in ContextPluginInformation plugInfo); 
	
	/**
	 * Request that Dynamix uninstall a specific ContextPlugin on behalf of the Application. 
	 * This method operates asynchronously. 
	 * 
	 * @param plugInfo The plugin to uninstall.
	 * @param callback A callback that can be used to track the success or failure of this method.
	 */
	void requestContextPluginUninstallWithCallback(in ContextPluginInformation plugInfo, in ICallback callback); 
	
	/**
	 * Returns all plug-ins known by Dynamix, regardless of installation status (you can use the returned context plug-in information to determine install status).
	 * This method operates synchronously.
	 */
	ContextPluginInformationResult getAllContextPluginInformation();
	
	/**
	 * Returns all installed plug-ins (note that installing plug-ins are not included).
	 * This method operates synchronously.
	 */
	ContextPluginInformationResult getInstalledContextPluginInformation();
	
	/**
	 * Returns the plug-in information associated with the specified plug-in id.
	 * This method operates synchronously.
	 */	
	ContextPluginInformationResult getContextPluginInformation(String pluginId);
	
	/**
	 * Returns the plug-ins that support the specified context type.
	 * This method operates synchronously.
	 */	
	ContextPluginInformationResult getAllContextPluginInformationForType(String contextType);
	
	/**
	 * Returns the id associated with the incoming handler.
	 * This method operates asynchronously.
	 */
	IdResult getHandlerId(IContextHandler handler);
	
	/**
	 * Returns the facade's session id.
	 * This method operates synchronously.
	 */
	IdResult getSessionId();
}