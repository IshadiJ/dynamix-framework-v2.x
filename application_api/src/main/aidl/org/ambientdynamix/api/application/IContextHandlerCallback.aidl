/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import org.ambientdynamix.api.application.ContextHandler;

/**
 * Callback for context handler installation.
 *
 * @author Darren Carlson
 */
interface IContextHandlerCallback
{
	/**
	 * Notification that the context handler has been added.
	 * @param handler The context handler that was added.
	 */
	oneway void onSuccess(in ContextHandler handler);
	
	/**
	 * Notification that the context handler could not be added.
	 * @param message The error message.
	 * @param errorCode The error code (see Dynamix error codes for details).
	 */
	oneway void onFailure(in String message, in int errorCode);
}
