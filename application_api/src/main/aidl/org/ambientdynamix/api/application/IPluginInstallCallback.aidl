/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.List;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;

/**
 * Provides a set of events and methods for receiving plug-in installation status events.
 *
 * @author Darren Carlson
 */
interface IPluginInstallCallback
{	
	/**
	 * Raised when a new Context Plug-in is being installed.
	 * @param plugin The Context Plug-in being installed.
	 */
	oneway void onInstallStarted(in ContextPluginInformation plugin);
	
	/**
	 * Raised when a new Context Plug-in installation has progressed.
	 * @param plugin The Context Plug-in being installed.
	 * @param plugin The Context Plug-in installation percent complete.
	 */
	oneway void onInstallProgress(in ContextPluginInformation plugin, in int percentComplete);
	
	/**
	 * Raised when a Context Plug-in has failed to install. 
	 *
	 * @param plugin The Context Plug-in that failed to install.
	 * @param message The message associated with the failure.
	 */
	oneway void onInstallFailed(in ContextPluginInformation plugin, in String errorMessage, in int errorCode);
	
	/**
	 * Raised when a new Context Plug-in has been installed.
	 * @param plugin The Context Plug-in that was installed. 
	 */
	oneway void onInstallComplete(in ContextPluginInformation plugin);
}