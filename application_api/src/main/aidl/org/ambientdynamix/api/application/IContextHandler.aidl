/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.List;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.IContextSupportCallback;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.IContextListener;
import org.ambientdynamix.api.application.IContextListenerResult;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.api.application.VersionedPlugin;


/**
 * Provides a set of methods for accessing context support from a Dynamix Framework instance. 
 *
 * @author Darren Carlson
 */
interface IContextHandler
{	
	/**
	 * Returns the list of registered context listeners.
	 */
	IContextListenerResult getContextListenerResult();
	
	/**
	 * Adds context support for the specified context type using the specified plug-in.
	 *
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addPluginContextSupport(in String pluginId, in String contextType);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in.
	 *
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addPluginContextSupportWithCallback(in String pluginId, in String contextType, in IContextSupportCallback callback);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in and listener.

	 *
	 * @param pluginId The plug-in to use.	 
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addPluginContextSupportWithListener(in String pluginId, in String contextType, in IContextListener listener);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in and listener.

	 *
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param listener The context listener. 
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addPluginContextSupportWithListenerAndCallback(in String pluginId, in String contextType, in IContextListener listener, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addPluginConfiguredContextSupport(in String pluginId, in String contextType, in Bundle contextSupportConfig);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *
	 * @param pluginIds The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addPluginsConfiguredContextSupport(in List<String> pluginIds, in String contextType, in Bundle contextSupportConfig);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addPluginConfiguredContextSupportWithCallback(in String pluginId, in String contextType, in Bundle contextSupportConfig, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *
	 * @param pluginIds The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */		
	void addPluginsConfiguredContextSupportWithCallback(in List<String> pluginIds, in String contextType, in Bundle contextSupportConfig, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *	
	 * @param pluginId The plug-in to use. 
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addPluginConfiguredContextSupportWithListener(in String pluginId, in String contextType, in Bundle contextSupportConfig, in IContextListener listener);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *	
	 * @param pluginIds The plug-ins to use. 
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */		
	Result addPluginsConfiguredContextSupportWithListener(in List<String> pluginIds, in String contextType, in Bundle contextSupportConfig, in IContextListener listener);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *	
	 * @param pluginId The plug-in to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	 
	 * @param listener The context listener.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addPluginConfiguredContextSupportWithListenerAndCallback(in String pluginId, in String contextType, in Bundle contextSupportConfig, in IContextListener listener, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *	
	 * @param pluginIds The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	 
	 * @param listener The context listener.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */		
	void addPluginsConfiguredContextSupportWithListenerAndCallback(in List<String> pluginIds, in String contextType, in Bundle contextSupportConfig, in IContextListener listener, in IContextSupportCallback callback);
	
	/**
	 * Returns the context support that has been registered by this handler.
	 *
	 * @return A ContextSupportResult
	 */	
	ContextSupportResult getContextSupport(); 
	
	/**
	 * Removes previously added context support. 
	 *
	 * @param info The context support to remove. 
	 */
	void removeContextSupport(in ContextSupportInfo info);
	
	/**
	 * Removes previously added context support. 
	 *
	 * @param info The context support to remove. 
	 * @param callback An optional callback that can be used to track the success or failure of this request.
	 */	
	void removeContextSupportWithCallback(in ContextSupportInfo info, in ICallback callback);
	
	/**
	 * Removes all context support for the specified contextType.  
	 *
	 * @param contextType The context support type to remove.
	 */	
	void removeContextSupportForContextType(in String contextType);
	
	/**
	 * Removes all context support for the specified contextType.  
	 *
	 * @param contextType The context support type to remove. 
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void removeContextSupportForContextTypeWithCallback(in String contextType, in ICallback callback);
	
	/**
	 * Removes all previously added context support for all listeners, regardless of contextType. 
	 */	
	void removeAllContextSupport();
	
	/**
	 * Removes all previously added context support for all listeners, regardless of contextType. 
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void removeAllContextSupportWithCallback(in ICallback callback);
	
	/**
	 * Resends all ContextEvents that have been cached by Dynamix for the listener. ContextEvents are provided to 
	 * applications according to Dynamix authentication and security policies defined by Dynamix users.
	 * Note that the requesting application will only received context info they are subscribed to.
	 * Events from this method are returned via this listener's onContextEvent method.
	 *
	 * @param listener The listener to re-send context events for.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result resendAllCachedContextEvents(in IContextListener listener);
	
	/**
	 * Resends the ContextEvent entities that have been cached for the listener within the specified 
	 * past number of milliseconds. If the number of milliseconds is longer than the max cache time, then all cached 
	 * events are returned. ContextEvents are provided to applications according to Dynamix authentication 
	 * and security policies. Note that the requesting application will only received context info they are subscribed to.
	 * Events from this method are returned via this listener's onContextEvent method.
	 * 
	 * @param listener The listener to re-send context events for.
	 * @param previousMills The time (in milliseconds) to retrieve past events.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result resendCachedContextEvents(in IContextListener listener, in int previousMills);
	
	/**
	 * Resends all ContextEvents (of the specified contextType) that have been cached by Dynamix for the listener. 
	 * ContextEvents are provided to applications according to Dynamix authentication and security policies defined by Dynamix users.
	 * Note that the requesting application will only received context info they are subscribed to.
	 * Events from this method are returned via this listener's onContextEvent method.
	 *
	 * @param listener The listener to re-send context events for.
	 * @param contextType The type of ContextEvent to return.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result resendAllTypedCachedContextEvents(in IContextListener listener, in String contextType);
	
	/**
	 * Resends the ContextEvent entities (of the specified contextType) that have been cached for the listener within the specified 
	 * past number of milliseconds. If the number of milliseconds is longer than the max cache time, then all appropriate cached
	 * events are returned. ContextEvents are provided to applications according to Dynamix authentication 
	 * and security policies. Note that the requesting application will only received context info they are subscribed to.
	 * Events from this method are returned via this listener's onContextEvent method.
	 * 
	 * @param listener The listener to re-send context events for.
	 * @param contextType The type of ContextEvent to return.
	 * @param previousMills The time (in milliseconds) to retrieve past events.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result resendTypedCachedContextEvents(in IContextListener listener, in String contextType, int previousMills);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * If you need results, use a context request method with a callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result pluginContextRequest(in String pluginId, in String contextType);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * If you need results, use a context request method with a callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType The type of context info to interact with.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result versionedPluginContextRequest(in String pluginId, in String pluginVersion, in String contextType);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * The results of this method are returned via the callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param callback The callback for the request.
	 */
	void pluginContextRequestWithCallback(in String pluginId, in String contextType, in IContextRequestCallback callback);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * The results of this method are returned via the callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType The type of context info to interact with.
	 * @param callback The callback for the request.
	 */	
	void versionedPluginContextRequestWithCallback(in String pluginId, in String pluginVersion, in String contextType, in IContextRequestCallback callback);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * The results of this method are returned via the callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param callback The callback for the request.
	 * @param status A callback for receiving context support installation status updates.
	 */
	void pluginContextRequestWithCallbackAndStatus(in String pluginId, in String contextType, in IContextRequestCallback callback, in IContextSupportCallback status);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction (sensing, control or actuation) using the specified plugin and contextType.
	 * This method will automatically install context support, if needed.
	 * The results of this method are returned via the callback.
	 * Note that this method is only available for ContextPlugins that support programmatic access (i.e., reactive context plug-in types).
	 *	
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType The type of context info to interact with.
	 * @param callback The callback for the request.
	 * @param status A callback for receiving context support installation status updates.
	 */	
	void versionedPluginContextRequestWithCallbackAndStatus(in String pluginId, in String pluginVersion, in String contextType, in IContextRequestCallback callback, in IContextSupportCallback status);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plugin.
	 * If you need results, use a context request method with a callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result configuredPluginContextRequest(in String pluginId, in String contextType, in Bundle contextConfig);	
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plugin.
	 * If you need results, use a context request method with a callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */
	Result configuredVersionedPluginContextRequest(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextConfig);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plug-in.
	 * The results of this method are returned via the callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @param callback The callback for the request.
	 */
	void configuredPluginContextRequestWithCallback(in String pluginId, in String contextType, in Bundle contextConfig, in IContextRequestCallback callback);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plug-in.
	 * The results of this method are returned via the callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType The type of context info to interact with.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @param callback The callback for the request.
	 */	
	void configuredVersionedPluginContextRequestWithCallback(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextConfig, in IContextRequestCallback callback);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plug-in.
	 * The results of this method are returned via the callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param contextType The type of context info to interact with.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @param callback The callback for the request.
	 * @param status A callback for receiving context support installation status updates.
	 */
	void configuredPluginContextRequestWithCallbackAndStatus(in String pluginId, in String contextType, in Bundle contextConfig, in IContextRequestCallback callback, in IContextSupportCallback status);
	
	/**
	 * Requests that Dynamix perform a dedicated context interaction using the specified plug-in.
	 * The results of this method are returned via the callback.
	 * Context requests may be of several types and are plug-in specific (see the plug-in documentation for details). 
	 * For some plug-ins, a context request may returns specific contextual information obtained from the environment. 
	 * For other plug-ins, a context request may be a change in the underlying contextual situation (e.g., playing a media file on a nearby media renderer).
	 * See the plugin's documentation for configuration options that can be included in the contextConfig Bundle.
	 *
	 * @param pluginId The id of the plugin that should perform the context request.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType The type of context info to interact with.
	 * @param contextConfig A plug-in specific Bundle of context request configuration options.
	 * @param callback The callback for the request.
	 * @param status A callback for receiving context support installation status updates.
	 */
	void configuredVersionedPluginContextRequestWithCallbackAndStatus(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextConfig, in IContextRequestCallback callback, in IContextSupportCallback status);
	
	
	
	
	
	

	
	/**
	 * Adds context support for the specified context type using the specified plug-in.
	 *
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addVersionedPluginContextSupport(in String pluginId, in String pluginVersion, in String contextType);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in.
	 *
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addVersionedPluginContextSupportWithCallback(in String pluginId, in String pluginVersion, in String contextType, in IContextSupportCallback callback);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in and listener.
	 *
	 * @param pluginId The plug-in to use.	
	 * @param pluginVersion The plug-in version to use. 
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addVersionedPluginContextSupportWithListener(in String pluginId, in String pluginVersion, in String contextType, in IContextListener listener);
	
	/**
	 * Adds context support for the specified context type using the specified plug-in and listener.
	 *
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param listener The context listener. 
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addVersionedPluginContextSupportWithListenerAndCallback(in String pluginId, in String pluginVersion, in String contextType, in IContextListener listener, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addVersionedPluginConfiguredContextSupport(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextSupportConfig);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addVersionedPluginConfiguredContextSupportWithCallback(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextSupportConfig, in IContextSupportCallback callback);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *	
	 * @param pluginId The plug-in to use. 
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addVersionedPluginConfiguredContextSupportWithListener(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextSupportConfig, in IContextListener listener);
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *	
	 * @param pluginId The plug-in to use.
	 * @param pluginVersion The plug-in version to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	 
	 * @param listener The context listener.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */	
	void addVersionedPluginConfiguredContextSupportWithListenerAndCallback(in String pluginId, in String pluginVersion, in String contextType, in Bundle contextSupportConfig, in IContextListener listener, in IContextSupportCallback callback);			
	
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *
	 * @param plugins The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */	
	Result addVersionedPluginsConfiguredContextSupport(in List<VersionedPlugin> plugins, in String contextType, in Bundle contextSupportConfig);
		
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *
	 * @param plugins The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */		
	void addVersionedPluginsConfiguredContextSupportWithCallback(in List<VersionedPlugin> plugins, in String contextType, in Bundle contextSupportConfig, in IContextSupportCallback callback);
		
	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle.
	 *	
	 * @param plugins The plug-ins to use. 
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	
	 * @param listener The context listener.
	 * @return A Result indicating if Dynamix accepted the request or not.
	 */		
	Result addVersionedPluginsConfiguredContextSupportWithListener(in List<VersionedPlugin> plugins, in String contextType, in Bundle contextSupportConfig, in IContextListener listener);
	

	/**
	 * Adds context support using the specified plug-in, context type and configuration Bundle. 
	 *	
	 * @param plugins The plug-ins to use.
	 * @param contextType A String describing the requested context type. Note that
	 * the contextType string must reference a valid context type string (as described in the developer documentation).	 
	 * @param contextSupportConfig A Bundle describing the requested context support. 
	          See the developer documentation for a description of available configuration options.	 
	 * @param listener The context listener.
	 * @param callback A callback that can be used to track the success or failure of this request.
	 */		
	void addVersionedPluginsConfiguredContextSupportWithListenerAndCallback(in List<VersionedPlugin> plugins, in String contextType, in Bundle contextSupportConfig, in IContextListener listener, in IContextSupportCallback callback);
}