/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.List;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;

/**
 * Listener interface for receiving context events from Dynamix.
 *
 * @author Darren Carlson
 */
interface IContextListener
{
	/**
	 * Notification that this context listener has been removed.
	 */
	oneway void onContextListenerRemoved();
	
	/**
	 * Notification of an incoming context result.
	 * @param event The context result.
	 * @see ContextResult
	 */
	oneway void onContextResult(in ContextResult result);
	
	/**
	 * Notification that a previously installed context support has been removed (e.g., if a serving plug-in was in installed).
	 * @param message The error message.
	 * @param errorCode The error code (see Dynamix error codes for details).
	 */
	oneway void onContextSupportRemoved(in ContextSupportInfo support, in String message, in int errorCode);
}