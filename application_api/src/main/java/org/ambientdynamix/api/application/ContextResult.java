/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.zip.Deflater;

/**
 * Represents a parameterized result of a Dynamix context interaction. It contains the 'native' object-based result data
 * (as an IContextInfo attachment), the time that the result was generated, how long the result is valid (including
 * forever), and (optionally) various string representations of the IContextInfo. String-based context representations
 * are used by clients who may not have access to a given IContextInfo data-type JAR on their class-path, but are still
 * interested in working with the context information. <br/>
 * This class also provides low-level interprocess communication (IPC) features. First, it provides transparent
 * (in-memory) compression and decompression of both string-based and (optionally) object-based context representations
 * to help improve IPC performance. String-based context representations are always compressed. Second, it provides
 * high-performance IPC streaming, which is able to transport arbitrary amounts of data across Android's IPC transport
 * mechanism, which normally has a 100KB hard limit. A StreamController is used to guard against excessive memory
 * consumption during streaming.
 *
 * @author Darren Carlson
 * @see IContextInfo
 */
public class ContextResult extends Expirable implements Serializable, Parcelable, IContextInfo {
    /**
     * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
     */
    public static final Parcelable.Creator<ContextResult> CREATOR = new Parcelable.Creator<ContextResult>() {
        @Override
        public ContextResult createFromParcel(Parcel in) {
            try {
                return new ContextResult(in);
            } catch (Exception e) {
                Log.w(TAG, e);
                throw new RuntimeException(e);
            }
        }

        @Override
        public ContextResult[] newArray(int size) {
            return new ContextResult[size];
        }
    };
    // Private variables
    private static final long serialVersionUID = 5110811252364172334L;
    private final static String TAG = ContextResult.class.getSimpleName();
    private ContextPluginInformation resultSource;
    private Map<String, String> contextRepresentationStrings = new HashMap<String, String>();
    private Map<String, byte[]> contextRepresentationStringsBytes = new HashMap<String, byte[]>();
    private IContextInfo contextInfo;
    private byte[] contextInfoBytes;
    private String contextType = "org.ambientdynamix.type";
    private boolean attachContextInfo;
    private String targetAppId;
    private String responseId;
    private IStreamController streamController;
    private UUID resultId = UUID.randomUUID();
    private boolean useStreaming = false;
    private boolean DEBUG = false;
    private boolean autoWebEncode = true;
    private String webEncodingFormat = AppConstants.JSON_WEB_ENCODING;

    /**
     * Creates a ContextResult using the incoming IContextInfo. Automatically Provides the current local time as a
     * timestamp and specifies no expiration (i.e., the context result is valid forever).
     */
    public ContextResult(IContextInfo contextInfo) {
        this(contextInfo, new Date(), -1);
    }

    /**
     * Creates a ContextResult using the incoming IContextInfo. Automatically provides the current local time as a
     * timestamp and specifies a specific expireMills (in milliseconds).
     */
    public ContextResult(IContextInfo contextInfo, int expireMills) {
        this(contextInfo, new Date(), expireMills);
    }

    /**
     * Creates a ContextResult using the incoming IContextInfo, timeStamp and expireMills (in milliseconds).
     */
    public ContextResult(IContextInfo contextInfo, Date timeStamp, int expireMills) {
        super(timeStamp, expireMills);
        this.contextInfo = contextInfo;
        this.contextType = contextInfo.getContextType();
        if (contextInfo != null)
            attachContextInfo = true;
        else
            attachContextInfo = false;
        if (contextInfo.getStringRepresentationFormats() != null) {
            for (String format : contextInfo.getStringRepresentationFormats()) {
                contextRepresentationStrings.put(format, contextInfo.getStringRepresentation(format));
            }
        }
    }

    /**
     * Setups up this result for streaming.
     *
     * @param streamController
     */
    public void prepStreaming(IStreamController streamController) {
        /*
         * Include methods for prep here
		 */
        this.streamController = streamController;
        if (contextInfo != null) {
            Parcel p = Parcel.obtain();
            p.writeParcelable(contextInfo, 0);
            contextInfoBytes = p.marshall();
            p.recycle();
        }
        try {
            // Compress the string-based context representations
            for (String format : contextRepresentationStrings.keySet()) {
                contextRepresentationStringsBytes.put(format,
                        Utils.zipString(contextRepresentationStrings.get(format), Deflater.BEST_SPEED));
            }
        } catch (Exception e) {
            Log.w(TAG, "Error in ContextResult constructor: " + e);
        }
        useStreaming = true;
    }

    /**
     * Returns the IStreamController, if set.
     */
    public IStreamController getStreamController() {
        return this.streamController;
    }

    /**
     * Writes the result out to IPC.
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        if (DEBUG)
            Log.d(TAG, "Writing ContextResult as Parcel");
        // Write out metadata
        try {
            out.writeByte((byte) (useStreaming ? 1 : 0));
            out.writeString(this.responseId);
            out.writeParcelable(resultSource, flags);
            out.writeSerializable(super.getTimeStamp());
            out.writeInt(super.getExpireMills());
            out.writeString(contextType);
            // Write the total number of string-based formats we're holding
            out.writeInt(contextRepresentationStrings.size());
            if (useStreaming) {
                streamController.start();
                // Stream out each string-based format...
                for (final String s : contextRepresentationStringsBytes.keySet()) {
                    if (DEBUG)
                        Log.v(TAG, "Writing format: " + s);
                    out.writeString(s);
                    final byte[] bytes = contextRepresentationStringsBytes.get(s);
                    if (DEBUG)
                        Log.v(TAG, "Streaming format content with byte length: " + bytes.length);
                    out.writeStrongBinder(new IDataInputStream.Stub() {
                        private final ByteArrayInputStream in = new ByteArrayInputStream(bytes);

                        @Override
                        public int read(byte[] buffer) throws RemoteException {
                            try {
                                if (streamController.outOfMemory()) {
                                    Log.e(TAG, "Closing stream and throwing RemoteException!");
                                    in.close();
                                    throw new Exception(
                                            "ContextResult contained too much data. Try reducing query scope.");
                                }
                                return in.read(buffer);
                            } catch (Exception e) {
                                throw new RemoteException();
                            }
                        }
                    });
                }
            } else {
                // Don't use streaming
                for (String format : contextRepresentationStrings.keySet()) {
                    out.writeString(format);
                    out.writeString(contextRepresentationStrings.get(format));
                }
            }
            // Stream out the IContextInfo, if the attachment is requested...
            out.writeByte((byte) (attachContextInfo ? 1 : 0));
            if (attachContextInfo) {
                if (useStreaming) {
                    if (streamController.outOfMemory()) {
                        throw new Exception("ContextResult contained too much data. Try reducing query scope.");
                    }
                    out.writeStrongBinder(new IDataInputStream.Stub() {
                        private final ByteArrayInputStream in = new ByteArrayInputStream(contextInfoBytes);

                        @Override
                        public int read(byte[] buffer) throws RemoteException {
                            try {
                                if (streamController.outOfMemory()) {
                                    Log.e(TAG, "Closing stream and throwing RemoteException!");
                                    in.close();
                                    throw new Exception(
                                            "ContextResult contained too much data. Try reducing query scope.");
                                } else
                                    return in.read(buffer);
                            } catch (Exception e) {
                                throw new RemoteException();
                            }
                        }
                    });
                } else {
                    out.writeParcelable(contextInfo, flags);
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "Error serializing ContextResult: " + e);
        } finally {
            if (streamController != null)
                streamController.stop();
        }
    }

    /**
     * A streaming-capable version of 'readParcelable'.
     */
    private ContextResult(Parcel in) throws Exception {
        this.useStreaming = in.readByte() == 1;
        this.responseId = in.readString();
        this.resultSource = in.readParcelable(getClass().getClassLoader());
        this.setTimeStamp((Date) in.readSerializable());
        this.setExpireMills(in.readInt());
        this.contextType = in.readString();
        // Convert the byte stream back into string-based context representations
        contextRepresentationStrings = new HashMap<String, String>();
        // Read the number of formats present
        int totalFormats = in.readInt();
        // Read in each format
        if (useStreaming) {
            for (int i = 0; i < totalFormats; i++) {
                String format = in.readString();
                if (DEBUG)
                    Log.v(TAG, "Receiving streamed string-based format: " + format);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                IDataInputStream dataStream = IDataInputStream.Stub.asInterface(in.readStrongBinder());
                byte[] buffer = new byte[8192];
                int size;
                try {
                    while ((size = dataStream.read(buffer)) != -1) {
                        out.write(buffer, 0, size);
                    }
                } catch (Exception e) {
                    throw new Exception("Problem reading context representation strings: " + e);
                }
                if (DEBUG)
                    Log.v(TAG, "Received " + format + ", which has a compressed size of: " + out.toByteArray().length);
                contextRepresentationStrings.put(format, Utils.unZipString(out.toByteArray()));
            }
        } else {
            for (int i = 0; i < totalFormats; i++) {
                contextRepresentationStrings.put(in.readString(), in.readString());
            }
        }
        // Check if we've got an IContextInfo attachement
        this.attachContextInfo = in.readByte() == 1;
        if (attachContextInfo) {
            if (useStreaming) {
                // Convert the byte stream map back into a IContextInfo representation
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                IDataInputStream data = IDataInputStream.Stub.asInterface(in.readStrongBinder());
                byte[] buffer = new byte[8192];
                int read;
                try {
                    while ((read = data.read(buffer)) != -1) {
                        out.write(buffer, 0, read);
                    }
                    // Store the stream's raw bytes
                    byte[] contextInfoBytes = out.toByteArray();
                    // Read and return the IContextInfo object using the class' classloader
                    try {
                        Parcel p = Parcel.obtain();
                        p.unmarshall(contextInfoBytes, 0, contextInfoBytes.length);
						/*
						 * Unmarshall the byte array using a Parcel, and make sure to set the Parcel's read position to
						 * 0, since the read position will be at the end of the object.
						 */
                        p.setDataPosition(0);
                        this.contextInfo = p.readParcelable(getClass().getClassLoader());
                        p.recycle();
                    } catch (Exception e) {
                        throw new Exception("Could not extract IContextInfo... necessary classes are not available");
                    }
                } catch (Exception e) {
                    throw new Exception("Exception during readContextInfoStream: " + e);
                } finally {
                    // Close the output stream
                    try {
                        out.close();
                    } catch (IOException e) {
                    }
                }
            } else {
                try {
                    this.contextInfo = in.readParcelable(getClass().getClassLoader());
                } catch (Exception e) {
                    Log.w(TAG, "Could not read parcelable (check that required data type classes are on build path): "
                            + e);
                    this.attachContextInfo = false;
                }
            }
        }
    }

    /**
     * If 'attachContextInfo' is true, the embedded IContextInfo object will be serialized during the 'writeToParcel'
     * method, which is used to send the object to a remote receiver (in another process) using Android IPC. If
     * 'attachContextInfo' is false, the IContextInfo object will be not serialized during 'writeToParcel'. This is
     * useful because not all remote clients will have the proper IContextInfo implementation classes on their
     * class-path. By switching off attachments, a string-only version of the result is sent, which is guaranteed to
     * succeed.
     *
     * @param attachContextInfo True if the attachment should be attached; false otherwise.
     */
    public void attachContextInfo(boolean attachContextInfo) {
        this.attachContextInfo = attachContextInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextType() {
        return this.contextType;
    }

    /**
     * Returns the source of this result.
     */
    public ContextPluginInformation getResultSource() {
        return this.resultSource;
    }

    /**
     * Returns the IContextInfo associated with this result, or null if no IContextInfo is attached. IContextInfo may
     * not be attached if the context source didn't provide a native context representation or if the receiver doesn't
     * have the correct data type JAR on its classpath. In such cases, the result's string-based context representations
     * can be used instead.
     */
    public IContextInfo getIContextInfo() {
        return contextInfo;
    }

    /**
     * Sets the result's IContextInfo.
     */
    public void setIContextInfo(IContextInfo contextInfo) {
        this.contextInfo = contextInfo;
    }

    /**
     * Returns the response id associated with this result, or null if no response id is assigned (e.g., for broadcast).
     */
    public String getResponseId() {
        return responseId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStringRepresentation(String format) {
        if (contextRepresentationStrings.containsKey(format)) {
            return contextRepresentationStrings.get(format);
        } else
            return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getStringRepresentationFormats() {
        return this.contextRepresentationStrings.keySet();
    }

    /**
     * Gets the target app id (for unicast).
     */
    public String getTargetAppId() {
        return targetAppId;
    }

    /**
     * Returns true if this result has attached IContextInfo; false otherwise.
     */
    public boolean hasIContextInfo() {
        return getIContextInfo() != null ? true : false;
    }

    /**
     * Sets the result source.
     */
    public void setResultSource(ContextPluginInformation eventSource) {
        if (eventSource == null) {
            Log.w(TAG, "setResultSource received null");
        }
        this.resultSource = eventSource;
    }

    /**
     * Sets the responseId (for unicast).
     */
    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    /**
     * Sets the target app id (for unicast).
     */
    public void setTargetAppId(String targetAppId) {
        this.targetAppId = targetAppId;
    }

    /**
     * Returns true if this result should be automatically encoded for Web clients; false otherwise.
     */
    public boolean autoWebEncode() {
        return autoWebEncode;
    }

    /**
     * Returns the requested auto Web encoding format for the result.
     */
    public String getWebEncodingFormat() {
        return this.webEncodingFormat;
    }

    /**
     * Sets the result up for auto web encoding by Dynamix using JSON. The underlying IContextInfo object will be
     * encoded using JavaBean conventions. Note that IContextInfo objects MUST adhere to JavaBean conventions for
     * auto-web-encoding to succeed: http://en.wikipedia.org/wiki/JavaBeans#JavaBean_conventions
     */
    public void setAutoWebEncode() {
        this.autoWebEncode = true;
        this.webEncodingFormat = AppConstants.JSON_WEB_ENCODING;
    }

    /**
     * Sets the result up for auto web encoding by Dynamix using the specified webEncodingFormat. Dynamix will NOT
     * auto-web-encode the IContextData in this case. Rather, the IContextInfo's string-based representation format
     * matching the specified webEncodingFormat will be used directly. If the IContextInfo doesn't provide the specified
     * webEncodingFormat, no data will be web encoded. See IContextInfo.getStringRepresentation(String format);
     */
    public void setManualWebEncode(String webEncodingFormat) {
        this.autoWebEncode = false;
        this.webEncodingFormat = webEncodingFormat;
    }

    /**
     * Sets the result up so that no Web encoding will occur. In this case, the result cannot be used by Web clients.
     */
    public void setNoWebEncoding() {
        this.autoWebEncode = false;
        this.webEncodingFormat = AppConstants.NO_AUTO_WEB_ENCODING;
    }
}