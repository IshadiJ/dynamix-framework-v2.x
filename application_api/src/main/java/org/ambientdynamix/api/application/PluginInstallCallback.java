/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.IBinder;
import android.os.RemoteException;

/**
 * Base implementation of IPluginInstallCallback.Stub with no functionality. Override methods as needed.
 *
 * @author Darren Carlson
 * @see IPluginInstallCallback
 */
public class PluginInstallCallback extends IPluginInstallCallback.Stub {
    private IPluginInstallCallback injected;

    public PluginInstallCallback() {
    }

    public PluginInstallCallback(IPluginInstallCallback injected) {
        this.injected = injected;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onInstallStarted(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onInstallStarted(plugin);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onInstallProgress(ContextPluginInformation plugin, int percentComplete) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onInstallProgress(plugin, percentComplete);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onInstallFailed(ContextPluginInformation plugin, String errorMessage, int errorCode)
            throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onInstallFailed(plugin, errorMessage, errorCode);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onInstallComplete(ContextPluginInformation plugin) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onInstallComplete(plugin);
    }

    /**
     * Override asBinder() as final to prevent framework NPE's.
     */
    @Override
    public final IBinder asBinder() {
        return super.asBinder();
    }
}
