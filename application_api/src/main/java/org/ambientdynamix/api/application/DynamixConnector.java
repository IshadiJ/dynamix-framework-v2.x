/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

/**
 * Convenience class that handles connecting to a Dynamix Framework instance and (optionally) opening a session.
 * 
 * @author Darren Carlson
 * 
 */
public class DynamixConnector {
	// Private data
	private final static String TAG = DynamixConnector.class.getSimpleName();
	private static IDynamixFacade facade;
	private static Context context;
	private static ISessionListener listener;
	private static ISessionCallback callback;
	private static boolean openSession = false;
	private static Handler handler;
	private static CONNECT_STATE state = CONNECT_STATE.DISCONNECTED;

	static {
		if (Looper.myLooper() == null)
			Looper.prepare();
		handler = new Handler();
	}

	public interface ConnectionListener {
		void onDisconnected();
	}

	/**
	 * Connection states.
	 */
	public enum CONNECT_STATE {
		DISCONNECTED, CONNECTING, AUTHORIZING, CONNECTED
	}

	/**
	 * Returns the connection state.
	 */
	public static CONNECT_STATE getConnectState() {
		return state;
	}

	/**
	 * Returns true if the DynamixConnector is connected to Dynamix; false otherwise.
	 */
	public static boolean isConnected() {
		return state == CONNECT_STATE.CONNECTED;
	}

	/**
	 * Returns a nice wrapper for IDynamixFacade instances that provides the method overloading that raw AIDL types
	 * lack, or null if Dynamix is not connected.
	 */
	public static DynamixFacade getDynamixFacade() {
		return new DynamixFacade(DynamixConnector.facade);
	}

	/**
	 * Returns the associated ServiceConnection.
	 */
	public static ServiceConnection getServiceConnection() {
		return sConnection;
	}

	/**
	 * Opens a connection with a Dynamix Framework instance.
	 * 
	 * @param context
	 *            The callers Android context.
	 * @param openSession
	 *            If true, the session will also be opened.
	 * @throws RemoteException
	 */
	public static void openConnection(Context context, boolean openSession) throws RemoteException {
		openConnection(context, openSession, null, null);
	}

	/**
	 * Opens a connection with a Dynamix Framework instance.
	 * 
	 * @param context
	 *            The callers Android context.
	 * @param openSession
	 *            If true, the session will also be opened.
	 * @param callback
	 *            The session callback (may be null).
	 * @throws RemoteException
	 */
	public static void openConnection(Context context, boolean openSession, ISessionCallback callback)
			throws RemoteException {
		openConnection(context, openSession, null, callback);
	}

	/**
	 * Opens a connection with a Dynamix Framework instance.
	 * 
	 * @param context
	 *            The callers Android context.
	 * @param openSession
	 *            If true, the session will also be opened.
	 * @param listener
	 *            The session listener (may be null).
	 * @throws RemoteException
	 */
	public static void openConnection(Context context, boolean openSession, final ISessionListener listener)
			throws RemoteException {
		openConnection(context, openSession, listener, null);
	}

	/**
	 * Opens a connection with a Dynamix Framework instance.
	 * 
	 * @param context
	 *            The callers Android context.
	 * @param openSession
	 *            If true, the session will also be opened.
	 * @param listener
	 *            The session listener (may be null).
	 * @param callback
	 *            The session callback (may be null).
	 * @throws RemoteException
	 */
	public synchronized static void openConnection(Context context, boolean openSession,
			final ISessionListener listener, ISessionCallback callback) throws RemoteException {
		Log.i(TAG, "Handling request to open a Dynamix connection...");
		if (Looper.myLooper() == null)
			Looper.prepare();
		DynamixConnector.context = context;
		DynamixConnector.listener = listener;
		DynamixConnector.callback = callback;
		DynamixConnector.openSession = openSession;
		// Check for proper state
		if (state == CONNECT_STATE.DISCONNECTED) {
			Log.i(TAG, "Trying to bind to Dynamix...");
			state = CONNECT_STATE.CONNECTING;
			Intent intent = new Intent();
			intent.setClassName("org.ambientdynamix.core", "org.ambientdynamix.core.DynamixService");
			context.bindService(intent, sConnection, Context.BIND_AUTO_CREATE);
		} else {
			if (isConnected() && facade.asBinder().isBinderAlive()) {
				Log.i(TAG, "Already connected to Dynamix!");
				if (openSession) {
					if (listener != null)
						facade.openSessionWithListenerAndCallback(listener, callback);
					else
						facade.openSessionWithCallback(callback);
				}
			} else {
				if (facade != null)
					Log.w(TAG, "Cannot connect from state " + state + " -  connection alive = "
							+ facade.asBinder().isBinderAlive());
				else
					Log.w(TAG, "Cannot connect from state " + state + " - connection alive = false (no facade)");
			}
		}
	}

	/**
	 * Closes the connection with Dynamix by unbinding the service.
	 */
	public static void closeConnection() {
		if (facade != null && facade.asBinder().isBinderAlive()) {
			try {
				facade.closeSessionWithCallback(new Callback() {
					@Override
					public void onSuccess() throws RemoteException {
						if (context != null && sConnection != null) {
							context.unbindService(sConnection);
						}
						setDisconnected();
					}

					@Override
					public void onFailure(String message, int errorCode) throws RemoteException {
						Log.w(TAG, "closeConnection.onFailure: " + message);
						if (context != null && sConnection != null) {
							context.unbindService(sConnection);
						}
						setDisconnected();
					}
				});
			} catch (RemoteException e) {
				Log.w(TAG, "closeConnection.RemoteException: " + e);
				if (context != null && sConnection != null) {
					try {
						/*
						 * Guard with a try/catch, since 'unbindService' throws an illegal argument exception if the
						 * service isn't registered.
						 */
						context.unbindService(sConnection);
					} catch (Exception e1) {
						Log.w(TAG, e1);
					}
					setDisconnected();
				}
			}
		} else {
			setDisconnected();
		}
	}

	/*
	 * The ServiceConnection is used to receive callbacks from Android telling our application that it's been connected
	 * to Dynamix, or that it's been disconnected from Dynamix. These events come from Android, not Dynamix. Dynamix
	 * events are always sent to our IDynamixListener object (defined farther below), which is registered (in this case)
	 * in during the 'addDynamixListener' call in the 'onServiceConnected' method of the ServiceConnection.
	 */
	private static ServiceConnection sConnection = new ServiceConnection() {
		/*
		 * Indicates that we've successfully connected to Dynamix. During this call, we transform the incoming IBinder
		 * into an instance of the IDynamixFacade, which is used to call Dynamix methods.
		 */
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			try {
				Log.d(TAG, "Dynamix is connected!");
				// Create a Dynamix Facade using the incoming IBinder
				facade = IDynamixFacade.Stub.asInterface(service);
				// Setup connection state
				state = CONNECT_STATE.CONNECTED;
				// Handle open session requests
				if (openSession) {
					if (listener == null)
						facade.openSessionWithCallback(callback);
					else
						facade.openSessionWithListenerAndCallback(listener, callback);
				}
			} catch (Exception e) {
				Log.w(TAG, e);
			}
		}

		/*
		 * Indicates that a previously connected IDynamixFacade has been disconnected from Dynamix. This typically means
		 * that Dynamix has crashed or been shut down by Android to conserve resources. In this case,
		 * 'onServiceConnected' will be called again automatically once Dynamix boots again.
		 */
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "Dynamix is disconnected!");
			setDisconnected();
			handler.post(new Runnable() {
				@Override
				public void run() {
					if (listener != null) {
						try {
							listener.onSessionClosed();
						} catch (RemoteException e) {
							Log.w(TAG, e);
						}
					}
				}
			});
		}
	};

	private static void setDisconnected() {
		state = CONNECT_STATE.DISCONNECTED;
		facade = null;
	}
}
