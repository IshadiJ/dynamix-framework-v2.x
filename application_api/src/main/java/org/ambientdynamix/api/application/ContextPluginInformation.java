/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ambientdynamix.api.application.AppConstants.ContextPluginType;
import org.ambientdynamix.api.application.AppConstants.PluginInstallStatus;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Provides immutable information about a ContextPlugin, including the plug-in's supported context types.
 * 
 * @author Darren Carlson
 */
public class ContextPluginInformation implements Parcelable {
	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<ContextPluginInformation> CREATOR = new Parcelable.Creator<ContextPluginInformation>() {
		@Override
		public ContextPluginInformation createFromParcel(Parcel in) {
			return new ContextPluginInformation(in);
		}

		@Override
		public ContextPluginInformation[] newArray(int size) {
			return new ContextPluginInformation[size];
		}
	};
	public static String EXTRAS_DYNAMIX_REPO = "EXTRAS_DYNAMIX_REPO";
	// Private variables
	private final String TAG = "ContextPluginInformation";
	private String pluginId = "";
	private String pluginName = "";
	private String pluginDescription = "";
	private VersionInfo version = new VersionInfo(0, 0, 0);
	private List<ContextType> supportedContextTypes = new ArrayList<ContextType>();
	private int statusInt = -1;
	private int plugTypeInt = -1;
	private String requiresConfiguration;
	private String isConfigured;
	private String isEnabled;
	private String isBackgroundService;
	private Map<String, String> extras = new HashMap<String, String>();
	private List<DynamixFeature> dynamixFeatures = new ArrayList<DynamixFeature>();
	private RepositoryInfo repo;

	public ContextPluginInformation() {
	}

	/**
	 * Creates a new ContextPluginInformation.
	 * 
	 * @param pluginId
	 *            The ID of the plug-in.
	 * @param pluginName
	 *            The name of the plug-in.
	 * @param pluginDescription
	 *            The description of the plug-in.
	 * @param version
	 *            The version of the plug-in.
	 * @param userControlledContextAcquisition
	 *            Whether or not this plug-in requires user controlled context acquisition (e.g. through a GUI).
	 * @param supportedContextTypes
	 *            A list of supported context types.
	 * @param plugType
	 *            The plug-in type
	 */
	public ContextPluginInformation(String pluginId, String pluginName, String pluginDescription,
			VersionInfo versionInfo, List<ContextType> supportedContextTypes, PluginInstallStatus status,
			ContextPluginType plugType, boolean requiresConfiguration, boolean isConfigured, boolean isEnabled,
			boolean isBackgroundService, Map<String, String> extras, List<DynamixFeature> dynamixFeatures,
			RepositoryInfo repo) {
		if (pluginId == null || pluginName == null || pluginDescription == null || versionInfo == null
				|| supportedContextTypes == null)
			throw new RuntimeException("Null in ContextPluginInformation constructor!");
		this.pluginId = pluginId;
		this.pluginName = pluginName;
		this.pluginDescription = pluginDescription;
		this.version = versionInfo;
		this.requiresConfiguration = requiresConfiguration ? "true" : "false";
		this.isConfigured = isConfigured ? "true" : "false";
		this.isEnabled = isEnabled ? "true" : "false";
		this.isBackgroundService = isBackgroundService ? "true" : "false";
		this.extras = extras;
		this.dynamixFeatures = dynamixFeatures;
		this.repo = repo;
		if (supportedContextTypes != null)
			this.supportedContextTypes = supportedContextTypes;
		switch (status) {
		case PENDING_INSTALL:
			statusInt = 7;
			break;
		case WAITING_FOR_DEPENDENCY:
			statusInt = 6;
			break;
		case CANNOT_ACCESS_NETWORK:
			statusInt = 5;
			break;
		case UNINSTALLING:
			statusInt = 4;
			break;
		case INSTALLED:
			statusInt = 3;
			break;
		case INSTALLING:
			statusInt = 2;
			break;
		case NOT_INSTALLED:
			statusInt = 1;
			break;
		case ERROR:
			statusInt = 0;
			break;
		}
		switch (plugType) {
		case NORMAL:
			plugTypeInt = 0;
			break;
		case LIBRARY:
			plugTypeInt = 1;
			break;
		default:
			Log.w(TAG, "No type found for: " + plugType + ". Setting to NORMAL.");
			plugTypeInt = 0;
			break;
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Returns the context plug-in's type.
	 * 
	 * @see AppConstants.ContextPluginType
	 */
	public ContextPluginType getContextPluginType() {
		switch (plugTypeInt) {
		case 0:
			return ContextPluginType.NORMAL;
		case 1:
			return ContextPluginType.LIBRARY;
		}
		Log.w(TAG, "getContextPluginType had invalid status int: " + statusInt + ". Returning NORMAL.");
		return ContextPluginType.NORMAL;
	}

	/**
	 * Returns the plug-in's install status.
	 * 
	 * @see AppConstants.PluginInstallStatus
	 */
	public PluginInstallStatus getInstallStatus() {
		switch (statusInt) {
		case 7:
			return PluginInstallStatus.PENDING_INSTALL;
		case 6:
			return PluginInstallStatus.WAITING_FOR_DEPENDENCY;
		case 5:
			return PluginInstallStatus.CANNOT_ACCESS_NETWORK;
		case 4:
			return PluginInstallStatus.UNINSTALLING;
		case 3:
			return PluginInstallStatus.INSTALLED;
		case 2:
			return PluginInstallStatus.INSTALLING;
		case 1:
			return PluginInstallStatus.NOT_INSTALLED;
		case 0:
			return PluginInstallStatus.ERROR;
		}
		Log.e(TAG, "getInstallStatus had invalid status int: " + statusInt);
		return null;
	}

	/**
	 * Returns the list of Dynamix features provided by this plug-in.
	 */
	public List<DynamixFeature> getDynamixFeatures() {
		return this.dynamixFeatures;
	}

	/**
	 * Returns true if the ContextPlugin's ContextPluginRuntime is installed; false otherwise.
	 */
	public boolean isInstalled() {
		return getInstallStatus() == PluginInstallStatus.INSTALLED;
	}

	/**
	 * Returns the description of the plugin.
	 */
	public String getPluginDescription() {
		return pluginDescription;
	}

	/**
	 * Returns the string-based id of the plugin.
	 */
	public String getPluginId() {
		return pluginId;
	}

	/**
	 * Returns the name of the plugin.
	 */
	public String getPluginName() {
		return pluginName;
	}

	/**
	 * Returns the list of supported context types.
	 */
	public List<ContextType> getSupportedContextTypes() {
		return supportedContextTypes;
	}

	/**
	 * Returns true if this plug-in supports the context type string; false otherwise.
	 */
	public boolean supportsContextType(String contextType) {
		return getContextType(contextType) != null;
	}

	/**
	 * Returns the ContextType object for the contextType string, or null if no ContextType can be found.
	 */
	public ContextType getContextType(String contextType) {
		for (ContextType type : supportedContextTypes) {
			if (type.getId().equalsIgnoreCase(contextType))
				return type;
		}
		return null;
	}

	/**
	 * Returns the version of the plugin.
	 * 
	 * @see VersionInfo
	 */
	public VersionInfo getVersion() {
		return version;
	}

	/**
	 * Returns true if this plugin requires configuration; false otherwise.
	 */
	public boolean getRequiresConfiguration() {
		return this.requiresConfiguration.equalsIgnoreCase("true");
	}

	/**
	 * Returns true if this plugin is configured; false otherwise.
	 */
	public boolean isConfigured() {
		return this.isConfigured.equalsIgnoreCase("true");
	}

	/**
	 * Returns true if this plugin is enabled; false otherwise.
	 */
	public boolean isEnabled() {
		return this.isEnabled.equalsIgnoreCase("true");
	}

	public boolean isBackgroundService() {
		return this.isBackgroundService.equalsIgnoreCase("true");
	}

	public RepositoryInfo getRepository() {
		return this.repo;
	}

	public Map<String, String> getExtras() {
		return this.extras;
	}

	@Override
	public String toString() {
		return this.pluginName + " " + this.version + " | " + this.pluginId;
	}

	@Override
	public boolean equals(Object candidate) {
		// Check for null
		if (candidate == null)
			return false;
		// Determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Make sure the id's and version numbers are the same
		ContextPluginInformation other = (ContextPluginInformation) candidate;
		if (this.getPluginId().equalsIgnoreCase(other.getPluginId()) && this.getVersion().equals(other.getVersion()))
			if (getRepository() != null) {
				return this.repo.equals(other.repo);
			} else
				return true;
		return false;
	}

	@Override
	public int hashCode() {
		int result = 17;
		if (getRepository() != null) {
			result = 31 * result + getPluginId().hashCode() + getVersion().hashCode() + getRepository().hashCode();
		} else
			result = 31 * result + getPluginId().hashCode() + getVersion().hashCode();
		return result;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.pluginId);
		dest.writeString(this.pluginName);
		dest.writeString(this.pluginDescription);
		dest.writeParcelable(version, flags);
		dest.writeList(this.supportedContextTypes);
		dest.writeInt(statusInt);
		dest.writeInt(plugTypeInt);
		dest.writeString(this.requiresConfiguration);
		dest.writeString(this.isConfigured);
		dest.writeString(this.isEnabled);
		dest.writeString(this.isBackgroundService);
		dest.writeMap(extras);
		dest.writeList(dynamixFeatures);
		dest.writeParcelable(repo, flags);
	}

	/*
	 * Private constructor (required for the static Parcelable.Creator method)
	 */
	private ContextPluginInformation(Parcel in) {
		this.pluginId = in.readString();
		this.pluginName = in.readString();
		this.pluginDescription = in.readString();
		this.version = in.readParcelable(getClass().getClassLoader());
		in.readList(this.supportedContextTypes, getClass().getClassLoader());
		this.statusInt = in.readInt();
		this.plugTypeInt = in.readInt();
		this.requiresConfiguration = in.readString();
		this.isConfigured = in.readString();
		this.isEnabled = in.readString();
		this.isBackgroundService = in.readString();
		in.readMap(extras, getClass().getClassLoader());
		in.readList(dynamixFeatures, getClass().getClassLoader());
		this.repo = in.readParcelable(getClass().getClassLoader());
	}
}