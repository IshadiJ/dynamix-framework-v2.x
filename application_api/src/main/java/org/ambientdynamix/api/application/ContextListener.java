/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.RemoteException;

/**
 * Base implementation of IContextListener.Stub with no functionality. Override methods as needed.
 *
 * @author Darren Carlson
 * @see IContextListener
 */
public class ContextListener extends IContextListener.Stub {
    private IContextListener injected;

    public ContextListener() {
    }

    public ContextListener(IContextListener injected) {
        this.injected = injected;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextSupportRemoved(ContextSupportInfo support, String message, int errorCode)
            throws RemoteException {
        // TODO Auto-generated method stub
        if (injected != null)
            injected.onContextSupportRemoved(support, message, errorCode);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextListenerRemoved() throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextListenerRemoved();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onContextResult(ContextResult result) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onContextResult(result);
    }
}
