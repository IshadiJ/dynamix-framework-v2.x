/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a context type in the Dynamix framework.
 * 
 * @author Darren Carlson
 *
 */
public class ContextType implements Parcelable, Serializable {
	private String id;
	private String name;
	private String description;

	/**
	 * Creates a ContextType.
	 * 
	 * @param id
	 *            The unique id of the context type.
	 * @param name
	 *            The friendly name for the context type that can be presented to the user.
	 * @param description
	 *            The friendly description for the context type that can be presented to the user.
	 */
	public ContextType(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	/**
	 * Returns the id of the context type (e.g., org.ambientdynamix.contextplugins.pedometer).
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns a friendly name for the context type that can be presented to the user.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns a friendly description for the context type that can be presented to the user.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Creates a ContextType from a Parcel.
	 */
	private ContextType(Parcel in) {
		this.id = in.readString();
		this.name = in.readString();
		this.description = in.readString();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(description);
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<ContextType> CREATOR = new Parcelable.Creator<ContextType>() {
		@Override
		public ContextType createFromParcel(Parcel in) {
			return new ContextType(in);
		}

		@Override
		public ContextType[] newArray(int size) {
			return new ContextType[size];
		}
	};
}
