/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.IBinder;
import android.os.RemoteException;

/**
 * Base implementation of IContextSupportCallback.Stub with no functionality. Override methods as needed.
 *
 * @author Darren Carlson
 * @see IContextSupportCallback
 */
public class ContextSupportCallback extends IContextSupportCallback.Stub {
    private IContextSupportCallback injected;

    public ContextSupportCallback() {
    }

    public ContextSupportCallback(IContextSupportCallback injected) {
        this.injected = injected;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onSuccess(ContextSupportInfo supportInfo) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onSuccess(supportInfo);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onProgress(int percentComplete) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onProgress(percentComplete);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onWarning(String message, int errorCode) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onWarning(message, errorCode);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onFailure(String message, int errorCode) throws RemoteException {
        // Base implementation - no operation
        if (injected != null)
            injected.onFailure(message, errorCode);
    }

    /**
     * Override asBinder() as final to prevent framework NPE's.
     */
    @Override
    public final IBinder asBinder() {
        return super.asBinder();
    }
}
