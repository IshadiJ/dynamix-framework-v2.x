/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.application;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a plug-in using an id and version.
 * 
 * @author Darren Carlson
 *
 */
public class VersionedPlugin implements Parcelable {
	private String pluginId, pluginVersion;
	private VersionInfo version;

	public VersionedPlugin(String pluginId) {
		this.pluginId = pluginId;
	}

	public VersionedPlugin(String pluginId, String pluginVersion) {
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
		this.version = VersionInfo.createVersionInfo(pluginVersion);
	}

	public VersionedPlugin(String pluginId, VersionInfo version) {
		this.pluginId = pluginId;
		this.version = version;
		this.pluginVersion = version.getValue();
	}

	public boolean hasVersion() {
		return this.pluginVersion != null && this.pluginVersion.length() > 0;
	}

	public String getPluginId() {
		return pluginId;
	}

	public String getPluginVersionString() {
		return pluginVersion;
	}

	public VersionInfo getPluginVersion() {
		return version;
	}

	@Override
	public String toString() {
		if (hasVersion())
			return pluginId + ": v" + pluginVersion;
		else
			return pluginId + ": <Latest Known Version>";
	}

	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their id's are the same
		VersionedPlugin other = (VersionedPlugin) candidate;
		if (this.hasVersion())
			return this.pluginId.equalsIgnoreCase(other.pluginId)
					&& this.getPluginVersionString().equalsIgnoreCase(other.getPluginVersionString());
		else
			return this.pluginId.equalsIgnoreCase(other.pluginId);
	}

	// HashCode Example: http://www.javafaq.nu/java-example-code-175.html
	@Override
	public int hashCode() {
		int result = 17;
		if (this.hasVersion())
			result = 31 * result + this.getPluginId().hashCode() + this.getPluginVersionString().hashCode();
		else
			result = 31 * result + this.getPluginId().hashCode();
		return result;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	private VersionedPlugin(Parcel in) {
		this.pluginId = in.readString();
		if (Utils.readBoolean(in)) {
			this.pluginVersion = in.readString();
			this.version = in.readParcelable(getClass().getClassLoader());
		}
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(pluginId);
		if (hasVersion()) {
			Utils.writeBoolean(out, true);
			out.writeString(pluginVersion);
			out.writeParcelable(version, 0);
		} else
			Utils.writeBoolean(out, false);
	}

	/**
	 * Static Parcelable.Creator required to reconstruct a the object from an incoming Parcel.
	 */
	public static final Parcelable.Creator<VersionedPlugin> CREATOR = new Parcelable.Creator<VersionedPlugin>() {
		@Override
		public VersionedPlugin createFromParcel(Parcel in) {
			return new VersionedPlugin(in);
		}

		@Override
		public VersionedPlugin[] newArray(int size) {
			return new VersionedPlugin[size];
		}
	};
}
